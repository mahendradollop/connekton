<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Web_Manager';
/////////////////////////////////////////////////////////////////////////////
//////////////////////// admin //////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
$route['admin'] 					= 'admin/Common_Manager';
$route['adminLogin'] 		       	= 'admin/Common_Manager/adminLogin';
$route['logout'] 		       		= 'admin/Common_Manager/logout';
$route['forgotPassword'] 		    = 'admin/Common_Manager/forgotPassword';
$route['admin_forgot_password'] 	= 'admin/Common_Manager/admin_forgot_password';
$route['resetPassword/(:any)']	   	= 'admin/Common_Manager/get_password';
$route['change_forgeted_password']  = 'admin/Common_Manager/change_forgeted_password';

$route['dashboard'] 			   	= 'admin/Dashboard_Manager';
////////////////////////plan
$route['plan']                            = 'admin/Common_Manager/plan';
$route['view_plan']                       = 'admin/Common_Manager/view_plan';
$route['view_article']                    = 'admin/Common_Manager/view_article';
$route['user_list']                       = 'admin/Common_Manager/user_list';
$route['client_list']                     = 'admin/Common_Manager/client_list';
$route['approved_kp_list']                = 'admin/Common_Manager/approved_kp_list';
$route['unapproved_kp_list']              = 'admin/Common_Manager/unapproved_kp_list';
$route['employer_list']                   = 'admin/Common_Manager/employer_list';
$route['view_question']                   = 'admin/Common_Manager/view_question';



/////////////////////////////////////////////////////////////////////////////
////////////////////////// web //////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
$route['signUpView'] 			   	= 'Web_Manager/signUpView';
$route['loginView'] 			    = 'Web_Manager/loginView';
$route['registrationView'] 			= 'Web_Manager/registrationView';
$route['planView'] 					= 'Web_Manager/planView';
$route['userDashboardView'] 		= 'web/DashboardManager/userDashboardView';
$route['clientQuesion'] 			= 'web/DashboardManager/clientQuesion';
$route['article'] 					= 'web/DashboardManager/article';
$route['userLogout'] 				= 'web/DashboardManager/userLogout';





$route['sendOtp'] 				= 'web/LoginSignupController/sendOtp';
$route['verifyOtp'] 			= 'web/LoginSignupController/verifyOtp';
$route['verifyOtp'] 			= 'web/LoginSignupController/verifyOtp';
$route['userRegister'] 			= 'web/LoginSignupController/userRegister';
$route['checkPhone'] 			= 'web/LoginSignupController/checkPhone';
$route['checkEmail'] 			= 'web/LoginSignupController/checkEmail';
$route['updateUserRegister'] 	= 'web/LoginSignupController/updateUserRegister';
$route['getUserPlan'] 			= 'webservice/PlanController/getUserPlan';
$route['addUserPlan'] 			= 'webservice/PlanController/addUserPlan';




$route['get_country'] 			= 'web/LoginSignupController/get_country';
$route['get_state'] 			= 'web/LoginSignupController/get_state';
$route['get_city'] 			= 'web/LoginSignupController/get_city';
$route['get_comunity'] 			= 'web/LoginSignupController/get_comunity';
$route['get_skills'] 			= 'web/LoginSignupController/get_skills';



/////////////////////////////////////////////////////////////////////////////
////////////////////////// webservice ///////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
$route['UserLogin'] 			= 'webservice/UserController/userLogin';
$route['addPlan'] 			= 'admin/PlanController/addPlan';
$route['getPlan'] 			= 'admin/PlanController/getPlan';
$route['getPlan/(:any)'] 			= 'admin/PlanController/getPlan/$1';
$route['getCommunityMembers'] 			= 'webservice/ArticleController/getCommunityMembers';


///////////////////user api for admin 
$route['getAllUserByAdmin'] 			= 'admin/AdminUserController/getAllUserByAdmin';
$route['getUserByAdmin/(:any)'] 			= 'admin/AdminUserController/getAllUserByAdmin/$1';
$route['getAllKpByAdmin'] 			= 'admin/AdminUserController/getAllKpByAdmin';
$route['getKpByAdmin/(:any)'] 			= 'admin/AdminUserController/getAllKpByAdmin/$1';
$route['getAllClientByAdmin'] 			= 'admin/AdminUserController/getAllClientByAdmin';
$route['getClientByAdmin/(:any)'] 			= 'admin/AdminUserController/getAllClientByAdmin/$1';
$route['getAllEmployesByAdmin'] 			= 'admin/AdminUserController/getAllEmployesByAdmin';
$route['getEmployesByAdmin/(:any)'] 			= 'admin/AdminUserController/getAllEmployesByAdmin/$1';
$route['updateUserByAdmin'] 			= 'admin/AdminUserController/updateUserByAdmin';
$route['getAllQuestion'] 			= 'admin/AdminQuestionController/getAllQuestion';
$route['getAllQuestion/(:any)'] 			= 'admin/AdminQuestionController/getAllQuestion/$1';
$route['editQuestionByAdmin'] 			= 'admin/AdminQuestionController/editQuestionByAdmin';

////////////////////////
$route['addArticle']             = 'webservice/ArticleController/addArticle';
$route['getAtricleCommentReportLike/(:any)']             = 'webservice/ArticleController/getAtricleCommentReportLike/$1';
$route['getArticle']             = 'webservice/ArticleController/getArticle';
$route['getArticle/(:any)']             = 'webservice/ArticleController/getArticle/$1';




$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
