<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{

    public function check_login($email,$password){
          $query = $this->db->select('*')
                        ->from('admin')
                        ->where('admin_email_id',$email)
                        ->where('admin_password',md5($password))
                        ->where('admin_is_active',1)
                        ->where('admin_is_verified',1)
                        ->where('admin_is_deleted',1)
                        ->get();
        if($query->num_rows() === 1){
            return $query->row_array();            
        }else {
            return false;
        } 
    }



    public function create_token($id){
        $sql = "insert into user_token (user_id,token)
                    values (".$id.",'".randomString()."')";
        return $this->db->query($sql) ? true : false;
    }

    

    

    public function destroy_token($id){
        $this -> db -> where('user_id', $id);
        return $this -> db -> delete('user_token')? true :false ;
    }



    public function checkToken($token){
        $query = $this->db->select('*')
                            ->where('token',$token)
                            ->limit(1)
                            ->get('Admin');
        return $query->num_rows() === 1 ? $query->row_array() : false;
    }






    public function change_password($admin_id,$new_password){
        $this->db->where('admin_id',$admin_id);
        $query =$this->db->update('Admin',array('admin_password'=>$new_password));
        return ($query) ? true : false;
    }



    public function getDataWhere($table,$where,$select){

        $this->db->select($select);

        $this->db->from($table);

        $this->db->where($where);

        $query=$this->db->get();

        return $query->num_rows() === 1 ? $query->row_array() : false;

    }



    public function insertData($table,$data){        

        $query=$this->db->insert($table,$data);

        return $query ? $this->db->insert_id() : false;

    }



    public function updateData($table,$data,$where){

        $this->db->where($where);

        $query=$this->db->update($table,$data);

        return $query ? true : false;

    }





}     



