<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * @Author:Meenakshi
 *This Model used for Road_watch api
 */
class AuthModel extends CI_Model
{
    public function check_admin_password($admin_id,$old_password){

        $this->db->select('* ');

        $this->db->from('admin');

        $this->db->where('admin_id',$admin_id);

        $this->db->where('admin_password',$old_password);

        $query=$this->db->get();

        return $query->num_rows() > 0 ? true : false;

    }



    public function change_admin_password($admin_id,$new_password){

        $this->db->where('admin_id',$admin_id);  

        $query=$this->db->update('admin',array('admin_password'=>$new_password)); 

        return ($query) ? true : false;

    }
}
