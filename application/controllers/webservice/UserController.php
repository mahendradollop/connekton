<?php
require APPPATH . 'libraries/REST_Controller.php';

class UserController extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->config->load('myConstant');
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->service('User_service');
    }

    /**********
     * Use: User Login
     * Param : email Type:String
     * 			mobile: type: Integer
     * 			password: Type: String
     * Method : Post
     * Response:OK ************ */
    public function userLogin_post()
    {
        $emailAndMobile = $this->input->post('emailAndMobile');
        $password = $this->input->post('password');
        if ($password != "" && $emailAndMobile != "") 
        {
            $result = $this->user_service->userLogin($emailAndMobile, md5($password));            
          
            if ($result) {

                if ($result['is_active'] != "Deactive") 
                {
                    if ($result['is_active'] != "Blocked") 
                    {

                        $userData['user_email'] = $result['email'];
                        $userData['user_id'] = $result['user_id'];
                        $userData['role'] = $result['user_role'];
                        $tokenData = $this->authorization_token->generateToken($userData);
                        $sucessResponse['token'] = $tokenData;
                        $sessionToken =  $sucessResponse['token'];
                        $sucessResponse['UserDetails'] = $result;
                        $token = array('token'=> $sessionToken);                     
                        $this->session->set_userdata('connectOnWeb', $token);
                        $sucessResponse['message'] = MESSAGE_conf::AUTHRIZED_USER;
                        $this->response(array("UserToken" => $sucessResponse, "message" => MESSAGE_conf::AUTHRIZED_USER), REST_Controller::HTTP_OK);
                    } 
                    else 
                    {
                        $this->response(array("message" => MESSAGE_conf::BLOCKED_ACCOUNT), REST_Controller::HTTP_UNAUTHORIZED);
                    }
                } 
                else {
                    $this->response(array("message" => MESSAGE_conf::UNDER_VERIFICATION), REST_Controller::HTTP_UNAUTHORIZED);
                }
            } 
            else {
                $this->response(array("message" => MESSAGE_conf::UN_AUTHRIZED_USER), REST_Controller::HTTP_UNAUTHORIZED);
            }
        }
         else {
            $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
        }
    }


    /******
     * Use:get user
     * Method:Get
     * Response:OK
     * ***** */
    public function getUser_get($userId = "")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);

        return $result;
    }
}
