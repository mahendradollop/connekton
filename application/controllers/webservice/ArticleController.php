<?php
require APPPATH . 'libraries/REST_Controller.php';

class ArticleController extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->config->load('myConstant');
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->service('User_service');
    }

    /**********
     * Use: User Login
     * Param : email Type:String
     * 			mobile: type: Integer
     * 			password: Type: String
     * Method : Post
     * Response:OK ************ */
    public function addArticle_post()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if($result)
        {            
            $id = $result['id'];
            $role = $result['role'];
            $article_id = $this->input->post('article_id');
            $community_ids = $this->input->post('community_ids');
            $content = $this->input->post('content');
            $is_delete = $this->input->post('is_delete');
            $is_active = $this->input->post('is_active');
            $article_title  = $this->input->post('article_title');
            $articleData  = array('community_ids' => implode(",", $community_ids) ,
                                    'article_title' => $article_title, 
                                    'content' => $content ,                                    
                
            );
            if (isset($article_id) && $article_id !="") 
            {
               if(isset($community_ids) && $community_ids != "")
               {
                    $updateArticleData['community_ids'] = implode(",", $community_ids);
               }
               if(isset($article_title) && $article_title != "")
               {
                    $updateArticleData['article_title'] = $article_title;
               }
               if(isset($content) && $content != "")
               {
                    $updateArticleData['content'] = $content;
               }
               if(isset($is_active) && $is_active != "")
               {
                   if($is_active == 1)
                   {
                        $updateArticleData['is_active'] = "active";
                   }
                   if($is_active == 0)
                   {
                        $updateArticleData['is_active'] = "not_active";
                   }
                    
               }
               if(isset($is_delete) && $is_delete != "")
               {
                    $updateArticleData['is_deleted'] = $is_delete;
               }
               if((isset($_FILES['cover_image']) && $_FILES['cover_image']['name'] !="" ))
               {
                    $image_data = $_FILES['cover_image'];
                    $path = "uploads/article/";
                    $image = $this->Common_model->upload_image($image_data, 1, $path);
                    $updateArticleData['cover_image'] = $path . $image;
               }
                $updateArticle = $this->user_service->updateArticle($article_id,$updateArticleData);
                if ($updateArticle) {
                    $this->response(array("message" => MESSAGE_conf::UPDATE_ARTICLE,), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array("message" => MESSAGE_conf::ARTICLE_UPDATE_FAILED,), REST_Controller::HTTP_BAD_REQUEST);
                }
               
            }
            else 
            {
                if((isset($_FILES['cover_image']) && $_FILES['cover_image']['name'] !="" ) && (isset($community_ids) && $community_ids !="") && (isset($content) && $content != "") && (isset($article_title) && $article_title != ""))
                {
                    $image_data = $_FILES['cover_image'];
                    $path = "uploads/article/";
                    $image = $this->Common_model->upload_image($image_data, 1, $path);
                    $articleData['cover_image'] = $path.$image;
                    $articleData['posted_by'] = $role;
                    $articleData['user_id'] = $id;  
                    $addArticle = $this->user_service->addArticle($articleData);

                    if($addArticle)
                    {
                        $this->response(array("message" => MESSAGE_conf::ADDED_ARTICLE,), REST_Controller::HTTP_OK);
                    }
                    else 
                    {
                        $this->response(array("message" => MESSAGE_conf::ARTICLE_ADDED_FAILED, ), REST_Controller::HTTP_BAD_REQUEST);
                    }
                }
                else 
                {                    
                    $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED, "data" => $articleData), REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }
        else 
        {
            return $result;
        } 
    }

    /****
     * Use:Get Article by and all
     * Method:get
     * Resaponse:OK
     * *** */

     public function getArticle_get($articleId = "")
     {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if ($result) 
        { 
            if($articleId != "")
            {
                $getArticle = $this->user_service->getArticleById($articleId);
            }
            else 
            {
               $getArticle = $this->user_service->getAllArticle();

            }
            if($getArticle)
            {
                $this->response(array("message" => MESSAGE_conf::SUCCESS, "data" => $getArticle), REST_Controller::HTTP_OK);

            }
            else 
            {
                $this->response(array("message" => MESSAGE_conf::FAILED, ), REST_Controller::HTTP_OK);

            }
        }
        else 
        {
            return $result;
        }
     }

     /****
      * Use: get comments of the article
        Method:Get
        Param :article id
        Response:OK
        *** */
    public function getAtricleCommentReportLike_get($articleId)
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if ($result) {
          
                $getAllArticleComment = $this->user_service->getAllArticleComment($articleId);
                $getAllArticleReport = $this->user_service->getAllArticleReport($articleId);
                $getAllArticleLikeDislike = $this->user_service->getAllArticleLikeDislike($articleId);           
            if ($getAllArticleComment) {
                $this->response(array("message" => MESSAGE_conf::SUCCESS, "comment" => $getAllArticleComment,"report"=> $getAllArticleReport,"likeDislike"=> $getAllArticleLikeDislike), REST_Controller::HTTP_OK);
            } else {
                $this->response(array("message" => MESSAGE_conf::FAILED,), REST_Controller::HTTP_OK);
            }
        } else {
            return $result;
        }
    }

        /****
      * Use:Get All Community Members
        Method:Get
        Param :
        Response:OK
        *** */
    public function getCommunityMembers_get()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if ($result) 
        {   
            $getCommunityMembers = $this->user_service->getAllCommunityMembers();

            if($getCommunityMembers)
            {
                $this->response(array("message" => MESSAGE_conf::SUCCESS, "data" => $getCommunityMembers), REST_Controller::HTTP_OK);
            }
            else 
            {
                $this->response(array("message" => MESSAGE_conf::FAILED, ), REST_Controller::HTTP_OK);
            }
        }
        else 
        {
            return $result;
        }
    }

    /**********
     * Use: Use for like dislike
     * Param : 
     * Method : Post
     * Response:OK ************ */

    public function addArticleLikeDislike_post()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if($result)
        {            
            $id = $result['id'];
            $is_delete = $this->input->post('is_delete');
            $type = $this->input->post('article');
            $target_id = $this->input->post('target_id');
            $like_dislike_id = $this->input->post('like_dislike_id');
            $action = $this->input->post('action');
        }
    }
}
