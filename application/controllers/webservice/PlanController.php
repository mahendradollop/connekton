<?php
require APPPATH . 'libraries/REST_Controller.php';

class PlanController extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->config->load('myConstant');
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->service('User_service');
    }

    /**********
     * Use: User Login
     * Param : email Type:String
     * 			mobile: type: Integer
     * 			password: Type: String
     * Method : Post
     * Response:OK ************ */
    public function addUserPlan_post()
    {

        $user_id = $this->input->post('user_id');
        $plan_id = $this->input->post('plan_id');
        $plan_duration = $this->input->post('plan_duration');

        $where = array('user_id'=> $user_id,'is_active'=>'Active');
        $result= $this->user_service->getDataWhere('user_plan','*,max(end_date) as max_end_date',$where);
        
        if (!isset($result['user_plan_id']) && $result['user_plan_id']=="") 
        {    
        	$start_date = date('Y-m-d');
        	$end_date =  date('Y-m-d', strtotime("+".$plan_duration." months", strtotime($start_date)));

        	if($plan_id!="" && $plan_duration!=""){
        		$planData = array(
    					"user_id"=>$user_id,
    					"plan_id"=>$plan_id,
    					"start_date"=>$start_date,
    					"end_date"=>$end_date,
    					); 
        		$addPlan = $this->user_service->insertData('user_plan',$planData);
        		if($addPlan)
                {
                    delete_cookie("register_user_cookie");
                    $this->response(array("message" => MESSAGE_conf::USER_PLAN,), REST_Controller::HTTP_OK);
                }
                else 
                {
                    $this->response(array("message" => MESSAGE_conf::WRONG, ), REST_Controller::HTTP_BAD_REQUEST);
                }
            	
            }
            else 
            {
                $this->response(array("message" => MESSAGE_conf::INVALID_USER), REST_Controller::HTTP_BAD_REQUEST);

            }
        } else {

            $start_date = date('Y-m-d', strtotime($result['max_end_date'] . ' +1 day'));
            $end_date =  date('Y-m-d', strtotime("+".$plan_duration." months", strtotime($start_date)));

            if($plan_id!="" && $plan_duration!=""){
                $planData = array(
                        "user_id"=>$user_id,
                        "plan_id"=>$plan_id,
                        "start_date"=>$start_date,
                        "end_date"=>$end_date,
                        ); 
                $addPlan = $this->user_service->insertData('user_plan',$planData);
                if($addPlan)
                {
                    delete_cookie("register_user_cookie");
                    $this->response(array("message" => MESSAGE_conf::USER_PLAN,), REST_Controller::HTTP_OK);
                }
                else 
                {
                    $this->response(array("message" => MESSAGE_conf::WRONG, ), REST_Controller::HTTP_BAD_REQUEST);
                }
                
            }
            else 
            {
                $this->response(array("message" => MESSAGE_conf::INVALID_USER), REST_Controller::HTTP_BAD_REQUEST);

            }
        
        }
        
    }


    public function getUserPlan_get(){
        $user_id = $this->input->get('user_id');
        $planId = $this->input->get('planId');

        $where = array('user_id'=> $user_id);
        $result= $this->user_service->getDataWhere('user','user_role',$where);

        if(isset($result) && $result['user_role']!="") 
        {
            $role = $result['user_role'];  


            if($role == "Client" || $role == "Employer" ||  $role == "User" || $role == "KP" )
            {
                if($planId !="")
                { 
                   $getPlan = $this->user_service->getPlanById($planId);                    
                }
                else {
                    
                    $getPlan = $this->user_service->getAllPlan($role);
                    
                }
                if($getPlan)
                {
                    $this->response(array("message" => MESSAGE_conf::SUCCESS,'plan'=> $getPlan), REST_Controller::HTTP_OK);

                }
                else 
                {
                    $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);

                }

            }
            else 
            {
                $this->response(array("message" => MESSAGE_conf::INVALID_USER), REST_Controller::HTTP_BAD_REQUEST);

            }

        }else{
            $this->response(array("message" => MESSAGE_conf::INVALID_USER), REST_Controller::HTTP_BAD_REQUEST);

        }
    }


    /******
     * Use:get user
     * Method:Get
     * Response:OK
     * ***** */
    public function getUser_get($userId = "")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);

        return $result;
    }
}
