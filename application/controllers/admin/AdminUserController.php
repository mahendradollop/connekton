<?php
require APPPATH . 'libraries/REST_Controller.php';

class AdminUserController extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->config->load('myConstant');
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->service('User_service');
    }
   /*********
    *   Use:Get all user by admin
    *   Method:get
    *   Response:OK
    ************ */

    public function getAllUserByAdmin_get($userId ="")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if ($result) {
            $role = $result['role']; 
            if($role == "Admin")
            {
                if($userId !="")
                {
                    $getUser = $this->user_service->getUserByIdByAdmin($userId);
                }
                else {
                    $getUser = $this->user_service->getAllUserByAdmin();
                }
                if($getUser)
                {
                    $this->response(array("message" => MESSAGE_conf::SUCCESS,"data"=> $getUser), REST_Controller::HTTP_OK);

                }
                else 
                {
                    $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
                }
            }
            else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        }
        else {
            return $result;
        }
    }


    /***
     * Use:Update user by admin
     * Method:put
     * Param:user_id
     * Response:OK    
     * */

     public function updateUserByAdmin_put()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if ($result) {
            $role = $result['role'];
            if ($role == "Admin") {
               $user_id = $this->put("user_id");
               $is_delete = $this->put("is_delete");
               $is_active = $this->put("is_active");
               if(isset($is_delete) && $is_delete !="")
               {
                   $updateData['is_delete']  = $is_delete;
               }
               if(isset($is_active) && $is_active !="")
               {
                    
                   if($is_active == 1 )
                   {                     
                        $is_active = "Active";
                   }
                  else
                    {                      
                        $is_active = "Blocked";
                    }
                   
                   $updateData['is_active']  = $is_active;
                  
               }
               $updateUserData=$this->user_service->updateUser($user_id, $updateData);
               if($updateUserData)
               {
                    $this->response(array("message" => MESSAGE_conf::SUCCESS), REST_Controller::HTTP_OK);
               }
               else 
               {
                    $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
               }
            } else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            return $result;
        }
    }

    /**********
     * Use:get all clients
     * Method:GET
     * Response:OK
     * **************** */
    public function getAllClientByAdmin_get($user_id="")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if ($result) {
            $role = $result['role'];
            if ($role == "Admin") {
                if ($user_id != "") {
                    $getUser = $this->user_service->getClientByIdByAdmin($user_id);
                } else {
                    $getUser = $this->user_service->getAllClientByAdmin();
                }
                if ($getUser) {
                    $this->response(array("message" => MESSAGE_conf::SUCCESS, "data" => $getUser), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
                }
            } else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            return $result;
        }
    }
    /**********
     * Use:get all employer
     * Method:GET
     * Response:OK
     * **************** */
    public function getAllEmployesByAdmin_get($user_id = "")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if ($result) {
            $role = $result['role'];
            if ($role == "Admin") {
                if ($user_id != "") {
                    $getUser = $this->user_service->getEmployesByIdByAdmin($user_id);
                } else {
                    $getUser = $this->user_service->getAllEmployesByAdmin();
                }
                if ($getUser) {
                    $this->response(array("message" => MESSAGE_conf::SUCCESS, "data" => $getUser), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
                }
            } else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            return $result;
        }
    }
    /**********
     * Use:get all KPs
     * Method:GET
     * Response:OK
     * **************** */
    public function getAllKpByAdmin_get($user_id = "")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if ($result) {
            $role = $result['role'];
            if ($role == "Admin") {
                if ($user_id != "") {
                    $getUser = $this->user_service->getKpByIdByAdmin($user_id);
                } else {
                    $getUser = $this->user_service->getAllKpByAdmin();
                }
                if ($getUser) {
                    $this->response(array("message" => MESSAGE_conf::SUCCESS, "data" => $getUser), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
                }
            } else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            return $result;
        }
    }
}
