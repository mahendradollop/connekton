<?php
require APPPATH . 'libraries/REST_Controller.php';

class planController extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->config->load('myConstant');
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->service('User_service');
    }
    /***
     * Use:Add and edit Plan
     * Param:Title(string)
     *       fee(int)*
     *       description(string)
     *       duration(string)
     *       role(string)
    *        plan For(string)
    *         question limit(int)
    *         question amount(int)
    *`Method:post
        Response:OK
     * ** */
    public function addPlan_post()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if ($result) 
        {
            $plan_title = $this->input->post('plan_title');
            $fee = $this->input->post('fee');
            $description = $this->input->post('description');
            $duration = $this->input->post('duration');
            $role = $this->input->post('role');
            $plan_for = $this->input->post('plan_for');
            $question_limit = $this->input->post('question_limit');
            $question_amount = $this->input->post('question_amount');
                $is_active = $this->input->post('is_active');
                $is_delete = $this->input->post('is_delete');
            $planId = $this->input->post('plan_id');
          
            if(isset($planId) && $planId !="")
            {
              //  print_r($_POST);
                //die; 
                  $plan_data = array();
                if(isset($plan_title) && $plan_title != "")
                {
                 $plan_data['plan_title'] = $plan_title;  
                }
                if(isset($plan_for) && $plan_for != "")
                {
                 $plan_data['plan_for'] = $plan_for;  
                }
                if(isset($fee) && $fee != "")
                {
                 $plan_data['fee'] = $fee;  
                }
                if(isset($description) && $description != "")
                {
                 $plan_data['description'] = $description;  
                }
                if(isset($role) && $role != "")
                {
                 $plan_data['role_type'] = $role;  
                }
                
                if(isset($duration) && $duration != "")
                {
                 $plan_data['duration'] = $duration;  
                }
                if(isset($question_limit) && $question_limit != "")
                {
                  $plan_data['question_limit'] = $question_limit;  
                }
                if(isset($question_amount) && $question_amount != "")
                {
                  $plan_data['question_amount'] = $question_amount;  
                }
                if ($role != "Client") {
                    $plan_data['plan_for'] = "";  
                    $plan_data['question_limit'] = "";
                    $plan_data['question_amount'] = "";  
                }              
                if(isset($is_delete) && $is_delete != "")
                {
                    
                  $plan_data['is_delete'] = $is_delete;  
                }                
                if(isset($is_active) && $is_active != "")
                {
                    if($is_active == 1)
                    {
                        $is_active = "active";
                    }
                    if($is_active == 0)
                    {
                        $is_active = "not_active";
                    }
                     $plan_data['is_active'] = $is_active;  
                }                
               
                $where = array('plan_id' => $planId);
                $updatePlan = $this->user_service->updateData('plan', $where,$plan_data);
                    if ($updatePlan) {
                        $this->response(array("message" => MESSAGE_conf::SUCCESS), REST_Controller::HTTP_OK);
                        } else {
                        $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
                     }
            }
            else 
            {
                if ($plan_title != "" && $fee != "" && $description != "" && $duration != "" && $role != "") {
                    $plan_data = array('plan_title' => $plan_title,
                                        'fee' => $fee,
                                        'description' => $description, 
                                        'duration' => $duration,
                                        'role_type' => $role,                                             
                                      );
                    if($role == "Client")
                    {
                        $plan_data['plan_for'] = $plan_for;
                        $plan_data['question_limit'] = $question_limit;
                        $plan_data['question_amount'] = $question_amount;
                    }
                    $addPlan = $this->user_service->insertData('plan',$plan_data);
                    if($addPlan)
                    {
                    $this->response(array("message" => MESSAGE_conf::SUCCESS), REST_Controller::HTTP_OK);
                    }
                    else
                    {
                    $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
                    }
                } else {
                    $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
                }
            }
            
        }
        else
        {
            return $result;
        }
    }

    /**
     * Use:get Plan (all,single)
     * Method:Get
     * param:PlanId
     * Response:OK
     * ***/

     public function getPlan_get($planId = "")
     {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if($result) 
        {
            $role = $result['role'];           
            if($role == "Client" || $role == "Employer" ||  $role == "User" || $role == "KP" || $role = "Admin")
            {
                if($planId !="")
                { 
                   $getPlan = $this->user_service->getPlanById($planId);                    
                }
                else {
                    if($role=="Admin"){
                        $role ="";
                    }
                    $getPlan = $this->user_service->getAllPlan($role);
                }
                if($getPlan)
                {
                    $this->response(array("message" => MESSAGE_conf::SUCCESS,'plan'=> $getPlan), REST_Controller::HTTP_OK);

                }
                else 
                {
                    $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);

                }

            }
            else 
            {
                $this->response(array("message" => MESSAGE_conf::INVALID_USER), REST_Controller::HTTP_BAD_REQUEST);

            }

        }
        else
        {
            return $result;
        }
     }
  
}
