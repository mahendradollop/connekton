<?php
require APPPATH . 'libraries/REST_Controller.php';

class AdminQuestionController extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->config->load('myConstant');
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->service('User_service');
    }
    /*********
     *   Use:Get all user by admin
     *   Method:get
     *   Response:OK
     ************ */
    

    public function getAllQuestion_get($questionId = "")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if ($result) {
            $role = $result['role'];
            if ($role == "Admin") {
                if ($questionId != "") {
                    $getQuestion = $this->user_service->getQuestionByIdByAdmin($questionId);
                } else {
                    $getQuestion = $this->user_service->getAllQuestionByAdmin();
                }
                if ($getQuestion) {
                    $this->response(array("message" => MESSAGE_conf::SUCCESS, "data" => $getQuestion), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
                }
            } else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            return $result;
        }
    }


}
