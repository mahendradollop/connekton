<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_Manager extends CI_Controller {

   	function __construct(){
		parent::__construct();
        if(!isset($this->session->userdata(session_key())['is_admin_login'])){
			$this->session->set_flashdata('error','You Must Login First.');
			redirect(base_url().'admin', 'refresh');
		}
	}

    public function template($path,$data=''){
	    $this->load->view('admin/includes/header');
	    $this->load->view('admin/includes/sidebar');
	    $this->load->view($path,$data);
	    $this->load->view('admin/includes/footer');

    }

	public function index()
	{
		$data	=	array();
		$this->template('admin/dashboard',$data);
	}
}
?>
