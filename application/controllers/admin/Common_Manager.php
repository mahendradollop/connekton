<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
	this controller have common functionality for admin
	like login, logout, change password, forget password etc
*/	

class Common_Manager extends CI_Controller {

	function __construct(){

		parent::__construct();
		$this->load->model('admin/Login_model');
		$this->load->library('Authorization_Token');
		$this->load->database();
		$this->load->service('User_service');

		date_default_timezone_set('asia/kolkata');

	}

	// load login Page 
	public function index(){

		//echo "<pre>";print_r(session_key());die();
		if(isset($this->session->userdata(session_key())['is_admin_login'])){
			$this->session->set_flashdata('error','You Must Login For That Page.');
			redirect(base_url().'dashboard', 'refresh');
		}
		$this->load->view('admin/login');
	}

	// login functionality of admin
	public function adminLogin(){		
		//echo "<pre>";print_r( $user_exits );die();
		if(isset($this->session->userdata(session_key())['is_admin_login'])){
			$this->session->set_flashdata('error','You Must Login For That Page.');
			redirect(base_url().'dashboard', 'refresh');
		}

		if($this->input->post('email')=="" ||  $this->input->post('password')==""){
			$this->session->set_flashdata('error','Please Enter valid Email and Password');
		 	redirect(base_url().'admin', 'refresh'); 
		}		
		
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true){

           $user_exits = $this->Login_model->check_login($this->input->post('email'),$this->input->post('password'));  

			if($user_exits){
				if(isset($user_exits['admin_id']))
				{
					$userData['user_email'] = $user_exits['admin_email_id'];
					$userData['user_id'] = $user_exits['admin_id'];
					$userData['role'] = "Admin";
					$tokenData = $this->authorization_token->generateToken($userData);
					//$sucessResponse['token'] = $tokenData;
					//$sessionToken =  $sucessResponse['token'];
					//$sucessResponse['UserDetails'] = $result;
					//$this->session->set_userdata('token', $sessionToken);
					 $set_session = array(
						'is_admin_login'=> true,
						'admin_id'=> $user_exits['admin_id'],
						'admin_name'=> $user_exits['admin_name'],
						'admin_phone'=> $user_exits['admin_phone'],
						'admin_email_id'=> $user_exits['admin_email_id'],
						'admin_role'=> $user_exits['admin_role'],
						'admin_profile_pic'=> $user_exits['admin_profile_pic'],
						'admin_token'=> $tokenData,
					);
				}

				$this->session->set_userdata(session_key(),$set_session);
				$this->session->set_flashdata('success', 'Login successfuly!!');
				redirect(base_url().'dashboard', 'refresh');
			}else{
				$this->session->set_flashdata('error','Email and Password Mismatch');
				redirect(base_url().'admin', 'refresh');
			}
		}else{
		    $this->session->set_flashdata('error','Please Enter Valid Email and Password');
		    redirect(base_url().'admin', 'refresh');
		}
	}

	public function logout()
	{
		unset($_SESSION[session_key()]);
			
		$this->session->set_flashdata('success','Logout Successfully');
		redirect(base_url('admin'));
	}

	public function forgotPassword(){
		$this->load->view('admin/forgotPassword');
    }

    public function admin_forgot_password()
	{
		$this->load->library('email');
        $admin_email=$_POST['email'];	
        
        // print_r($admin_email);
        // die();

        $from_email='mahendra.dollop@gmail.com';       
        $token = mt_rand(100000, 999999);
        $token =md5($token);
        $url=base_url().'resetPassword/'.$token;
        $select='admin_id';
        $where=array('admin_email_id'=>$admin_email,'admin_is_active'=>1);
        //check admin is exist or is active
        $varifyAdminEmail = $this->Login_model->getDataWhere('Admin',$where,$select);
        //print_r($varifyAdminEmail);die;
        if (!empty($varifyAdminEmail)) {

        		$admin_email='mahendra.dollop@gmail.com'; 

        		$this->email->from($from_email, 'ConneKton'); 
		        $this->email->to($admin_email);
		        $this->email->subject('Forgote password mail'); 
		        $this->email->message('Please click on url to create new password '. $url);

		        if($this->email->send()) {
		        //if(1) {
					$data=array('token'=>$token);
					$where=array('admin_id'=>$varifyAdminEmail['admin_id']);
					$res = $this->Login_model->updateData('Admin',$data,$where);					
					if ($res) {
						$this->session->set_flashdata('success', 'Mail Sent successfuly!! Please check your mail');
					} else {
						$this->session->set_flashdata('error','failed');
					}	
		        }else{
		         	$this->session->set_flashdata('error','mail failed');
		        }
		}else{
			$this->session->set_flashdata('error','You Entered Wrong Email.');
		}
 		redirect(base_url().'forgotPassword');
	}

	public function get_password(){
		$this->load->view('admin/resetPassword');
	}

	public function change_forgeted_password(){
		$new_password= md5($_POST['new_password']);
		$token 		 =$_POST['token'];
		$checkToken = $this->Login_model->checkToken($token);
		//echo $checkToken;die();
		if (!empty($checkToken)) {
			$admin_id=$checkToken['admin_id']; 
			$res = $this->Login_model->change_password($admin_id,$new_password);
			if ($res) {
				$this->session->set_flashdata('success', 'password change successfuly! please Login. ');
				redirect(base_url().'admin');
			} else {
				$this->session->set_flashdata('error','failed');				
			}					

		}else{
			$this->session->set_flashdata('error','you are not authority to change admin password');
		}		
		redirect(base_url().'forgotPassword');	
	}
	public function plan()
	{
		
		
		if (isset($_SESSION['ConneKton_session']['admin_token']) && $_SESSION['ConneKton_session']['admin_token'] != "")
		{
			
			$data = array();
			if (isset($_GET['id']) && $_GET['id'] != "") {
		
				$planId = $_GET['id'];
				$data['planById'] = $this->user_service->getPlanById($planId);
				
				if($data['planById'] ==0)
				{
					
					redirect(base_url() . 'plan');
				}
			}
			$this->load->view('admin/plan', $data);
		} else {
		
			$this->session->set_flashdata('error', 'You must login ');
			redirect(base_url() . 'admin');
		}
		
		
		
	}
	public function view_plan()
	{
		if (isset($_SESSION['ConneKton_session']['admin_token']) && $_SESSION['ConneKton_session']['admin_token'] != "") {
				$this->load->view('admin/view_plan');
		} else {
						$this->session->set_flashdata('error', 'You must login ');
			redirect(base_url() . 'admin');
		}
	}
	public function view_article()
	{
		if (isset($_SESSION['ConneKton_session']['admin_token']) && $_SESSION['ConneKton_session']['admin_token'] != "") {
			$this->load->view('admin/view_article');
		} else {
						$this->session->set_flashdata('error', 'You must login ');
			redirect(base_url() . 'admin');
		}
		
	}
	public function client_list()
	{
		if (isset($_SESSION['ConneKton_session']['admin_token']) && $_SESSION['ConneKton_session']['admin_token'] != "") {
			$this->load->view('admin/client_list');
		} else {
						$this->session->set_flashdata('error', 'You must login ');
			redirect(base_url() . 'admin');
		}
		
	}
	public function user_list()
	{
		if (isset($_SESSION['ConneKton_session']['admin_token']) && $_SESSION['ConneKton_session']['admin_token'] != "") {
			$this->load->view('admin/user_list');
		} else {
						$this->session->set_flashdata('error', 'You must login ');
			redirect(base_url() . 'admin');
		}
		
	}
	public function approved_kp_list()
	{
		if (isset($_SESSION['ConneKton_session']['admin_token']) && $_SESSION['ConneKton_session']['admin_token'] != "") {
			$this->load->view('admin/approved_kp_list');
		} else {
						$this->session->set_flashdata('error', 'You must login ');
			redirect(base_url() . 'admin');
		}
		
	}
	public function unapproved_kp_list()
	{
		if (isset($_SESSION['ConneKton_session']['admin_token']) && $_SESSION['ConneKton_session']['admin_token'] != "") {
			$this->load->view('admin/unapproved_kp_list');
		} else {
						$this->session->set_flashdata('error', 'You must login ');
			redirect(base_url() . 'admin');
		}
		
	}
	public function employer_list()
	{
		if (isset($_SESSION['ConneKton_session']['admin_token']) && $_SESSION['ConneKton_session']['admin_token'] != "") {
			$this->load->view('admin/employer_list');
		} else {
						$this->session->set_flashdata('error', 'You must login ');
			redirect(base_url() . 'admin');
		}
		
	}
	public function view_question()
	{
		if (isset($_SESSION['ConneKton_session']['admin_token']) && $_SESSION['ConneKton_session']['admin_token'] != "") {
			$this->load->view('admin/view_question');
		} else {
						$this->session->set_flashdata('error', 'You must login ');
			redirect(base_url() . 'admin');
		}
		
	}
}
