<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
	Title: 	DashboardManager
	Auther: Ujjavala saha
	functionality: this controller for load view pages of web
*/
class DashboardManager extends CI_Controller {

	 public function __construct()
    {
        parent::__construct();
        $this->config->load('myConstant');
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database();
        $this->load->service('User_service');
        if(!isset($this->session->userdata('connectOnWeb')['token'])){
			redirect(base_url().'loginView', 'refresh');
		}
    }

	// template to load header footer and body
    public function template($path,$data='',$view_type=""){
	    $data = array();
		$headers = array();
		//print_r($_SESSION);die();
		$headers['Authorization'] = $_SESSION['connectOnWeb']['token']; 

    	$result = tokenVerification($headers);
		if (isset($result)) {
			$data['role'] = $result['role'];
			$data['users'] =  $this->user_service->getDataWhere('user','full_name,profile_pic',array('user_id' =>$result['id']));
		}   
	    $this->load->view('web/includes/inner_header',$data);
	    $this->load->view($path);
	    $this->load->view('web/includes/inner_footer');
    }

	public function userDashboardView()
	{
		
		$this->template('web/dashboardView');
	}
	public function clientQuesion()
	{
		
		$this->template('web/clientQuesion');
	}

	public function article()
	{	
		$this->template('web/article_view');
	}

	public function userLogout()
	{	
		unset($_SESSION["connectOnWeb"]);
        redirect(base_url().'loginView');
	}

}
