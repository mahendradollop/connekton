<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
	Title: 	Web_Manager
	Auther: Mahendra kushwah
	functionality: this controller for load view pages of web
*/
class LoginSignupController extends CI_Controller
{	
	 public function __construct()
	  {
	    parent::__construct();
	    $this->config->load('myConstant');
	    $this->load->helper(array('form', 'url', 'Validation_helper'));
	    $this->load->library('form_validation');
	    $this->load->database('');
	    $this->load->service('User_service');
	    $this->load->library('Authorization_Token');
	    $this->load->helper('date');
	    date_default_timezone_set("Asia/Kolkata");
	  }


	  	public function checkPhone(){
	  		$phone_number = $this->input->get_post("phone_number")!=""?$this->input->get_post("phone_number"):"";
	  		if(isset($phone_number) && $phone_number!="")
			{
				$check_mobile = $this->user_service->check_user_mobile($phone_number,'1');
				if(!empty($check_mobile)){
					echo "false"; 
				}else
				{
					echo "true";
				}

			}
	  	}
	  	public function checkEmail(){
	  		$email = $this->input->get_post("email")!=""?$this->input->get_post("email"):"";
	  		if(isset($email) && $email!="")
			{
				$check_mail = $this->user_service->check_email($email,'user');
				if(!empty($check_mail)){
					echo "false"; 
				}else
				{
					echo "true";
				}

			}
	  	}
		public function sendOtp(){

			$json = array();
			if ($this->input->post()) {
				$mobile = $this->input->get_post("mobile")!=""?$this->input->get_post("mobile"):"";
				if(isset($mobile) && $mobile!="")
				{
					$check_mobile = $this->user_service->check_mobile($mobile);
					if(!empty($check_mobile)){
						$result = $this->user_service->update_otp($mobile);
					}else{

						$result =  $this->user_service->insert_otp($mobile);
					}
					if ($result) {
			         	$status=200;
	        		   	$message='Success';
			        } else {
			          $status=1;
	        		  $message='Something Went Wrong!';
			        }

				}else{
					$status=1;
			   		$message='Field must be requird';
				}
			}else{
				$status=1;
		   		$message=$this->lang->line('Unauthorized Access');//"UnAuthorized Access";

			}
			echo json_encode(array("status"=>$status, "message"=>$message, "data"=>''));
		}
		public function verifyOtp(){

			$json = array();
			if ($this->input->post()) {
				$otp = $this->input->get_post("otp")!=""?$this->input->get_post("otp"):"";
				$mobile = $this->input->get_post("mobile")!=""?$this->input->get_post("mobile"):"";

				$data = array(
						'otp'=>$otp,
						'mobile'=>$mobile
					);

				if(isset($mobile) && $mobile!="" && isset($otp) && $otp!="")
				{
					$verify_otp = $this->user_service->verify_otp($data);
					if(!empty($verify_otp)){

						$status=200;
	        		   	$message='Success';
					}else{

						$status=1;
	        		   	$message='Invalid otp.';
					}
				}else{
					$status=1;
			   		$message='Field must be requird';
				}
			}
			else {
				$status=1;
			   	$message=$this->lang->line('Unauthorized Access');//"UnAuthorized Access";
			}
			echo json_encode(array("status"=>$status, "message"=>$message, "data"=>$data));
		}

		public function userRegister(){
			$json = array();
			if ($this->input->post()) {
				$user_role = $this->input->get_post("user_role")!=""?$this->input->get_post("user_role"):"";
				$full_name = $this->input->get_post("full_name")!=""?$this->input->get_post("full_name"):"";
				$phone_number = $this->input->get_post("phone_number")!=""?$this->input->get_post("phone_number"):"";
				$password = $this->input->get_post("password")!=""?$this->input->get_post("password"):"";

				$data = array(
						'user_role'=>$user_role,
						'full_name'=>$full_name,
						'phone_number'=>$phone_number,
						'password'=>md5($password)
					);

				if(isset($user_role) && $user_role!="" && isset($full_name) && $phone_number!="" && isset($phone_number) && $full_name!="" && isset($password) && $password!="")
				{
					$check_mobile = $this->user_service->check_user_mobile($phone_number,'1');
					if(empty($check_mobile)){
						$user = $this->user_service->register_user($data);
						
						if(!empty($user)){

							$status=200;
		        		   	$message='Success';
		        		   	$data = $user;
		        		   	$cookie_array = array('user_id'=>$data,'user_role'=>$user_role);
		        		   	set_cookie('register_user_cookie',json_encode($cookie_array), 3600*2);	

						}else{

							$status=1;
		        		   	$message='Something went wrong';
		        		   	$data = "";
						}
					}else if(!empty($check_mobile) && $check_mobile['isRegisterSuccessfully']=='0'){

						$where = array('user_id'=>$check_mobile['user_id']);
						$user = $this->user_service->update_register_user($data,$where,'user');
						if(!empty($user)){

							$status=200;
		        		   	$message='Success';
		        		   	$data = $check_mobile['user_id'];
		        		   	$cookie_array = array('user_id'=>$check_mobile['user_id'],'user_role'=>$user_role);
		        		   	set_cookie('register_user_cookie',json_encode($cookie_array), 3600*2);	


						}else{

							$status=1;
		        		   	$message='Something went wrong';
		        		   	$data = "";
						}
					
					}else{
						$status=1;
		        		$message='Already Exist';
		        		$data = "";
					}
				}else{
					$status=1;
			   		$message='Field must be requird';
				}
			}
			else {
				$status=1;
			   	$message=$this->lang->line('Unauthorized Access');//"UnAuthorized Access";
			}
			echo json_encode(array("status"=>$status, "message"=>$message, "data"=>$data));

		}

		public function updateUserRegister(){

			$register_user_cookie = json_decode(get_cookie('register_user_cookie'),true);
			$user_role = $register_user_cookie['user_role'];
			$cookie_user_id = $register_user_cookie['user_id'];

			$user_id = $this->input->get_post("user_id")!=""?$this->input->get_post("user_id"):"";
			$email = $this->input->get_post("email")!=""?$this->input->get_post("email"):"";
			$country_id = $this->input->get_post("country")!=""?$this->input->get_post("country"):"";
			$state_id = $this->input->get_post("state")!=""?$this->input->get_post("state"):"";
			$city_id = $this->input->get_post("city")!=""?$this->input->get_post("city"):"";
			$location = $this->input->get_post("location")!=""?$this->input->get_post("location"):"";
			$latitude = $this->input->get_post("latitude")!=""?$this->input->get_post("latitude"):"";
			$longitude = $this->input->get_post("longitude")!=""?$this->input->get_post("longitude"):"";
			$education = $this->input->get_post("education")!=""?$this->input->get_post("education"):"";
			$designation_name = $this->input->get_post("designation_name")!=""?$this->input->get_post("designation_name"):"";
			$experience = $this->input->get_post("experience")!=""?$this->input->get_post("experience"):"";
			$organization_name = $this->input->get_post("organization_name")!=""?$this->input->get_post("organization_name"):"";

			$community = $this->input->get_post("community")!=""?$this->input->get_post("community"):"";
			$skills = $this->input->get_post("skills")!=""?$this->input->get_post("skills"):"";

			///////////employee detail////////////
			$type =  $this->input->get_post("type")!=""? $this->input->get_post("type"):"";
			$job_companyname =  $this->input->get_post("job_companyname")!=""? $this->input->get_post("job_companyname"):"";
			$job_designation =  $this->input->get_post("job_designation")!=""? $this->input->get_post("job_designation"):"";
			$job_experience =  $this->input->get_post("job_experience")!=""? $this->input->get_post("job_experience"):"";
			$bussiness_name =  $this->input->get_post("bussiness_name")!=""? $this->input->get_post("bussiness_name"):"";
			$bussiness_field =  $this->input->get_post("bussiness_field")!=""? $this->input->get_post("bussiness_field"):"";
			$employee_size =  $this->input->get_post("employee_size")!=""? $this->input->get_post("employee_size"):"";
			///////////employee detail////////////
			
	        $user_data = array(
        			"email"=>$email,
					"country_id"=>$country_id,
					"state_id"=>$state_id,
					"city_id"=>$city_id,
					"location"=>$location,
					"latitude"=>$latitude,
					"longitude"=>$longitude,
					"email"=>$email,
					"education"=>$education,
					"isRegisterSuccessfully"=>1,
        		);

	        if (isset($_FILES) && !empty($_FILES['profile_pic']['name'])) {
	            $image_data=$_FILES['profile_pic'];
	            $path='uploads/users/';
	            $user_data['profile_pic']=$path.$this->Common_model->upload_image($image_data,1,$path);
	        }
	        if (isset($_FILES) && !empty($_FILES['resume']['name'])) {
	            $image_data=$_FILES['resume'];
	            $path='uploads/resume/';
	            $user_data['resume']=$path.$this->Common_model->upload_image($image_data,1,$path);
	        }
			$where = array("user_id"=>$cookie_user_id);
			if ($this->input->post()) {
				if($user_id!="" && $email!="" && $country_id!="" && $state_id!="" && $city_id!="" && $location!=
				"" &&  $latitude!="" && $longitude!="") {


					$check_mail = $this->user_service->check_email($email,'user',$cookie_user_id);
					if(empty($check_user)){
				        
				        ////////////////////Employer role //////////////////
				        if($user_role=="Employer"){
							///////////employeeDetail/////////////////
							

							$employee_data = array(
								'type'=>$type,
								'user_id'=>$user_id,
							);
							if($type=='Job'){
								$employee_data['job_companyname']=$job_companyname;
								$employee_data['job_designation']=$job_designation;
								$employee_data['job_experience']=$job_experience;
							}else{
								$employee_data['bussiness_name']=$bussiness_name;
								$employee_data['bussiness_field']=$bussiness_field;
								$employee_data['employee_size']=$employee_size;
							}
							if(($type=='Job' &&  isset($job_companyname) && $job_companyname!="" && isset($job_designation) && $job_designation!="" && isset($job_experience) && $job_experience!="" ) || ($type=='Bussiness' &&  isset($bussiness_name) && $bussiness_name!="" && isset($bussiness_field) && $bussiness_field!="" && isset($employee_size) && $employee_size!="" ))
							{
								$result = $this->user_service->insert_register_user($employee_data,'employer');
							}else{
								$status=1;
							   	$message='Field must be requird';
							   	$data =$employee_data; 
							}	
							///////////employeeDetail/////////////////
						}
						////////////////////user/KP's role //////////////////
						else if($user_role=="User" || $user_role=="KP")
						{
							///////////experience_data/////////////////
							 $experience_data = array(
		        				'user_id'=>$user_id,
		        				'designation_name'=>$designation_name,
								'experience'=>$experience,
								'organization_name'=>$organization_name,
		        			);
							 if($designation_name!="" && $experience!="" && $organization_name!=""){
							 	$result = $this->user_service->insert_register_user($experience_data,'user_experience');
							 }else{
								$status=1;
							   	$message='Field must be requird';
							   	$data =$experience_data; 
							}	
							///////////experience_data/////////////////
							$result = $this->user_service->communitySkills($user_id,$community,$skills);

						}
						$result = $this->user_service->update_register_user($user_data,$where,'user');
				        if($result){
				        	if($user_role!="KP"){
				        		delete_cookie("register_user_cookie");
				        	}
				   	        $status=200;
				    	    $message="success"; 
				            $data = $user_role;
				   	    }else{
				   	        $status=1;
				   		    $message="Something Went Wrong!";
				   		    $data ="";
				   	    }
					}else{
						$status=1;
		        		$message='Already Exist Email Id';
		        		$data = "";
					}
				}else{
					$status=1;
				   	$message='Field must be requird';
				   	$data =$user_data; 
				}
					
			}else{
				$status=1;
			   	$message=$this->lang->line('Unauthorized Access');//"UnAuthorized Access";
			}
		   	echo json_encode(array("status"=>$status, "message"=>$message, "data"=>$data));
			die;
		}
	
		//////////////country data////////////////
		public function get_country(){

			$status="";
	   		$message="";
	   		$data = "";

			$sql="SELECT *
	   	          FROM country";
	   	    $result = $this->Common_model->executeSql($sql,'result_array');
	   	    if($result){
	   	        $status=200;
	    	    $message="success"; 
	            $data = $result;
	   	    }else{
	   	        $status=1;
	   		    $message="Something Went Wrong!";
	   	    }
	   	    
		   	echo json_encode(array("status"=>$status, "message"=>$message, "data"=>$data));

		}
		//////////////state data////////////////
		public function get_state(){
			$status="";
	   		$message="";
	   		$data = "";

			$country_id = $this->input->get_post("country_id")!=""?$this->input->get_post("country_id"):"";
			
			$user_data=array(
				'country_id'=>$country_id,
				);
	        if ($this->input->post()) {
			   
				if($country_id!="")
			   	{
			   	    $sql="SELECT *
			   	          FROM states
			   	          WHERE country_id='$country_id'";
			   	    $result = $this->Common_model->executeSql($sql,'result_array');
			   	    if($result){
			   	        $status=200;
	    	    	    $message="success"; 
	                    $data = $result;
			   	    }else{
			   	        $status=1;
			   		    $message="Something Went Wrong!";
			   	    }
			   	}
			   	else
			   	{
			   		$status=1;
			   		$message="Field must be requird";
			   		$data =$this->Common_model->getRowArray($user_data);
			   	}
			} else {
				$status=1;
			   	$message="UnAuthorized Access";
			}
		   
		   	echo json_encode(array("status"=>$status, "message"=>$message, "data"=>$data));
			
		}
		//////////////city data////////////////
		public function get_city(){
			$status="";
	   		$message="";
	   		$data = "";
			$dataArray = [];

			$state_id = $this->input->get_post("state_id")!=""?$this->input->get_post("state_id"):"";
			$country_id = $this->input->get_post("country_id")!=""?$this->input->get_post("country_id"):"";
			
			$user_data=array(
				'state_id'=>$state_id,
				'country_id'=>$country_id,
				);
	        if ($this->input->post()) {
			   
				if($state_id!="" )
			   	{
			   	    $sql="SELECT *
			   	          FROM city
			   	          WHERE state_id='$state_id'";
			   	    $result = $this->Common_model->executeSql($sql,'result_array');
			   	    
			   	    if($result){
			   	        $status=200;
	    	    	    $message="success"; 
	                    $data = $result;
			   	    }else{
			   	        $status=1;
			   		    $message="Something Went Wrong!";
			   	    }
			   	}
			} else {
				$status=1;
			   	$message="UnAuthorized Access";
			}

		   	echo json_encode(array("status"=>$status, "message"=>$message, "data"=>$data , ));
		}
		//////////////community data////////////////
		public function get_comunity(){

			$status="";
	   		$message="";
	   		$data = "";

			$sql="SELECT *
	   	          FROM community";
	   	    $result = $this->Common_model->executeSql($sql,'result_array');
	   	    if($result){
	   	        $status=200;
	    	    $message="success"; 
	            $data = $result;
	   	    }else{
	   	        $status=1;
	   		    $message="Something Went Wrong!";
	   	    }
	   	    
		   	echo json_encode(array("status"=>$status, "message"=>$message, "data"=>$data));

		}
		//////////////skills data////////////////
		public function get_skills(){
			$status="";
	   		$message="";
	   		$data = "";

			$community_id = $this->input->get_post("community_id")!=""?$this->input->get_post("community_id"):"";
			
			$user_data=array(
				'community_id'=>$community_id,
				);
	        if ($this->input->post()) {
			   
				if($community_id!="")
			   	{
			   	    $sql="SELECT *
			   	          FROM skills
			   	          WHERE community_id IN($community_id)";
			   	    $result = $this->Common_model->executeSql($sql,'result_array');
			   	    if($result){
			   	        $status=200;
	    	    	    $message="success"; 
	                    $data = $result;
			   	    }else{
			   	        $status=1;
			   		    $message="Something Went Wrong!";
			   	    }
			   	}
			   	else
			   	{
			   		$status=1;
			   		$message="Field must be requird";
			   		$data =$this->Common_model->getRowArray($user_data);
			   	}
			} else {
				$status=1;
			   	$message="UnAuthorized Access";
			}
		   
		   	echo json_encode(array("status"=>$status, "message"=>$message, "data"=>$data));
			
		}
		


	}

?>