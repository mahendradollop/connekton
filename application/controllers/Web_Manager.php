<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
	Title: 	Web_Manager
	Auther: Mahendra kushwah
	functionality: this controller for load view pages of web
*/
class Web_Manager extends CI_Controller {

	function __construct()
	{
		parent::__construct();
			if(isset($this->session->userdata('connectOnWeb')['token'])){
			redirect(base_url().'userDashboardView', 'refresh');
		}
	}

	// template to load header footer and body
	public function template($path,$data=''){
	    $this->load->view('web/includes/header');	    
	    $this->load->view($path,$data);
	    $this->load->view('web/includes/footer');
    }

   

	// load home page
	public function index()
	{
		$this->template('web/index');
	}

	// load signup page
	public function signUpView()
	{
		// $this->load->template('web/signUpView');
		$this->template('web/signUpView');
	}

	public function loginView()
	{
		// $this->load->template('web/loginView');
		$this->template('web/loginView');
	}

	// load Registration page
	public function registrationView()
	{
		$data = array();

		$register_user_cookie = json_decode(get_cookie('register_user_cookie'),true);
		if(isset($register_user_cookie['user_id']) && $register_user_cookie['user_id']!="" && isset($register_user_cookie['user_role']) && $register_user_cookie['user_role']!=""){

			$data['register_user_cookie'] = $register_user_cookie;
			// $this->load->template('web/registrationView',$data);
			$this->template('web/registrationView',$data);
		}
		else{
			redirect(base_url().'signUpView');
		}
	}
	public function planView(){
		$data = array();

		$register_user_cookie = json_decode(get_cookie('register_user_cookie'),true);
		if(isset($register_user_cookie['user_id']) && $register_user_cookie['user_id']!="" && isset($register_user_cookie['user_role']) && $register_user_cookie['user_role']="KP"){
			$data['register_user_cookie'] = $register_user_cookie;
			$this->template('web/planView',$data);
		}
		else{
			redirect(base_url().'signUpView');
		}
	}

}
