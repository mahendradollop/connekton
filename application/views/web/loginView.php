<!DOCTYPE html>
<html>
<head>
	<title>Connekton</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/web/css/bootstrap-material-design.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/web/css/style.css">
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/logo.png" >
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/web/css/style_signup.css" >

	
	<script src="<?php echo base_url() ?>assets/web/js/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url() ?>assets/web/js/custom.js"></script>
   
</head>

<body>

<!-- ///////////// Login sign up section ////////// -->


<main class="login_sign_up_sec_pg">

	<div class="container-fluid px-md-0">	
		<div class="login_sign_up_sec_card_bg main-forms-box p-0">
			<div class="row mx-md-0">
				<div class="col-md-6 left_fixed_img ">
					<div class="login_sign_up_sec_img_vctr left-side-img position-relative"> 
						<img class="img-fluid" src="<?php echo base_url('assets/web/images/')?>/login.png">
					</div>
				</div>
				<div class="col-md-6">
					<div class="login_container_main01 ragister-form shadow-0">
						 <div class="reg_top_logo pb-4">
	                        <a href="index.html" class="site-logo">
	                           <img src="<?php echo base_url('assets/web/images/')?>1_logo.png" class="img-fluid">
	                        </a>
	                     </div>
						<div class="login_title mb-3">
							<h3 class="font-700 color-dark mb-3">
								Login Now
							</h3>
							<p class="color-light-gray">
								Enter Your Email id and Password to login you account.
							</p>
						</div>
						<div style="color:red" class="error_msg"></div>
						<form class="custom_fomr_login_sign_up" id="login_form">
						    <div class="form-group mb-4">
						    	<label for="phone_no10" class="col-sm-2 col-form-label">Phone Number or Email</label>
						    	<div class="w-100">
						      		<input type="text" class="form-control" id="emailAndMobile" placeholder="Phone no. or email" name="emailAndMobile">
						    	</div>
						  	</div>
							<div class="form-group mb-4">
						    	<label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
						    	<div class="w-100">
						      		<input type="password" class="form-control" id="password" placeholder="******"  name="password">
						    	</div>
							</div>
							<div class="form-group mb-5">
						    	<a href="forgot-password.php" class="color-primary forgot_pass_link_btn">
						    		Forgot Password?
						    	</a>
							</div>	
							<div class="form-group">
						    	<button class="btn btn-primary login_pg_btn_lyt">
						    		Login Now
						    	</button>
							</div>
							<div class="form-group text-center">
						    	<span class="font-13">
						    		Don't have an Account? 
						    		<a href="sign_up.php" class="color-primary ml-1 font-500 font-15">Sign Up</a> 
						    	</span>
							</div>					 
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="btm_cpyrght_sec_login_pg mt-0 d-inline-block w-100 text-center d-inline-flex p-3">
		<p class="font-13 font-500 mb-0">
			Copyright ©2020 connekton. All rights reserved.
		</p>
		<p class="font-13 font-500 ml-auto mb-0">
			<a href="javascript:void(0)" class="color-primary">Terms of Use</a>
			 <span class="color-primary">|</span> 
			<a href="javascript:void(0)" class="color-primary">Privacy Policy</a>
		</p>
	</div>
</main>

<!-- ////////// -------------- /////////// -->


</body>

 <script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.toaster.js')?>"></script>

<script type="text/javascript">
	
	
  $(document).ready(function(){
  	 $( "#login_form" ).validate({
           rules: {
            emailAndMobile: "required",
	        password:{
                required:true,
                minlength:6,
                maxlength:16,
            },
           },
            messages: {
                emailAndMobile: "Phone no. or email is required",
                password:{
	                required:   "Password is required",
                    minlength:  "Password should be minimum 6 characters",
                    maxlength:  "Password should be maximum 16 characters",
	            },
            },
         submitHandler: function(form)
            {
                $.ajax({
                  url: '<?php echo base_url('UserLogin'); ?>',
                  type: 'POST',
                  data:  new FormData($('#login_form')[0]),
                  cache:false,
                  contentType: false,
                  processData: false,
                  success: function(response) {
                  	$.toaster({ priority : 'success', title : 'Success', message : response.message });
                  	setTimeout(function() { 
                        window.location.href = "<?php echo base_url('userDashboardView'); ?>";
                    }, 2000);

                },
                error: function (xhr, exception) {
                	$('.error_msg').html(xhr.responseJSON.message);
                	setTimeout(function() { $(".error_msg").html(''); }, 15000);
               	    $.toaster({ priority : 'danger', title : 'Notice', message : xhr.responseJSON.message });
                }
              });
            }
         });  
    });
</script>

</html>




