<!DOCTYPE html>
<html>
   <head>
    <meta charset="utf-8">
      <link rel="shortcut icon" href="<?php echo base_url('assets/web/images/')?>/favicon.png">
      <title>Connekton</title>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link rel="shortcut icon" href="/favicon.ico">
      <link rel="stylesheet" href="<?php echo base_url('assets/web/css/')?>bootstrap-material-design.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/')?>style.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/')?>/custom_svg_icons.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/web/css/')?>select2.min.css">
   </head>
<body>
 <!-- inner head start here  -->
   <nav class="navbar navbar-expand-lg ckt_inner_header user_header">
       <div class="container-fluid px-0">
            <a class="navbar-brand" href="<?php base_url();?>">
                <img src="<?php echo base_url('assets/web/images/')?>/logo.png">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mx-auto align-items-center">
                    <li class="nav-item active">
                        <a class="nav-link h_link" href="user_project.php"><span class="icons icon-briefcase"></span> Projects</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link h_link" href="user_question.php"><span class="icons icon-question"></span> Questions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link h_link d-none" href="#"><span class="icons icon-notes"></span> Article</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link h_link" href="user_job_post.php"><span class="icons icon-folder"></span> Job</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link h_link" href="<?php base_url();?>article"><span class="icons icon-unity"></span> Article</a>
                    </li>
                    <?php if($role=='Users'){?>
                    <li class="nav-item">
                        <a class="nav-link post_btn" href="#">Post a Resume</a>
                    </li>
                  <?php } ?> 
                </ul>
            </div>
            <div class="right_navadr ml-auto text-right d-inline-flex w-auto" id="navbarNav">
                <ul class="navbar-nav align-items-center">
                    <li class="nav-item active">
                        <a class="nav-link h_link notification" href="#"><span class="icons icon-notification mr-0"></span><span class="noti_circle d-flex align-items-center rounded-circle justify-content-center">1</span></a>
                    </li>
                    <li class="nav-item dropdown user-profile">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="user_image d-inline-flex align-items-center ctk_overflow_hidden justofy-content-center rounded-circle">
                            <img src="<?php echo base_url('assets/web/images/')?>/user-img.png" class="img-fluid">
                        </span> <?php echo $users['full_name']?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="user_dashboard.php">Dashboard</a>
                            <a class="dropdown-item" href="#">My Workstream</a>
                            <a class="dropdown-item" href="#">Profile</a>
                            <a class="dropdown-item" href="<?php echo base_url('userLogout')?>">Logout</a>
                        </div>
                     </li>
                </ul>
            </div>
        </div>
    </nav>


<script type="text/javascript">
    var BASE_URL = "<?php echo base_url(); ?>";
    var token = "<?php echo isset($this->session->userdata('connectOnWeb')['token'])?$this->session->userdata('connectOnWeb')['token']:''; ?>";
</script>