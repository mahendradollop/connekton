<!DOCTYPE html>
<html>
<head>
	<title>Connekton</title>

<link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/web/css/style.css">
<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/logo.png" >
</head>

<body>

<!-- ///////////// Navigation bar ////////// -->

<div class="ck_top_nav_info">
	<div class="container-fluid">
		<div class="left_mail_cal_info">
			<a href="javascript:void(0)" class="font-500">
				<span class="material-icons">mail</span>
				<span>support@connekton.com</span>
			</a>
		</div>
		<div class="right_socl_info">
			<div class="social_sec">
				<ul>
					<li>
						<a href="javascript:void(0)">
							<img width="20" src="<?php echo base_url(); ?>assets/web/images/Social/facebook.svg">
						</a>
					</li>
					<li>
						<a href="javascript:void(0)">
							<img width="20" src="<?php echo base_url(); ?>assets/web/images/Social/linkedin.svg">
						</a>
					</li>
					<li>
						<a href="javascript:void(0)">
							<img width="20" src="<?php echo base_url(); ?>assets/web/images/Social/twitter.svg">
						</a>
					</li>
					<li class="color-light">|</li>
					<li class="login_sign_up_sec">
						<a href="<?php echo base_url('loginView');?>">
							<span>Login</span>
						</a>
					</li>
					<li class="login_sign_up_sec sign_up_btn_hm">
						<a href="<?php echo base_url('signUpView'); ?>">
							<span class="color-primary">Sign Up</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<nav class="navbar navbar-expand-lg navbar_main_conktn">
  <a class="navbar-brand" href="index.html">
  	<img src="<?php echo base_url(); ?>assets/logo.png">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Employers</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Candidates</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Community</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">ConneKton Insights</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">How We Work</a>
      </li>
    </ul>
  </div>
</nav>

<!-- ///////------------////////// -->

<script type="text/javascript">
    var BASE_URL = "<?php echo base_url(); ?>";
    var token = "<?php echo isset($this->session->userdata('connectOnWeb')['token'])?$this->session->userdata('connectOnWeb')['token']:''; ?>";
</script>