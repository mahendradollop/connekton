</body>
   <script src="<?php echo base_url('assets/web/js/')?>jQuery.min.js"></script>
   <script src="<?php echo base_url('assets/web/js/')?>popper.js"></script>
   <script src="<?php echo base_url('assets/web/js/')?>bootstrap-material-design.js"></script>
   <script src="<?php echo base_url('assets/web/js/')?>select2.min.js"></script>
   <script>
      $(document).ready(function() {
          $('body').bootstrapMaterialDesign(); 

          // job page filte
          $('#job-filter').click(function (){
            $('.add_question_box').slideToggle('');
            console.log('slide');
          });

          $('.jod_detail_collapse').click(function (){
            $(this).toggleClass('jd_show');
            $('.job_detail_card').toggleClass('jd_body_show');
          });

          // bootstrap popup intitalize
          $('#myModal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
          })

          // community side bar see more
          $('.see_more').click(function (){
                $('.community_members_list').toggleClass('show');
                
                if ($('.community_members_list').hasClass('show')){
                  $(this).html('See Less');
                } else {
                  $(this).html('See More');
                }
          });

          $("#community_ids").select2({
            placeholder: "Select communities",
                allowClear: true
          });
      });
   </script>
   </html>