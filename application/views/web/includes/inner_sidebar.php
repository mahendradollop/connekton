<div class="dashboard_side_bar">
		<div class="my-wallet border_rounded_5">
			<h4 class="w_title d-inline-flex align-items-center w-100 font-14 font-600"><span class="wallet-circle"><img src="<?php echo base_url('assets/web/images/')?>/wallet.png" class="img-fluid"></span> Wallet</h4>
			<p class="Avilable-blance font-12 font-500 mb-0">Avilable Balance</p>
			<p class="amoun font-16 font-700 mb-0">$50.00</p>
		</div>
		<div class="general-links py-4 ">
			<h5 class="font-16 font-500 border-top pt-4">General</h5>
			<ul class="side-bar-links pl-0">
				<li><a href="#"><span class="material-icons mr-2">group</span> My Connection</a></li>
				<li><a href="#"><span class="material-icons mr-2">payments</span>Membership PLan</a></li>
				<li><a href="#"><span class="material-icons mr-2">history_edu</span> Transaction History</a></li>
				<li><a href="#"><span class="material-icons mr-2">settings</span> Settings</a></li>
				<li><a href="#"><span class="material-icons mr-2">confirmation_number</span>Bids</a></li>
			</ul>
		</div>
	</div>