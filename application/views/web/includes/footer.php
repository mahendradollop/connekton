<footer class="footer_Sec">
	<img class="footer_bg_img_cvr" src="<?php echo base_url(); ?>assets/web/images/footer_bg_img.png">
	<div class="container">
		<div class="row mx-0">
			<div class="col-md-3">
				<div class="footer_content_container">
					<h5>
						<img src="<?php echo base_url(); ?>assets/web/images/logo.png">
					</h5>
					<p class="mt-4 font-500 font-13">
						Lorem ipsum dolor sit amet,
						consectetur adipisicing elit, sed do
						eiusmod tempor incididunt ut labore
						et dolore magna aliqua.
					</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="footer_content_container">
					<h5 class="color-primary font-600">
						Browse
					</h5>
					<ul class="pl-0 mt-3">
						<li>
							<a class="font-500 font-13 color-dark" href="javascript:void(0)">
								Find Jobs
							</a>
						</li>
						<li>
							<a class="font-500 font-13 color-dark" href="javascript:void(0)">
								Find Freelancers
							</a>
						</li>
						<li>
							<a class="font-500 font-13 color-dark" href="javascript:void(0)">
								Projects
							</a>
						</li>
						<li>
							<a class="font-500 font-13 color-dark" href="javascript:void(0)">
								Buy Service
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-md-3">
				<div class="footer_content_container">
					<h5 class="color-primary font-600">
						Company
					</h5>
					<ul class="pl-0 mt-3">
						<li>
							<a class="font-500 font-13 color-dark" href="javascript:void(0)">
								Career
							</a>
						</li>
						<li>
							<a class="font-500 font-13 color-dark" href="javascript:void(0)">
								Contact Us
							</a>
						</li>
						<li>
							<a class="font-500 font-13 color-dark" href="javascript:void(0)">
								About Us
							</a>
						</li>
						<li>
							<a class="font-500 font-13 color-dark" href="javascript:void(0)">
								Articles
							</a>
						</li>
						<li>
							<a class="font-500 font-13 color-dark" href="javascript:void(0)">
								Blogs
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-md-3">
				<div class="footer_content_container">
					<h5 class="color-primary font-600">
						Get In Touch
					</h5>
					<ul class="pl-0 mt-3">
						<li>
							<a class="font-500 font-13 color-dark" href="javascript:void(0)">
								<img class="mr-2" src="<?php echo base_url(); ?>assets/web/images/Social/facebook_blue.svg" width="23">
								Facebook
							</a>
						</li>
						<li>
							<a class="font-500 font-13 color-dark" href="javascript:void(0)">
								<img class="mr-2" src="<?php echo base_url(); ?>assets/web/images/Social/linkedin_blue.svg" width="23">
								LinkdIn
							</a>
						</li>
						<li>
							<a class="font-500 font-13 color-dark" href="javascript:void(0)">
								<img class="mr-2" src="<?php echo base_url(); ?>assets/web/images/Social/twitter_blue.svg" width="23">
								Twitter
							</a>
						</li>						
					</ul>
				</div>
			</div>
			<div class="col-md-12 mt-3">
				<div class="copyright_container py-4 d-flex flex-wrap w-100" style="border-top: 1px solid #ccc;">
					<p class="font-13 font-500 mb-0 py-2">
						Copyright ©2021 connekton. All rights reserved.
					</p>
					<p class="font-13 font-500 ml-auto mb-0 py-2">
						<a href="javascript:void(0)" class="color-dark">Terms of Use</a>
						|
						<a href="javascript:void(0)" class="color-dark">Privacy Policy</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- ////////// -------------- /////////// -->

</body>

<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
 --><script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
<script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>

<script>

$(document).ready(function() {
	$('body').bootstrapMaterialDesign(); 
});

	function formatMoney(amount, decimalCount = 0, decimal = ".", thousands = ",") {
	  try {
	    decimalCount = Math.abs(decimalCount);
	    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
	    const negativeSign = amount < 0 ? "-" : "";
	    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
	    let j = (i.length > 3) ? i.length % 3 : 0;

	    return '$ '+negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
	  } catch (e) {
	    console.log(e)
	  }
	};



</script>

</html>