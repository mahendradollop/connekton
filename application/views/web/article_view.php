
    <section class="main_body theme_gary p_py_44 d-inline-block w-100">
        <div class="container">
            <div class="row">
                 <div class="col-md-3">  
                <div class="side_bar border_rounded_5 h-100">
                   <div class="community_members">
                        <h4 class="font-16 font-700 py-3 ">Communities</h4>
                        <div class="community_members_list mt-1">
                            <div id="accordion">
                                <!-- <div class="card">
                                    <div class="card-header active" id="headingOne">
                                      <h5 class="mb-0">
                                        <button class="btn btn-link font-14 p-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                         Application Development
                                        </button>
                                      </h5>
                                    </div>
                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                      <div class="card-body p_px_15">
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Leenda Jakhop</a>
                                                <span class="member-type font-10">Freelancer </span>
                                            </div>
                                        </div>
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Leenda Jakhop</a>
                                                <span class="member-type font-10">Freelancer </span>
                                            </div>
                                        </div>
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Leenda Jakhop</a>
                                                <span class="member-type font-10">Freelancer </span>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                      <h5 class="mb-0">
                                        <button class="btn btn-link collapsed font-14 p-0" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                         Artificial Intelligence
                                        </button>
                                      </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"      data-parent="#accordion">
                                      <div class="card-body  p_px_15">
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Leenda Jakhop</a>
                                                <span class="member-type font-10">Freelancer </span>
                                            </div>
                                        </div>
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Leenda Jakhop</a>
                                                <span class="member-type font-10">Freelancer </span>
                                            </div>
                                        </div>
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Leenda Jakhop</a>
                                                <span class="member-type font-10">Freelancer </span>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingfour">
                                      <h5 class="mb-0">
                                        <button class="btn btn-link collapsed font-14 p-0" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                         Arts and Entertainment 
                                        </button>
                                      </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                      <div class="card-body p_px_15">
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Sunny Jones </a>
                                                <span class="member-type font-10">Design</span>
                                            </div>
                                        </div>
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Mark Jorden  </a>
                                                <span class="member-type font-10">Sales & Marketing</span>
                                            </div>
                                        </div>
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Willy Scot </a>
                                                <span class="member-type font-10">Devlopment</span>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingfour">
                                      <h5 class="mb-0">
                                        <button class="btn btn-link collapsed font-14 p-0" data-toggle="collapse" data-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                         Audit
                                        </button>
                                      </h5>
                                    </div>
                                    <div id="collapsefour" class="collapse" aria-labelledby="headingfour" data-parent="#accordion">
                                      <div class="card-body  p_px_15">
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Sunny Jones </a>
                                                <span class="member-type font-10">Design</span>
                                            </div>
                                        </div>
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Mark Jorden  </a>
                                                <span class="member-type font-10">Sales & Marketing</span>
                                            </div>
                                        </div>
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Willy Scot </a>
                                                <span class="member-type font-10">Devlopment</span>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingfive">
                                      <h5 class="mb-0">
                                        <button class="btn btn-link collapsed font-14 p-0" data-toggle="collapse" data-target="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                         Auto
                                        </button>
                                      </h5>
                                    </div>
                                    <div id="collapsefive" class="collapse" aria-labelledby="headingfive" data-parent="#accordion">
                                      <div class="card-body  p_px_15">
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Sunny Jones </a>
                                                <span class="member-type font-10">Design</span>
                                            </div>
                                        </div>
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Mark Jorden  </a>
                                                <span class="member-type font-10">Sales & Marketing</span>
                                            </div>
                                        </div>
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Willy Scot </a>
                                                <span class="member-type font-10">Devlopment</span>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingsix">
                                      <h5 class="mb-0">
                                        <button class="btn btn-link collapsed font-14 p-0" data-toggle="collapse" data-target="#collapsesix" aria-expanded="false" aria-controls="collapsesix">
                                         Automation
                                        </button>
                                      </h5>
                                    </div>
                                    <div id="collapsesix" class="collapse" aria-labelledby="headingsix" data-parent="#accordion">
                                      <div class="card-body  p_px_15">
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Sunny Jones </a>
                                                <span class="member-type font-10">Design</span>
                                            </div>
                                        </div>
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Mark Jorden  </a>
                                                <span class="member-type font-10">Sales & Marketing</span>
                                            </div>
                                        </div>
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Willy Scot </a>
                                                <span class="member-type font-10">Devlopment</span>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" id="headingseven">
                                      <h5 class="mb-0">
                                        <button class="btn btn-link collapsed font-14 p-0" data-toggle="collapse" data-target="#collapseseven" aria-expanded="false" aria-controls="collapseseven">
                                         Banking
                                        </button>
                                      </h5>
                                    </div>
                                    <div id="collapseseven" class="collapse" aria-labelledby="headingseven" data-parent="#accordion">
                                      <div class="card-body  p_px_15">
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Sunny Jones </a>
                                                <span class="member-type font-10">Design</span>
                                            </div>
                                        </div>
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Mark Jorden  </a>
                                                <span class="member-type font-10">Sales & Marketing</span>
                                            </div>
                                        </div>
                                        <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="<?php echo base_url(); ?>assets/web/images/1_member.png" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">Willy Scot </a>
                                                <span class="member-type font-10">Devlopment</span>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                            <a href="#" class="d-block see_more font-14 border-bottom p_px_15 py-2">See More.....</a>
                   </div>
                </div>
            </div>

                <div class="col-md-9">
                    <div class="main_center_body">
                        <div class="posted-bar post-artical bg-white border_rounded_5 m_mb_20"  data-toggle="modal" data-target="#addartical">
                          <div type="button" class="posted-user-profile font-12 d-inline-flex align-items-center w-100 mb-2" data-toggle="modal" data-target="#addquestion">
                             <span class="user-image"> <img src="<?php echo base_url(); ?>assets/web/images/user-img.png" class="img-fluid"></span>
                                Client
                          </div>
                          <h5 class="font-20 font-600 m-0">Post Your Artical</h5>
                       </div>
                      
                        <div class="main_top_tiitle d-inline-flex align-items-center w-100 border_rounded_5 m_mb_20">
                              <h4 class="font-22 font-600 mb-0">Artical <span class="no_projects">100</span></h4>
                              <div class="filte_input ml-auto d-inline-flex align-items-center">
                                 <span class="font-14 font-500">Sort by</span>
                                 <span class="bmd-form-group is-filled">
                                    <div class="input-group ml-3">
                                       <select class="form-control custom-select" id="">
                                          <option selected="">Latest</option>
                                          <option>new Post</option>
                                          <option>old</option>
                                          <option>top 10</option>
                                       </select>
                                    </div>
                                 </span>
                              </div>
                           </div>
                        <div class="artical_boxes position-relative pb-2" id="showArticle">
                            <!-- <div class="inner_artical_splits_cards w-100 pb-3 bg-white m_mb_20 position-relative">
                                <span class="date">02:45 PM</span>
                                <div class="job_posted_by d-inline-block w-100 position-relative">
                                     <span class="posted_user_profile d-inline-flex align-items-center font-12 rounded-circle ">
                                     <img src="<?php echo base_url(); ?>assets/web/images/user-img.png" class="img-fluid" alt="">  
                                     </span>
                                     <h5>Brayce</h5>
                                     <p>UI Designer</p>
                                  </div>
                                <div class="artical_image pt-2">
                                    <img src="<?php echo base_url(); ?>assets/web/images/artical_1.png" class="img-fluid" alt="">
                                </div>
                                <div class="artical_body">
                                    <h4 class="question_name font-20 font-700 d-inline-flex w-auto pb-1">6 Common Mistakes Computer Users Must Avoid</h4>
                                    <p class="font-14">Are you a novice computer user? If so, then there are several things you'll want to learn to avoid. In article outlines the common issues faced by most computer users.</p>
                                </div>
                                <div class="like-comment-bar d-inline-flex align-items-center w-100 py-3 border-top">
                                    <a href="#" class="like mr-5"><span class="material-icons">thumb_up</span> 1.5k</a>
                                    <a href="#" class="dislike"><span class="material-icons">thumb_down</span> 0</a>
                                    <a href="#" onclick="return false;" class="comment ml-auto"><span class="material-icons">chat</span>Comments 10k</a>
                                </div>
                                <div class="comment-box">
                                    <div class="cmm-inner-body d-inline-block w-100 py-3 border-top">
                                        <div class="type-comment d-inline-flex align-items-center w-100 mb-3">
                                            <spna class="comm-user-img">
                                                <img src="">
                                            </spna>
                                            <div class="add-your-comment">
                                                <input type="text" class="form-control rounded-pills" placeholder="Type Your Comm">
                                                <a href="#" class="emoji"><img src="<?php echo base_url(); ?>assets/web/images/emoji.png" class=""></a>
                                            </div>
                                            <div class="comment-send">
                                                <a href="#" class="btn btn-send">
                                                    <span class="material-icons">send</span>
                                                </a>
                                            </div>
                                            </div>
                                            <div class="other-comments d-inline-flex w-100 mb-3">
                                                <spna class="comm-user-img">
                                                    <img src="">
                                                </spna>
                                                <div class="add-your-comment position-relative">
                                                    <span class="date">02:45 PM</span>
                                                   <span class="cmm-posted-name">Lisa</span>
                                                   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p> 
                                                </div>
                                            </div>
                                            <div class="other-comments d-inline-flex w-100 mb-3">
                                                <spna class="comm-user-img">
                                                    <img src="">
                                                </spna>
                                                <div class="add-your-comment position-relative">
                                                    <span class="date">02:45 PM</span>
                                                   <span class="cmm-posted-name">Lisa</span>
                                                   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="inner_artical_splits_cards w-100 pb-3 bg-white m_mb_20 position-relative">
                                <span class="date">02:45 PM</span>
                                <div class="job_posted_by d-inline-block w-100 position-relative">
                                     <span class="posted_user_profile d-inline-flex align-items-center font-12 rounded-circle ">
                                     <img src="<?php echo base_url(); ?>assets/web/images/user-img.png" class="img-fluid" alt="">  
                                     </span>
                                     <h5>Brayce</h5>
                                     <p>UI Designer</p>
                                  </div>
                                <div class="artical_image pt-2">
                                    <img src="<?php echo base_url(); ?>assets/web/images/artical_1.png" class="img-fluid" alt="">
                                </div>
                                <div class="artical_body">
                                    <h4 class="question_name font-20 font-700 d-inline-flex w-auto pb-1">6 Common Mistakes Computer Users Must Avoid</h4>
                                    <p class="font-14">Are you a novice computer user? If so, then there are several things you'll want to learn to avoid. In article outlines the common issues faced by most computer users.</p>
                                </div>
                                <div class="like-comment-bar d-inline-flex align-items-center w-100 py-3 border-top">
                                    <a href="#" class="like mr-5"><span class="material-icons">thumb_up</span> 1.5k</a>
                                    <a href="#" class="dislike"><span class="material-icons">thumb_down</span> 0</a>
                                    <a href="#" onclick="return false;" class="comment ml-auto"><span class="material-icons">chat</span>Comments 10k</a>
                                </div>
                                <div class="comment-box">
                                    <div class="cmm-inner-body d-inline-block w-100 py-3 border-top">
                                        <div class="type-comment d-inline-flex align-items-center w-100 mb-3">
                                            <spna class="comm-user-img">
                                                <img src="">
                                            </spna>
                                            <div class="add-your-comment">
                                                <input type="text" class="form-control rounded-pills" placeholder="Type Your Comm">
                                                <a href="#" class="emoji"><img src="<?php echo base_url(); ?>assets/web/images/emoji.png" class=""></a>
                                            </div>
                                            <div class="comment-send">
                                                <a href="#" class="btn btn-send">
                                                    <span class="material-icons">send</span>
                                                </a>
                                            </div>
                                            </div>
                                            <div class="other-comments d-inline-flex w-100 mb-3">
                                                <spna class="comm-user-img">
                                                    <img src="">
                                                </spna>
                                                <div class="add-your-comment position-relative">
                                                    <span class="date">02:45 PM</span>
                                                   <span class="cmm-posted-name">Lisa</span>
                                                   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p> 
                                                </div>
                                            </div>
                                            <div class="other-comments d-inline-flex w-100 mb-3">
                                                <spna class="comm-user-img">
                                                    <img src="">
                                                </spna>
                                                <div class="add-your-comment position-relative">
                                                    <span class="date">02:45 PM</span>
                                                   <span class="cmm-posted-name">Lisa</span>
                                                   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="inner_artical_splits_cards w-100 pb-3 bg-white m_mb_20 position-relative">
                                <span class="date">02:45 PM</span>
                                <div class="job_posted_by d-inline-block w-100 position-relative">
                                     <span class="posted_user_profile d-inline-flex align-items-center font-12 rounded-circle ">
                                     <img src="<?php echo base_url(); ?>assets/web/images/user-img.png" class="img-fluid" alt="">  
                                     </span>
                                     <h5>Brayce</h5>
                                     <p>UI Designer</p>
                                  </div>
                                <div class="artical_image pt-2">
                                    <img src="<?php echo base_url(); ?>assets/web/images/artical_1.png" class="img-fluid" alt="">
                                </div>
                                <div class="artical_body">
                                    <h4 class="question_name font-20 font-700 d-inline-flex w-auto pb-1">6 Common Mistakes Computer Users Must Avoid</h4>
                                    <p class="font-14">Are you a novice computer user? If so, then there are several things you'll want to learn to avoid. In article outlines the common issues faced by most computer users.</p>
                                </div>
                                <div class="like-comment-bar d-inline-flex align-items-center w-100 py-3 border-top">
                                    <a href="#" class="like mr-5"><span class="material-icons">thumb_up</span> 1.5k</a>
                                    <a href="#" class="dislike"><span class="material-icons">thumb_down</span> 0</a>
                                    <a href="#" onclick="return false;" class="comment ml-auto"><span class="material-icons">chat</span>Comments 10k</a>
                                </div>
                                <div class="comment-box">
                                    <div class="cmm-inner-body d-inline-block w-100 py-3 border-top">
                                        <div class="type-comment d-inline-flex align-items-center w-100 mb-3">
                                            <spna class="comm-user-img">
                                                <img src="">
                                            </spna>
                                            <div class="add-your-comment">
                                                <input type="text" class="form-control rounded-pills" placeholder="Type Your Comm">
                                                <a href="#" class="emoji"><img src="<?php echo base_url(); ?>assets/web/images/emoji.png" class=""></a>
                                            </div>
                                            <div class="comment-send">
                                                <a href="#" class="btn btn-send">
                                                    <span class="material-icons">send</span>
                                                </a>
                                            </div>
                                            </div>
                                            <div class="other-comments d-inline-flex w-100 mb-3">
                                                <spna class="comm-user-img">
                                                    <img src="">
                                                </spna>
                                                <div class="add-your-comment position-relative">
                                                    <span class="date">02:45 PM</span>
                                                   <span class="cmm-posted-name">Lisa</span>
                                                   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p> 
                                                </div>
                                            </div>
                                            <div class="other-comments d-inline-flex w-100 mb-3">
                                                <spna class="comm-user-img">
                                                    <img src="">
                                                </spna>
                                                <div class="add-your-comment position-relative">
                                                    <span class="date">02:45 PM</span>
                                                   <span class="cmm-posted-name">Lisa</span>
                                                   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<!-- add artical  Modal -->
<div class="modal fade bd-example-modal-lg" id="addartical" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add Artical</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <form id="article_form">
             <div class="modal-body">
                <div class="inner-form">    
                    <div class="form-group mb-2 bmd-form-group">
                      <label for="article_title" class="col-form-label bmd-label-static">Title</label>
                      <div class="w-100">
                         <input type="text" class="form-control" name="article_title" placeholder="Write Your Title">
                      </div>
                   </div>
                   <div class="form-group mb-2 bmd-form-group">
                      <label for="content" class="col-form-label bmd-label-static">Content</label>
                      <div class="w-100">
                         <textarea class="form-control h-140 p-3" placeholder="" name="content"></textarea>
                      </div>
                   </div>
                 <div class="form-group mb-3 bmd-form-group">
                      <label for="community_ids" class="col-form-label bmd-label-static">Communities</label>
                      <div class="w-100">
                         <select id="community_ids" name="community_ids[]" class="js-states form-control" multiple>
                            <!-- <option>Java</option>
                            <option>Javascript</option>
                            <option>PHP</option>
                            <option>Visual Basic</option> -->
                         </select>
                      </div>
                   </div>
                   <div class="w-100 Certificate-field position-relative">
                      <input id="cover_image" name="cover_image" type="file" class="form-control" placeholder="Certificate">
                      <label for="cover_image" class="Certificate-text d-flex align-items-center justify-content-center w-100">
                      <span class="text pl-3 font-500">Upload Image</span>
                      <span class="material-icons ml-4 pr-3">perm_media</span>
                      </label>
                   </div>
                   <label for="agrement" class="my-3 font-12">
                    <input type="radio" name="agrement" id="agrement"> Agrement <a href="#" class="primary-color">Terms & Conditions</a></label>
                   <div class="form-group mb-0">
                      <!-- <a href="#" class="btn btn btn-primary btn-block">Post</a> -->
                      <button type="submit" class="btn btn btn-primary btn-block">Post</button>
                   </div>
                </div>
             </div>
         </form>
      </div>
   </div>
</div>

<script src="<?php echo base_url() ?>assets/web/js/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/web/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/article_view.js')?>"></script>
