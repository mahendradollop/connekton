<?php
$user_role = $register_user_cookie['user_role'];
$image ="";
$experience ="";
if($user_role=="KP"){
   $image ="kp_register.png";
   $experience ="3 yrs";
}
else if($user_role=='Employer'){
   $image ="employe.png";
}
else if($user_role=='Users'){
   $image ="kp_register.png";
}else if($user_role=='Client'){
   $image ="client.png";
}


?>
<!DOCTYPE html>
<html>
   <head>
      <title>Connekton</title>
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/web/css/bootstrap-material-design.min.css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/web/css/style.css">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/web/css/select2.min.css">
      <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/logo.png" >
   </head>
   <!-- ragistration page start here  -->
   <main class="login_sign_up_sec_pg ragister-page">
      <div class="main-forms-box d-inline-block w-100">
         <div class="container_fluid px-md-0">

           
            <div class="row mx-md-0">
               <div class="col-md-6 pl-md-0 left_fixed_img">
                  <div class="left-side-img position-relative">
                        <img src="<?php echo base_url(); ?>assets/web/images/<?php echo $image;?>" class="">

                  </div>
               </div>
               <div class="col-md-6">
                  <div class="ragister-form Employer">
                     <div class="reg_top_logo pb-">
                        <a href="<?php echo base_url(); ?>" class="site-logo">
                           <img src="<?php echo base_url(); ?>assets/web/images/1_logo.png" class="img-fluid">
                        </a>
                     </div>
                     <div class="login_title mb-4">
                        <h1 class="font-700 color-dark mb-3 ft-z-28">Registration for <?php echo $user_role;?></h1>
                        <p>Create Your Account</p>
                     </div>
                     <div class="error_msg"></div>
                     <form class="custom_fomr_login_sign_up ragister-fileds" id="users_form" enctype="multipart/form-data">
                        <input type="hidden" name="user_id" value="<?php echo $register_user_cookie['user_id'];?>">
                        <input type="hidden" name="latitude" value="12121" id="latitude">
                        <input type="hidden" name="longitude" value="12121" id="longitude">
                        <div class="upload_resume_file_btn_col">
                           <div class="form-group mb-4 bmd-form-group file-input pt-0">
                              <input type="file" name="profile_pic" id="file-input" class="file-input__input" >
                              <label class="file-input__label mb-0" for="file-input">
                              <span class="material-icons">account_circle</span>
                              <span>Upload Profile</span></label>
                              <div id="img_div"></div>
                           </div>
                        </div>
                        <div class="form-group bmd-form-group">
                           <label class="col-form-label bmd-label-static">Email</label>
                           <div class="w-100">
                              <input type="email" name="email" class="form-control"> 
                           </div>
                        </div>
                        <div class="form-group mb-4 bmd-form-group">
                           <label class="col-form-label bmd-label-static">Address</label>
                           <div class="row">
                              <div class="col-md-4">
                                 <select type="select" class="custom-select country" name="country" onchange="getStateByCountryId(this.value)">
                                    <option value="">Country</option>
                                 </select>
                              </div>
                              <div class="col-md-4">
                                 <select type="select" class="custom-select state" name="state" onchange="getCityByStateId(this.value)" >
                                    <option value="">State</option>
                                 </select>
                              </div>
                              <div class="col-md-4">
                                 <select type="select" class="custom-select city" name="city"  >
                                    <option value="">City</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="form-group bmd-form-group">
                           <label class="col-form-label bmd-label-static">Location </label>
                           <div class="w-100 position-relative">
                              <input type="text" id="location" class="form-control" placeholder="Location" name="location" value="indore">
                              <span class="material-icons location-icon">  location_on </span>
                           </div>
                        </div>
                        <?php if($user_role=='Employer'){?>
                        <div class="form-group mb-4 row main-accordion position-relative">
                           <div class="col-md-6">
                              <div class="bmd-form-group collapse-main">
                                 <label for="job-bussiness" class="col-form-label bmd-label-static">Job</label>
                                 <div class="d-block w-100 custom-collapse collapse-show">
                                    <label class="btn btn-gray-outline collapse-btn">
                                       <input type="radio" name="type" id="optionsRadios2" value="Job" class="d-none job_type" checked>
                                       Job Type
                                     </label>
                                    <div class="collapse" id="job">
                                       <div class="card card-body">
                                          <input type="text" class="form-control mb-3" placeholder="Company name" name="job_companyname">
                                          <input type="text" class="form-control mb-3" placeholder="Designation" name="job_designation">
                                          <input type="text" class="form-control mb-3" placeholder="Work experience" name="job_experience">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>

                           <div class="col-md-6">
                              <div class="bmd-form-group collapse-main">
                                 <label for="job-bussiness" class="col-form-label bmd-label-static">Bussiness</label>
                                 <div class=" d-block w-100 custom-collapse">
                                    <label class="btn btn-gray-outline collapse-btn">
                                         <input type="radio" name="type" id="optionsRadios2" value="Bussiness" class="d-none job_type">
                                       Bussiness Type
                                     </label>
                                    <div class="collapse" id="collapseExample">
                                       <div class="card card-body">
                                          <input type="text" class="form-control mb-3" placeholder="Bussiness name" name="bussiness_name">
                                          <input type="text" class="form-control mb-3" placeholder="Bussiness Field" name="bussiness_field">
                                          <input type="text" class="form-control mb-3" placeholder="Employee Size" name="employee_size">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <?php } ?>
                        <?php if($user_role=="User" || $user_role=="KP"){?>
                           <div class="form-group mb-4 bmd-form-group">
                              <label for="education" class="col-form-label bmd-label-static">Highest Education</label>
                              <div class="w-100">
                                 <input type="text" class="form-control" placeholder="Highest Education" name="education">
                              </div>
                           </div>
                           <div class="form-group bmd-form-group">
                              <label class="col-form-label bmd-label-static">Community</label>
                              <div class="w-100">
                                 <select id="single" class="js-states form-control community" name="community[]" onchange="getSkillByCommunityId(this.value)" multiple>
                                 </select>
                              </div>
                           </div>

                           <div class="form-group mb-4 bmd-form-group">
                              <label for="partner" class="col-form-label bmd-label-static">Skills</label>
                              <div class="w-100">
                                 <select id="partners" class="js-states form-control skills" multiple name="skills[]">
                                 </select>
                              </div>
                           </div>
                           <div class="form-group mb-4 row">
                              <div class="col-md-8">
                                 <div class="bmd-form-group collapse-btn">
                                    <label for="Company-name" class="col-form-label bmd-label-static">Designation name</label>
                                    <div class="w-100">
                                       <input type="text" class="form-control" placeholder="Designation name" name="designation_name">
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="bmd-form-group collapse-btn">
                                    <label class="col-form-label bmd-label-static">Experience <?php echo $experience?></label>
                                    <div class="w-100">
                                       <input type="number" class="form-control" placeholder="Experience <?php echo $experience?>" name="experience">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        
                           <div class="form-group bmd-form-group">
                              <label class="col-form-label bmd-label-static">Organization name </label>
                              <div class="w-100">
                                 <input type="text" class="form-control" placeholder="Organization name" name="organization_name">
                              </div>
                           </div>
                           <div class="form-group bmd-form-group Certificate">
                              <div class="w-100 Certificate-field position-relative">
                                 <input id="Certificate" type="file" class="form-control" placeholder="Certificate" name="resume">
                                 <label for="Certificate" class="Certificate-text d-flex align-items-center justify-content-center w-100">
                                 <span class="text pl-3 font-500">Upload Resume</span>
                                 <span class="material-icons ml-4 pr-3">description</span>
                                 </label>
                              </div>
                           </div>
                        <?php } ?> 


                        <div class="sbmd-form-group mt-4">
                           <button type="submiy" class="btn btn-primary proceed_next_btn_wizrd">SUBMIT</button>                        
                        </div>
                     </form>
                  </div>
               </div>
            </div>
           
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </main>
   <!-- registration page end here  -->
   </body>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> -->
   <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js"></script> 
   <script src="<?php echo base_url(); ?>assets/web/js/select2.min.js"></script>
   <script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.toaster.js')?>"></script>
     <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCC1XUgdtsZr2K67-_1jW0hOH4_CPp_h8&libraries=places"></script>
   
  
   <script>
      $(document).ready(function() {
          $('body').bootstrapMaterialDesign();      
      
          $('.collapse-btn').click(function (){
               $('.custom-collapse.collapse-show').removeClass('collapse-show');
               $(this).parent().toggleClass('collapse-show');
          });
      });
      
      $("#multiple").select2({
            placeholder: "Select a programming language",
            allowClear: true
      });
      
      $("#multiple-Candidate").select2({
            placeholder: "Select a programming language",
            allowClear: true
      });
      $(".skills").select2({
         placeholder: "Select a programming language",
         allowClear: true
      });
      

      
      function readURL(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          
          reader.onload = function(e) {
            $('#img_div').html('<img src="'+e.target.result+'" style="width:100px">');
          }
          
          reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
      }

      $("#file-input").change(function() {
        readURL(this);
      });
      ////////////////get country///////////////
      getCountry();
      function getCountry(){ 
         var main_content = '<option value="">Country</option>';
         $.ajax({
               url: "<?= base_url() ?>/get_country",
               success: function(result){
               var obj=$.parseJSON(result);
               if(obj.status == 200){
                 $.each(obj.data, function(index,val){
                     main_content+='<option  value="'+val.id+'">'+val.name+'</option>';
                  })
               }
               $('.country').html(main_content);
             }
         })
      }
       ////////////////get state///////////////
       function getStateByCountryId(val)
       {
           
           var main_content = '<option value="">State</option>';
           $.ajax({type: "POST",
                    url: "<?= base_url() ?>/get_state",
                    data:{
                      country_id: val  
                    },
                    async:false,
                   success: function(result){
                   var obj=$.parseJSON(result);
                   
                   
               if(obj.status == 200){
                       $.each(obj.data, function(index,val){
                           main_content+='<option value="'+val.id+'">'+val.name+'</option>';
                        })
                      
                   }
                    $('.state').html(main_content);
                }
           })
       }
      ////////////////get city///////////////
      function getCityByStateId(val)
      {
        var main_content = '<option value="">City</option>';
           $.ajax({
            type: "POST",
            url: '<?php echo base_url();?>get_city',
            data:{state_id:val},
            success: function(data){
                var obj= JSON.parse(data);
                var city = '<option value="">City</option>';
                for(var i=0;i < obj.data.length;i++){
                    city += '<option value="'+obj.data[i].id+'">'+obj.data[i].name+'</option>';
                }
                $(".city").html(city);
            }
        });
      } 
      ////////////////get comunity///////////////
      getComunity();
      function getComunity(){ 
         var main_content ="";
         $.ajax({
               url: "<?= base_url() ?>/get_comunity",
               success: function(result){
               var obj=$.parseJSON(result);
               if(obj.status == 200){
                 $.each(obj.data, function(index,val){
                     main_content+='<option  value="'+val.community_id +'">'+val.community_name+'</option>';
                  })
               }
               $('.community').html(main_content);
               $(".community").select2({
                   placeholder: "Select a programming language",
                   allowClear: true,
                  maximumSelectionLength: 5
               });
               getSkillByCommunityId(obj.data[0]['community_id']);
             }
         })
      }
      function getSkillByCommunityId(val){
         
         var val = $(".community").val().join(',');
         var main_content = '';
           $.ajax({type: "POST",
              url: "<?= base_url() ?>get_skills",
              data:{
                community_id: val  
              },
              async:false,
             success: function(result){
             var obj=$.parseJSON(result);
             
               if(obj.status == 200){
                 $.each(obj.data, function(index,val){
                     main_content+='<option value="'+val.skill_id+'_'+val.community_id+'" community_id="'+val.community_id+'">'+val.skill_name+'</option>';
                  })
               }
                 $('.skills').html(main_content);
                 $(".skills").select2({
                     placeholder: "Select a programming language",
                     allowClear: true
                  });
                }
           })
      }

      //////////////////////
       /*function initialize() {
       var input = document.getElementById('location');
       const options = {
        types: ["address"],
      };
       var autocomplete = new google.maps.places.Autocomplete(input,options);
         google.maps.event.addListener(autocomplete, 'place_changed', function () {
             var place = autocomplete.getPlace();
             document.getElementById('latitude').value = place.geometry.location.lat();
             document.getElementById('longitude').value = place.geometry.location.lng();
         });
     }
     google.maps.event.addDomListener(window, 'load', initialize);*/
      //////////////////////



      var experience_min ="<?php echo $user_role=="KP" ? '3' : '' ;?>";
      var redirect_url ="<?php echo $user_role=="KP" ? 'planView' : 'loginView' ;?>";
      $( "#users_form" ).validate({
           rules: {
             country: "required",
             state: "required",
             city: "required",
             location: "required",
             "community[]": "required",
             "skils[]": "required",
             designation_name:  "required",
             experience: {
                  required: true,
                  min: experience_min,
               },
             organization_name:  "required",
             education:  "required",
             email:{
               required: true,
               email: true,
                remote:{
                     url: "<?php echo base_url()?>checkEmail",
                     async:false,
                     type: "post",
                     data:
                     {
                         email: function()
                         {
                             return $('#users_form :input[name="email"]').val();
                         }
                     }
                                   
                 },
               
             },
              type:  "required",
             job_companyname: {
               required: function (element) {
                      if($("input:radio[name='type']:checked").val()=='Job'){
                          return true;                          
                      }
                      else
                      {
                          return false;
                      }  
                   }  

               },
             job_designation: {
               required: function (element) {
                      if($("input:radio[name='type']:checked").val()=='Job'){
                          return true;                          
                      }
                      else
                      {
                          return false;
                      }  
                   }  

               },
             job_experience: {
               required: function (element) {
                      if($("input:radio[name='type']:checked").val()=='Job'){
                          return true;                          
                      }
                      else
                      {
                          return false;
                      }  
                   }  

               },
            bussiness_name: {
               required: function (element) {
                      if($("input:radio[name='type']:checked").val()=='Bussiness'){
                          return true;                          
                      }
                      else
                      {
                          return false;
                      }  
                   }  

               },
             bussiness_field: {
               required: function (element) {
                      if($("input:radio[name='type']:checked").val()=='Bussiness'){
                          return true;                          
                      }
                      else
                      {
                          return false;
                      }  
                   }  

               },
             employee_size: {
               required: function (element) {
                      if($("input:radio[name='type']:checked").val()=='Bussiness'){
                          return true;                          
                      }
                      else
                      {
                          return false;
                      }  
                   }  

               },
           },
            messages: {
                  country: "Conntry is required",
                  state: "State is required",
                  city: "City is required",
                  location: "Location is required",  
                  "community[]":"Community is required", 
                  "skils[]":"Skils is required", 
                  designation_name:"Designation Name is required", 
                  experience:"Experience is required",
                  experience: {
                     required: "Experience is required",
                     min: "Min "+experience_min+" yrs experience required",
                  }, 
                  organization_name:"Organization name is required", 
                  email:{
                     required: "Email is required",
                     remote: "Email already exist",
                  },
                
            },
         submitHandler: function(form)
            {
                $.ajax({
                  url: '<?php echo base_url('updateUserRegister'); ?>',
                  type: 'POST',
                  data:  new FormData($('#users_form')[0]),
                  cache:false,
                  contentType: false,
                  processData: false,
                  success: function(response) {
                      var obj = JSON.parse(response);
                      if(obj.status==200){
                        var priority = 'success';
                        var title    = 'Success';
                        var message  = obj.data+' added successfully!';
                        $.toaster({ priority : priority, title : title, message : message });
                        setTimeout(function() { 
                          window.location.href = "<?php echo base_url(); ?>"+redirect_url;
                        }, 2000);
                        
                      }else{
                        $('.error_msg').html(obj.message);
                          setTimeout(function() { $(".error_msg").html(''); }, 15000);
                      }
                  }
              });
            }
         });  


  </script>
</html>