<!DOCTYPE html>
<html>
<head>
	<title>Connekton</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/web/css/bootstrap-material-design.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/web/css/style.css">
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/logo.png" >
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/web/css/style_signup.css" >
	<style type="text/css">
		
		.valid_err_msg{
			color: red;
		}
		.tab{display: none; width: 100%; height: 50%;margin: 0px auto;}
.current{display: block;}
label.error{text-transform: none !important;}
	</style>
	<!-- <link rel="stylesheet" type="text/css" href="https://www.jqueryscript.net/demo/Signup-Form-Wizard-jQuery-multiStepForm/css/multi-form.css?v2"> -->

	<script src="<?php echo base_url() ?>assets/web/js/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url() ?>assets/web/js/custom.js"></script>
    <!-- <script type="text/javascript" src="https://www.jqueryscript.net/demo/Signup-Form-Wizard-jQuery-multiStepForm/js/multi-form.js?v2"></script> -->
</head>

<body>

<!-- ///////////// Login sign up section ////////// -->

<main class="login_sign_up_sec_pg">
	<img class="shape_00_bg_light_img_on_sign_up" src="<?php echo base_url(); ?>assets/web/images/login_left_bg_img.png">
	<!-- <div class="logo_sec d-inline-block w-100 text-center">
		<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/logo.png"></a>
	</div> -->
	<div class="container">	
		<div class="login_sign_up_sec_card_bg">
			<div class="row">
				<div class="col-md-12 m-auto">
					<div class="login_container_main01">
	                    <div class="wizard">  
	                        <form id="myForm" role="form"  class="login-box custom_fomr_login_sign_up mb-0">
	                        	<a href="index.php" class="wizar-back font-18 text-dark"><span class="material-icons mr-2">arrow_back</span>Back</a>
	                            <ul class="nav nav-tabs">
	                            	<li>
		                            	<a href="#" class="">
							            <span class="round-tab btn btn-primary p-0 step">1</span>
							        	</a>
						        	</li>
						        	<li>
						        		<a herf="#" class="">
							            <span class="round-tab btn btn-primary p-0 step">2</span>
							        	</a>
							        </li>
							        <li>
							        	<a href="#" class="">
							        	<span class="round-tab btn btn-primary p-0 step">3</span>
								        </a>
								    </li>
							        <li>
							        	<a href="#" class="">
						            	<span class="round-tab btn btn-primary p-0 step">4</span>
							        	</a>
							        </li>
						        </ul>
	                            <div class="tab-content" id="main_form">
	                            	<div class="tab">
	                                <!-- <div class="tab-pane active" role="tabpanel" id="step1"> -->
	                                    <div class="login_title mb-4">
											<h4 class="font-700 color-dark mb-2 text-center">
												Choose Role
											</h4>
											<p class="color-light-gray text-center">
												Select your user role to proceed
											</p>
										</div>
	                                    
							            <div class="select_role_radio_btn w-100 row mx-0 my-3 btn-group btn-group-toggle" data-toggle="buttons">
							              
							              <div class="col-md-3">
							                <label class="btn btn-secondary the_custom_radio_btn_chse_role" data-role="Client">
							                  <span class="material-icons check_radio_icon">check_circle</span>
							                  <div class="the_img_on_rdio_btn my-3 mx-auto">
							                    <img src="<?php echo base_url(); ?>assets/web/images/How_it_works/freelance.svg">
							                  </div>
							                  <input type="radio" class="user_role_selection" name="user_role" id="option1" autocomplete="off" value="Client" > 
							                  <h6 class="role-name mt-4 font-600">Client</h6>
							                  
							                </label>
							              </div>

							              <div class="col-md-3">
							                <label class="btn btn-secondary the_custom_radio_btn_chse_role" data-role="Employer">
							                  <span class="material-icons check_radio_icon">check_circle</span>
							                  <div class="the_img_on_rdio_btn my-3 mx-auto">
							                    <img src="<?php echo base_url(); ?>assets/web/images/How_it_works/freelance.svg">
							                  </div>
							                  <input type="radio" class="user_role_selection" name="user_role" id="option2" autocomplete="off" value="Employer"> 
							                  <h6 class="role-name mt-4 font-600">Employer</h6>	
							                </label>
							              </div>

							             

							              <div class="col-md-3">
							                <label class="btn btn-secondary the_custom_radio_btn_chse_role" data-role="KP's">
							                  	<span class="material-icons check_radio_icon">check_circle</span>
							                  	<div class="the_img_on_rdio_btn my-3 mx-auto">
							                    	<img src="<?php echo base_url(); ?>assets/web/images/How_it_works/freelance.svg">
							                  	</div>
							                  	<input type="radio" class="user_role_selection" name="user_role" id="option4" autocomplete="off" value="KP"> 
							                  	<h6 class="role-name mt-4 font-600">KP's</h6>
							                </label>
							              </div>
							               <div class="col-md-3">
							                <label class="btn btn-secondary the_custom_radio_btn_chse_role" data-role="User">
							                  <span class="material-icons check_radio_icon">check_circle</span>
							                  <div class="the_img_on_rdio_btn my-3 mx-auto">
							                    <img src="<?php echo base_url(); ?>assets/web/images/How_it_works/freelance.svg">
							                  </div>
							                  <input type="radio" class="user_role_selection" name="user_role" id="option3" autocomplete="off" value="User"> 
							                  <h6 class="role-name mt-4 font-600" value="User">User</h6>	
							                </label>
							              </div>
							              
							            </div>

	                                   <!--  <ul class="list-inline pull-right col-md-12 mb-0 text-center">
	                                        <li class="">
	                                        	<span class="valid_err_msg" id="role_selection_id">Please Select Role</span>
	                                        	<button type="button" id="step1_next_btn" class="next-step btn btn-primary proceed_next_btn_wizrd">Proceed Next</button>
	                                        </li>
	                                    </ul> -->
	                                </div>
	                                <div class="tab">
	                               <!--  <div class="tab-pane" role="tabpanel" id="step2"> -->
	                                    <div class="login_title mb-4">
											<h4 class="font-700 color-dark mb-2 text-center">
												Basic Details
											</h4>
											<p class="color-light-gray text-center">
												Fill up the details and verify you account through OTP
											</p>
										</div>

	                                    <div class="row step_cotainer_sec_width">
	                                    	<div class="form-group w-100 mb-4">
										    	<label for="phone_no10" class="col-form-label">Full Name</label>
										    	<div class="w-100">
										      		<input type="text" class="form-control" id="full_name" name="full_name" placeholder="Full Name">
										    	</div>
										  	</div>
										  	<div class="form-group w-100 mb-4">
										    	<label for="phone_no10" class="col-form-label">Phone Number</label>
										    	<div class="w-100">
										      		<input type="number" class="form-control phone_number" id="phone_number" name="phone_number" placeholder="Phone Number">
										    	</div>
										  	</div>
	                                   	</div>
	                                    <!-- <ul class="list-inline pull-right col-md-12 mb-0 text-center">
	                                        <li class="">
	                                        	<button type="button" id="step2_next_btn" class="next-step btn btn-primary proceed_next_btn_wizrd">Get OTP</button>
	                                        </li>
	                                    </ul> -->
	                                </div>

	                                <!-- <div class="tab-pane" role="tabpanel" id="step3"> -->
	                                <div class="tab">
	                                    <div class="login_title mb-4">
											<h3 class="font-700 color-dark mb-3 text-center">
												OTP Verification
											</h3>
											<p class="color-light-gray text-center">
												Enter valid OTP you get on your phone number
											</p>
										</div>
										<p class="sent_msg" style="color:green"></p>
										<div class="otp_error_msg" style="color:red;display: none">Invalid OTP</div>
										
	                                    <div class="row step_cotainer_sec_width otp_verify_sec_row">
	                                    	<div class="form-group">
												<div class="col-md-12">
										    		<label for="otp_verify" class="col-sm-2 col-form-label">Enter OTP</label>
										    	</div>
												<div class="passcode-wrapper row mx-0 text-center">
													<div class="form-group col-md-3 pt-0">
														<input id="codeBox1" name="codeBox1" type="number" maxlength="1" onkeyup="onKeyUpEvent(1, event)" onfocus="onFocusEvent(1)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
													</div>
													<div class="form-group col-md-3 pt-0">
														<input id="codeBox2" name="codeBox2" type="number" maxlength="1" onkeyup="onKeyUpEvent(2, event)" onfocus="onFocusEvent(2)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
													</div>
													<div class="form-group col-md-3 pt-0">
														<input id="codeBox3"  name="codeBox3" type="number" maxlength="1" onkeyup="onKeyUpEvent(3, event)" onfocus="onFocusEvent(3)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
													</div>
													<div class="form-group col-md-3 pt-0">
														<input id="codeBox4" name="codeBox4" type="number" maxlength="1" onkeyup="onKeyUpEvent(4, event)" onfocus="onFocusEvent(4)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
													</div>
												</div>
											</div>
	                                   	</div>	
	                                    <!-- <ul class="list-inline pull-right col-md-12 mb-0 text-center">
	                                        <li class="">
	                                        	<button type="button" id="step3_next_btn" class="next-step btn btn-primary proceed_next_btn_wizrd mt-2 mb-3">Verify OTP</button>
	                                        </li>
	                                    </ul> -->
										<div class="form-group text-center">
									    	<p class="resend_msg" style="color:green"></p>
									    	<span class="font-13">
									    		
									    		<a href="javascript:void(0)" class="color-primary ml-1 font-500 font-15" onclick="send_otp($('#phone_number').val(),'resend')">Resend OTP</a> 
									    	</span>
										</div>
	                                </div>

	                                <!-- <div class="tab-pane" role="tabpanel" id="step4"> -->
	                                 <div class="tab">	
	                                    <div class="login_title mb-4">
											<h4 class="font-700 color-dark mb-2 text-center">
												Enter Password
											</h4>
											<p class="color-light-gray text-center">
												Fill the password or confirm password field
											</p>
										</div>
										<div class="error_phone_msg" style="color:red;display: none">Already Exist</div>

	                                    <div class="row step_cotainer_sec_width">
	                                    	<div class="form-group w-100 mb-4">
										    	<label for="phone_no10" class="col-form-label">Password</label>
										    	<div class="w-100">
										      		<input type="password" class="form-control" id="password" name="password" placeholder="******">
										    	</div>
										  	</div>
										  	<div class="form-group w-100 mb-4">
										    	<label for="phone_no10" class="col-form-label">Confirm Password</label>
										    	<div class="w-100">
										      		<input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="******">
										    	</div>
										  	</div>
	                                   	</div>
	                                </div>
							            <div class="d-inline-flex w-100 mt-2">
							                <button type="button" class="previous default-btn prev-step ml-auto">Previous</button>
							                <button type="button" class="next next-step btn btn-primary proceed_next_btn_wizrd mx-auto">Next</button>
							                <button type="button" class="submit">Submit</button>
							            </div>

	                                <div class="clearfix"></div>

	                            </div>
	                            
	                        </form>
	                    </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="btm_cpyrght_sec_login_pg mt-0 d-inline-block w-100 text-center">
		<p class="font-13 font-500 mb-0 pt-3">
			Copyright ©2021 connekton. All rights reserved.
		</p>
		<p class="font-13 font-500 ml-auto mb-0 py-2">
			<a href="javascript:void(0)" class="color-primary">Terms of Use</a>
			 <span class="color-primary">|</span> 
			<a href="javascript:void(0)" class="color-primary">Privacy Policy</a>
		</p>
	</div>
</main>

<!-- ////////// -------------- /////////// -->


</body>

<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
<script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
<script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>

<script>

	$(document).ready(function() {
		$('body').bootstrapMaterialDesign(); 
	});

	///////////// Otp Input

	function getCodeBoxElement(index) {
	  return document.getElementById('codeBox' + index);
	}
	function onKeyUpEvent(index, event) {
	  const eventCode = event.which || event.keyCode;
	  if (getCodeBoxElement(index).value.length === 1) {
		 if (index !== 4) {
			getCodeBoxElement(index+ 1).focus();
		 } else {
			getCodeBoxElement(index).blur();
			// Submit code
			console.log('submit code ');
		 }
	  }
	  if (eventCode === 8 && index !== 1) {
		 getCodeBoxElement(index - 1).focus();
	  }
	}
	function onFocusEvent(index) {
	  for (item = 1; item < index; item++) {
		 const currentElement = getCodeBoxElement(item);
		 if (!currentElement.value) {
			  currentElement.focus();
			  break;
		 }
	  }
	}

	///////////// Form wizard

</script>

<script type="text/javascript">
	
	
  $(document).ready(function(){
  		var role ="";
		$('.the_custom_radio_btn_chse_role').click(function(){
			role = $(this).attr('data-role');
		})

        $.validator.addMethod('date', function(value, element, param) {
            return (value != 0) && (value <= 31) && (value == parseInt(value, 10));
        }, 'Please enter a valid date!');
        $.validator.addMethod('month', function(value, element, param) {
            return (value != 0) && (value <= 12) && (value == parseInt(value, 10));
        }, 'Please enter a valid month!');
        $.validator.addMethod('year', function(value, element, param) {
            return (value != 0) && (value >= 1900) && (value == parseInt(value, 10)); 
        }, 'Please enter a valid year not less than 1900!'); 
        $.validator.addMethod('username', function(value, element, param) {
            var nameRegex = /^[a-zA-Z0-9]+$/;
            return value.match(nameRegex);
        }, 'Only a-z, A-Z, 0-9 characters are allowed');

        var val = {
            // Specify validation rules
            rules: {
                user_role: "required",
                full_name: "required",
                email: {
                    required: true,
                    email: true
                },
                phone_number: {
                    required:true,
                    minlength:10,
                    maxlength:10,
                    digits:true,
                    // remote: "<?php echo base_url()?>checkPhone",
                    remote:{
					      url: "<?php echo base_url()?>checkPhone",
					      type: "post",
					      async:false,
					      data:
					      {
					          phone_number: function()
					          {
					              return $('#myForm :input[name="phone_number"]').val();
					          }
					      }
										     
					  },
                },
                codeBox1: {
                    required:true,
                    minlength:1,
                    maxlength:1,
                    digits:true
                },
                codeBox2: {
                    required:true,
                    minlength:1,
                    maxlength:1,
                    digits:true
                },
                codeBox3: {
                    required:true,
                    minlength:1,
                    maxlength:1,
                    digits:true
                },
                codeBox4: {
                    required:true,
                    minlength:1,
                    maxlength:1,
                    digits:true
                },
                
                password:{
                    required:true,
                    minlength:6,
                    maxlength:16,
                },
                confirm_password:{
                    required:true,
                    minlength:6,
                    maxlength:16,
                    equalTo : "#password"
                }
            },
            // Specify validation error messages
            messages: {
                user_role:      "User role is required",
                full_name:      "Full name is required",
                email: {
                    required:   "Email is required",
                    email:      "Please enter a valid e-mail",
                },
                phone_number:{
                    required:   "Phone number is requied",
                    minlength:  "Please enter 10 digit mobile number",
                    maxlength:  "Please enter 10 digit mobile number",
                    digits:     "Only numbers are allowed in this field",
                    remote: 	"Mobile no already exist"
                },
                
                password:{
                    required:   "Password is required",
                    minlength:  "Password should be minimum 6 characters",
                    maxlength:  "Password should be maximum 16 characters",
                },
                confirm_password:{
                    required:   "Confirm Password is required",
                    minlength:  "Confirm Password should be minimum 6 characters",
                    maxlength:  "Confirm Password should be maximum 16 characters",

                    
                }
            },
            submitHandler: function(form)
            {
            	 $.ajax({
		            url: 'userRegister',
		            type: 'POST',
		            data: $('#myForm').serialize(),

		            success: function(response) {
		                var obj = JSON.parse(response);
		                if(obj.status==200){
		                   window.location.href = "<?php echo base_url('registrationView'); ?>";
		                }else{
		                  $('.error_phone_msg').show();
                          setTimeout(function() { $(".error_phone_msg").hide(); }, 15000);
		                }
		            }
		        });
            }
          
        }
        $("#myForm").multiStepForm(
        {
            // defaultStep:0,
            beforeSubmit : function(form, submit){
                alert("called before submiting the form");
                alert(form);
                alert(submit);
            },
            validations:val,
        }
        ).navigateTo(0);
    });
</script>

</html>




