

<!-- /////////////// Header ///////////// -->

<header class="main_dash_header">
	<div class="overlay_on_header"></div>
	<div class="header_video_section">
	    <video id="bg_header_video" preload="auto" autoplay="true" loop="loop" muted="muted" autobuffer="autobuffer">
	    	<source src="<?php echo base_url(); ?>assets/web/images/dummy_vid_header.mp4" type="video/mp4">
	    </video>
	</div>
	<div class="container position-relative" style="z-index: 99">
		<div class="row mx-0">
			<div class="col-md-8 px-0 pb-5">
				<div class="header_heading_sec">
					<h3 class="color-light">Find the Jobs</h3>
					<h1 class="color-light font-700">Search from 25,800 Jobs</h1>
				</div>
				<div class="header_main_search_sec mb-4 pb-2">
					<div class="search_sec_container">
						<div class="search_sec_input search_sec_dvs">
							<div class="input-group mb-0">
								<span class="material-icons">search</span>
								<input type="text" class="form-control" placeholder="Search here...">	
							</div>
						</div>
						<div class="search_sec_cat_selct search_sec_dvs">
							<div class="input-group mb-0">
								<select>
									<option value="0">Select Catagories</option>
									<option value="1">Cat01</option>
									<option value="2">Cat02</option>
									<option value="3">Cat03</option>
									<option value="4">Cat04</option>
								</select>
								<span class="material-icons">arrow_drop_down</span>								
							</div>
						</div>
						<div class="search_sec_locate_selct search_sec_dvs">
							<div class="input-group mb-0">
								<select>
									<option value="0">Select Location</option>
									<option value="1">Cat01</option>
									<option value="2">Cat02</option>
									<option value="3">Cat03</option>
									<option value="4">Cat04</option>
								</select>
								<span class="material-icons">arrow_drop_down</span>	
							</div>
						</div>
						<div class="search_btn_header search_sec_dvs">
							<div class="input-group mb-0 justify-content-end">
								<button type="button" class="btn btn-primary color-primary">
									Search
									<span class="material-icons">arrow_forward</span>
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="upload_res_candte_sec">
					<div class="row mx-0">
						<div class="col-md-6 pl-0">
							<h5 class="color-light font-600 mb-4 pb-2 position-relative">Let Employers Find You :</h5>
							<span class="color-light font-14">
								Thousands of employers search for candidates
								on  Service Management Services.
							</span>
						</div>
						<div class="col-md-6 my-auto pl-0 upload_resume_file_btn_col">
							<div class="file-input">
						      <input type="file" name="file-input" id="file-input" class="file-input__input"/>
						      <label class="file-input__label mb-0" for="file-input">
						        <span class="material-icons">file_upload</span>
						        <span>Upload Resume</span></label>
						    </div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
	<div class="col-md-12 px-0 pt-4 position-relative" style="z-index: 99">
		<div class="all_comunites_on_header">
			<div class="container">
				<div class="tile_of_comunites">
					<h5 class="color-light font-600 mb-4 pb-2 position-relative d-inline-block">
						Communities
					</h5>
					<a href="javascript:void(0)" class="color-link font-600 font-18 float-right read_more_link_txt">View All 
					<span class="material-icons">arrow_forward</span>
				</a>
				</div>
				<div class="all_comunites_Cards_dis d-flex flex-wrap pt-4 pb-5 w-100">
					<div class="Comunites_Cards d-flex flex-wrap w-100">
						<div class="Comunites_Cards_container">
							<div class="Comunites_Cards_img_container">
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/Catagories/plumber.svg">
								<img class="img-fluid bg_img_comunites_Cards_light" src="<?php echo base_url(); ?>assets/web/images/Catagories/plumber.svg">
							</div>
							<h6 class="Comunites_sec_title text-center">
								Construction
							</h6>
						</div>
						<div class="Comunites_Cards_container">
							<div class="Comunites_Cards_img_container">
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/Catagories/web-development.svg">
								<img class="img-fluid bg_img_comunites_Cards_light" src="<?php echo base_url(); ?>assets/web/images/Catagories/web-development.svg">
							</div>
							<h6 class="Comunites_sec_title text-center">
								Web Development
							</h6>
						</div>
						<div class="Comunites_Cards_container">
							<div class="Comunites_Cards_img_container">
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/Catagories/tyre.svg">
								<img class="img-fluid bg_img_comunites_Cards_light" src="<?php echo base_url(); ?>assets/web/images/Catagories/tyre.svg">
							</div>
							<h6 class="Comunites_sec_title text-center">
								Automotive
							</h6>
						</div>
						<div class="Comunites_Cards_container">
							<div class="Comunites_Cards_img_container">
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/Catagories/consultation.svg">
								<img class="img-fluid bg_img_comunites_Cards_light" src="<?php echo base_url(); ?>assets/web/images/Catagories/consultation.svg">
							</div>
							<h6 class="Comunites_sec_title text-center">
								Consultant
							</h6>
						</div>
						<div class="Comunites_Cards_container">
							<div class="Comunites_Cards_img_container">
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/Catagories/accounting.svg">
								<img class="img-fluid bg_img_comunites_Cards_light" src="<?php echo base_url(); ?>assets/web/images/Catagories/accounting.svg">
							</div>
							<h6 class="Comunites_sec_title text-center">
								Accountant
							</h6>
						</div>
						<div class="Comunites_Cards_container">
							<div class="Comunites_Cards_img_container">
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/Catagories/article.svg">
								<img class="img-fluid bg_img_comunites_Cards_light" src="<?php echo base_url(); ?>assets/web/images/Catagories/article.svg">
							</div>
							<h6 class="Comunites_sec_title text-center">
								Content Writer
							</h6>
						</div>
						<div class="Comunites_Cards_container">
							<div class="Comunites_Cards_img_container">
								<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/Catagories/management.svg">
								<img class="img-fluid bg_img_comunites_Cards_light" src="<?php echo base_url(); ?>assets/web/images/Catagories/management.svg">
							</div>
							<h6 class="Comunites_sec_title text-center">
								Sr. Business Manager
							</h6>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</header>

<!-- ////////// -------------- /////////// -->

<!-- /////////////// How it Works section ///////////// -->

<section class="how_it_wrk_Sec pb-5">
	<div class="container">
		<div class="row mx-0">
			<div class="col-md-12 mb-5 d-flex justify-content-center">
				<h2 class="section_title_all font-700 text-center position-relative">
					How It Works
				</h2>
			</div>
			<div class="col-md-3">
				<div class="how_it_wrk_Sec_container text-center w-100">
					<div class="how_it_wrk_Sec_tp_img mb-4 pb-2">
						<img width="85" src="<?php echo base_url(); ?>assets/web/images/How_it_works/journal.svg">
					</div>
					<h5 class="font-600 mb-4 position-relative">
						Post Projects
					</h5>
					<span class="font-12">
						Lorem ipsum dolor sit amet,
						consectetur adipisicing elit, sed do
						eiusmod tempor incididunt ut
						labore et dolore magna.
					</span>
				</div>
			</div>
			<div class="col-md-3">
				<div class="how_it_wrk_Sec_container text-center w-100">
					<div class="how_it_wrk_Sec_tp_img mb-4 pb-2">
						<img width="85" src="<?php echo base_url(); ?>assets/web/images/How_it_works/freelance.svg">
					</div>
					<h5 class="font-600 mb-4 position-relative">
						Hire
					</h5>
					<span class="font-12">
						Lorem ipsum dolor sit amet,
						consectetur adipisicing elit, sed do
						eiusmod tempor incididunt ut
						labore et dolore magna.
					</span>
				</div>
			</div>
			<div class="col-md-3">
				<div class="how_it_wrk_Sec_container text-center w-100">
					<div class="how_it_wrk_Sec_tp_img mb-4 pb-2">
						<img width="85" src="<?php echo base_url(); ?>assets/web/images/How_it_works/clock.svg">
					</div>
					<h5 class="font-600 mb-4 position-relative">
						Connect
					</h5>
					<span class="font-12">
						Lorem ipsum dolor sit amet,
						consectetur adipisicing elit, sed do
						eiusmod tempor incididunt ut
						labore et dolore magna.
					</span>
				</div>
			</div>
			<div class="col-md-3">
				<div class="how_it_wrk_Sec_container text-center w-100">
					<div class="how_it_wrk_Sec_tp_img mb-4 pb-2">
						<img width="85" src="<?php echo base_url(); ?>assets/web/images/How_it_works/salary.svg">
					</div>
					<h5 class="font-600 mb-4 position-relative">
						Pay Safety
					</h5>
					<span class="font-12">
						Lorem ipsum dolor sit amet,
						consectetur adipisicing elit, sed do
						eiusmod tempor incididunt ut
						labore et dolore magna.
					</span>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ////////// -------------- /////////// -->

<!-- /////////////// Section connect collab ///////////// -->

<section class="connect_collab_sec">
	<div class="container">
		<div class="row mx-0">
			<div class="col-md-6">
				<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/connect_collab.png">
			</div>
			<div class="col-md-6 m-auto">
				<h2 class="section_title_all font-700 text-left mb-5 position-relative">
					Connect & Collaborate
				</h2>
				<p class="mb-4">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
					eiusmod tempor incididunt ut labore et dolore magna aliqua.
					Ut enim ad minim veniam, quis nostrud exercitation ullamco
					laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
					dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
					nulla pariatur. Excepteur sint occaecat cupidatat non proident,
					sunt in culpa qui officia deserunt mollit anim id est laborum Duis
					aute irure sint occaecat.
				</p>
				<a href="javascript:void(0)" class="color-link font-600 font-18 read_more_link_txt">Read More 
					<span class="material-icons">arrow_forward</span>
				</a>
			</div>
		</div>
	</div>
</section>

<!-- ////////// -------------- /////////// -->

<!-- /////////////// Section Earn Quickly ///////////// -->

<section class="earn_quickly_sec">
	<div class="container">
		<div class="row mx-0">
			<div class="col-md-6 m-auto">
				<h2 class="section_title_all font-700 text-left mb-5 position-relative">
					Earn Quickly
				</h2>
				<p class="mb-4">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
					eiusmod tempor incididunt ut labore et dolore magna aliqua.
					Ut enim ad minim veniam, quis nostrud exercitation ullamco
					laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
					dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
					nulla pariatur. Excepteur sint occaecat cupidatat non proident,
					sunt in culpa qui officia deserunt mollit anim id est laborum Duis
					aute irure sint occaecat.
				</p>
				<a href="javascript:void(0)" class="color-link font-600 font-18 read_more_link_txt">Read More 
					<span class="material-icons">arrow_forward</span>
				</a>
			</div>
			<div class="col-md-6">
				<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/earn_quickly.png">
			</div>
		</div>
	</div>
</section>

<!-- ////////// -------------- /////////// -->

<!-- /////////////// Section Get hired ///////////// -->

<section class="get_hired_Sec">
	<div class="container">
		<div class="row mx-0">
			<div class="col-md-6">
				<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/get_hired.png">
			</div>
			<div class="col-md-6 m-auto">
				<h2 class="section_title_all font-700 text-left mb-5 position-relative">
					Get Hired
				</h2>
				<p class="mb-4">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
					eiusmod tempor incididunt ut labore et dolore magna aliqua.
					Ut enim ad minim veniam, quis nostrud exercitation ullamco
					laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
					dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
					nulla pariatur. Excepteur sint occaecat cupidatat non proident,
					sunt in culpa qui officia deserunt mollit anim id est laborum Duis
					aute irure sint occaecat.
				</p>
				<a href="javascript:void(0)" class="color-link font-600 font-18 read_more_link_txt">Read More 
					<span class="material-icons">arrow_forward</span>
				</a>
			</div>
		</div>
	</div>
</section>

<!-- ////////// -------------- /////////// -->

<!-- /////////////// Section Lowest Fee ///////////// -->

<section class="lowest_fee_sec">
	<div class="container">
		<div class="row mx-0">
			<div class="col-md-6 m-auto">
				<h2 class="section_title_all font-700 text-left mb-5 position-relative">
					Lowest Fee
				</h2>
				<p class="mb-4">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
					eiusmod tempor incididunt ut labore et dolore magna aliqua.
					Ut enim ad minim veniam, quis nostrud exercitation ullamco
					laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
					dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
					nulla pariatur. Excepteur sint occaecat cupidatat non proident,
					sunt in culpa qui officia deserunt mollit anim id est laborum Duis
					aute irure sint occaecat.
				</p>
				<a href="javascript:void(0)" class="color-link font-600 font-18 read_more_link_txt">Read More 
					<span class="material-icons">arrow_forward</span>
				</a>
			</div>
			<div class="col-md-6">
				<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/lowest_fee.png">
			</div>
		</div>
	</div>
</section>

<!-- ////////// -------------- /////////// -->

<!-- /////////////// Section Get hired ///////////// -->

<section class="get_hired_Sec">
	<div class="container">
		<div class="row mx-0">
			<div class="col-md-6">
				<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/quality_Work.png">
			</div>
			<div class="col-md-6 m-auto">
				<h2 class="section_title_all font-700 text-left mb-5 position-relative">
					Quality Work
				</h2>
				<p class="mb-4">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
					eiusmod tempor incididunt ut labore et dolore magna aliqua.
					Ut enim ad minim veniam, quis nostrud exercitation ullamco
					laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
					dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
					nulla pariatur. Excepteur sint occaecat cupidatat non proident,
					sunt in culpa qui officia deserunt mollit anim id est laborum Duis
					aute irure sint occaecat.
				</p>
				<a href="javascript:void(0)" class="color-link font-600 font-18 read_more_link_txt">Read More 
					<span class="material-icons">arrow_forward</span>
				</a>
			</div>
		</div>
	</div>
</section>

<!-- ////////// -------------- /////////// -->

<!-- /////////////// Section Post Articles ///////////// -->

<section class="post_article_Sec">
	<div class="container">
		<div class="row mx-0">
			<div class="col-md-6 m-auto">
				<h2 class="section_title_all font-700 text-left mb-5 position-relative">
					Lowest Fee
				</h2>
				<p class="mb-4">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
					eiusmod tempor incididunt ut labore et dolore magna aliqua.
					Ut enim ad minim veniam, quis nostrud exercitation ullamco
					laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
					dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
					nulla pariatur. Excepteur sint occaecat cupidatat non proident,
					sunt in culpa qui officia deserunt mollit anim id est laborum Duis
					aute irure sint occaecat.
				</p>
				<a href="javascript:void(0)" class="color-link font-600 font-18 read_more_link_txt">Read More 
					<span class="material-icons">arrow_forward</span>
				</a>
			</div>
			<div class="col-md-6">
				<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/post_article.png">
			</div>
		</div>
	</div>
</section>

<!-- ////////// -------------- /////////// -->

<!-- /////////////// Section Counter ///////////// -->

<section class="counter_sec">
	<div class="container">	
		<div class="row mx-0">
			<div class="col-md-3" style="border-right: 1px solid #ddd;">
				<div class="counter_sec_conatiner">	
					<div class="counter_img_sec">
						<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/Counter_icon/projects.svg">
					</div>
					<h5 class="font-600 text-center mb-3">Projects</h5>
					<h2 class="color-primary font-700 text-center">
						12500<span class="font-300 font-14">+</span>
					</h2>
				</div>
			</div>
			<div class="col-md-3" style="border-right: 1px solid #ddd;">
				<div class="counter_sec_conatiner">	
					<div class="counter_img_sec">
						<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/Counter_icon/freelancer.svg">
					</div>
					<h5 class="font-600 text-center mb-3">Freelancer</h5>
					<h2 class="color-primary font-700 text-center">
						7525<span class="font-300 font-14">+</span>
					</h2>
				</div>
			</div>
			<div class="col-md-3" style="border-right: 1px solid #ddd;">
				<div class="counter_sec_conatiner">	
					<div class="counter_img_sec">
						<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/Counter_icon/jobs.svg">
					</div>
					<h5 class="font-600 text-center mb-3">Jobs</h5>
					<h2 class="color-primary font-700 text-center">
						4523<span class="font-300 font-14">+</span>
					</h2>
				</div>
			</div>
			<div class="col-md-3">
				<div class="counter_sec_conatiner">	
					<div class="counter_img_sec">
						<img class="img-fluid" src="<?php echo base_url(); ?>assets/web/images/Counter_icon/Kps.svg">
					</div>
					<h5 class="font-600 text-center mb-3">KPs</h5>
					<h2 class="color-primary font-700 text-center">
						3246<span class="font-300 font-14">+</span>
					</h2>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ////////// -------------- /////////// -->

<!-- /////////////// Section Newsletter ///////////// -->

<section class="newsletter_Sec py-0">
	<div class="container">
		<div class="newsletter_Sec_bg_container overflow-hidden">
			<img class="newsletter_bg_img_container" src="<?php echo base_url(); ?>assets/web/images/newsletter_bg_img.png">
			<div class="row mx-0">
				<div class="col-md-6">
					<div class="newsletter_Sec_title">
						<h3 class="font-600 color-light mb-3">
							Subscribe Our Newsletter
						</h3>
						<p class="font-400 font-14 mb-0 color-light">
							Your information is safe with us!
							<br>
							Unsubscribe Anytime.
						</p>
					</div>
				</div>
				<div class="col-md-6 m-auto">
					<div class="newsletter_Sec_input">
						<div class="input-group mb-0 justify-content-end input_group_newsletter">
							<input type="text" class="form-control" placeholder="Enter Your Email ID">
							<button type="button" class="btn btn-primary color-primary mb-0">
								Subscribe
								<span class="material-icons position-arrow">arrow_forward</span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- ////////// -------------- /////////// -->

<!-- /////////////// Section Newsletter ///////////// -->






