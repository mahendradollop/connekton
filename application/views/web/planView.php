<?php
//print_r($register_user_cookie['user_id']);die;

?>

<!DOCTYPE html>
<html>
<head>
    <title>Connekton</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/web/css/bootstrap-material-design.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/web/css/style.css">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/logo.png" >
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/web/css/style_signup.css" >
    <style type="text/css">
        
        .valid_err_msg{
            color: red;
        }
        .tab{display: none; width: 100%; height: 50%;margin: 0px auto;}
.current{display: block;}
label.error{text-transform: none !important;}
    </style>
   
</head>

<body>
<div class="price-sec-wrap theme_gary p_py_44 d-inline-block w-100 h-100">
        <div class="container">
            <div class="main-heading font-20 font-600 mb-5 text-center">PRICING PLAN</div>
            <div class="error_msg"></div>
            <div class="row plan_container"></div>
        </div>
    </div>
</body>

<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
<!-- <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
<script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script> -->
<script src="<?php echo base_url() ?>assets/web/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/web/js/jquery.toaster.js')?>"></script>


   

    <script type="text/javascript">
        getPlan();
        function getPlan(){
            var plan_html ="";
            var user_id ="<?php echo $register_user_cookie['user_id'];?>"; 
            $.ajax({
              url: "<?php echo base_url('getUserPlan?user_id=');?>"+user_id,
              success: function(response){
                if(response!=""){
                    $.each(response.plan, function( index, value ) {
                        var checked="";
                        if(index==0){
                            checked ="checked";
                        }
                        plan_html +=`<div class="col-lg-4">
                                        <div class="price-box">
                                            <div class="">
                                                <div class="price-label basic">`+value['plan_title']+`</div>
                                                <div class="price">`+formatMoney(value['fee'])+`</div>
                                                <div class="price-info">Duration: `+value['duration']+` Month</div>
                                            </div>
                                            <div class="info">
                                                <p class="short-des">`+value['description']+`</p>
                                            </div>
                                            <div class="form-check" >
                                            <label for="plan_`+value['plan_id']+`" href="#" class="plan-btn" onclick="user_plan(`+value['plan_id']+`,`+value['duration']+`)">Join `+value['plan_title']+` </label>
                                            <input type="radio" name="plan_id" value="`+value['plan_id']+`" class="opacity-0" id="plan_`+value['plan_id']+`">
                                            </div>
                                        </div>
                                    </div>`;
                    });
                    $('.plan_container').html(plan_html);

                }
              },
                error: function (xhr, exception) {
                    $('.error_msg').html(xhr.responseJSON.message);
                    setTimeout(function() { $(".error_msg").html(''); }, 15000);
                    $.toaster({ priority : 'danger', title : 'Notice', message : xhr.responseJSON.message });
                }
            });

        }
        function user_plan(plan_id,plan_duration){
            var user_id ="<?php echo $register_user_cookie['user_id'];?>"; 
            $.ajax({
              url: "<?php echo base_url('addUserPlan');?>",
              method:'POST',
              data:{ plan_id: plan_id , plan_duration:plan_duration,user_id:user_id },
              success: function(response){
                $.toaster({ priority : 'success', title : 'Success', message : response.message });
                setTimeout(function() { 
                  window.location.href = "<?php echo base_url('loginView'); ?>";
                }, 2000);
                
              },
              error: function (xhr, exception) {
                    $.toaster({ priority : 'danger', title : 'Notice', message : xhr.responseJSON.message });
                }
            });
        }




    </script>
