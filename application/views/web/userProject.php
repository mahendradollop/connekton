

<!-- dashboard section start here -->
<section class="main_body theme_gary p_py_44 d-inline-block w-100">
   <div class="container">
      <div class="row">
         <div class="col-md-3">
            <div class="side_bar border_rounded_5">
               <div class="side_top-title py-2 border-bottom">
                  <h3 class="font-16 font-400">Questions</h3>
               </div>
               <div class="nav flex-column nav-pills mt-4" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                  <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><span class="icons icon-question mr-2"></span> All Questions</a>
                  <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><span class="icons icon-question mr-2"></span> My Question</a>
               </div>
            </div>
         </div>
         <div class="col-md-9">
            <div class="main_center_body">
               <div class="qusetion-posted-bar bg-white border_rounded_5 m_mb_20">
                  <div type="button" class="posted-user-profile font-12 d-inline-flex align-items-center w-100 mb-2" data-toggle="modal" data-target="#addquestion">
                     <span class="user-image"> <img src="images/user-img.png" class="img-fluid"></span>
                     Client
                  </div>
                  <h5 class="font-20 font-600 m-0">Post Your Question</h5>
               </div>
               <div class="tab-content" id="v-pills-tabContent">
                  <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                     <div class="main_top_search w-100 position-relative p_pb_20">
                        <div class="row align-items-center">
                           <div class="col-md-6">
                              <h4 class="font-20 font-600 mb-0">All Questions</h4>
                           </div>
                           <div class=" col-md-6">
                              <div class="input-group searc_field">
                                 <input type="text" class="form-control border_rounded_5" placeholder="Search...." aria-label="Recipient's username" aria-describedby="basic-addon2">
                                 <div class="input-group-append">
                                    <button class="btn btn-link sercah_btn" type="button"><span class="icon-loupe"></span></button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="add_question_box position-relative ">
                           <div class="aq_inner_body border_rounded_5 bg-white my-3  py-1 border">
                              <div class="kp_profile_name d-inline-flex align-items-center border-bottom w-100 py-2 p_px_15">
                                 <span class="profile_pic ctk_overflow_hidden rounded-circle mr-3"><img src="images/user-img.png" class="img-fluid" alt=""></span>
                                 <span class="font-14">Jhon Smith</span>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="question_box b border_rounded_5 bg-white p_px_15 py-3 position-relative m_mb_20">
                        <span class="date font-12 font-400 text-dark">25-02-2020</span>
                        <ul class="Question_posted_by d-inline-flex align-items-center w-100 pl-0">
                           <li><a href="#" class="posted_name pl-0">
                              <span class="image ctk_overflow_hidden rounded-circle mr-2"><img src="images/user-img.png" class="img-fluid" alt=""></span> 
                              John Smith
                              </a>
                           </li>
                           <li><a href="#" class="posted_details">Ui Designer</a></li>
                        </ul>
                        <h4 class="question_name font-16 font-700 pb-1"><span class="icons icon-question mr-1"></span>What is your favorite training app, and why?</h4>
                        <div class="answer_like_dislike-box d-inline-flex align-items-center w-100 mt-2 mb-3">
                           <span class="number-of-ans-bage">6 Answer</span>
                           <div class="like-dislike-btns ml-auto d-inline-flex">
                              <a href="#" class="like-btn mr-2"><span class="material-icons">thumb_up</span> 50</a>
                              <a href="#" class="like-btn"><span class="material-icons">thumb_down</span> 0</a>
                           </div>
                        </div>
                        <div class="main-anser-container position-relative pt-3 border-top">
                           <div class="answer_box border_rounded_5 d-inline-block w-100 p-2 mb-2">
                              <ul class="Question_posted_by d-inline-flex align-items-center w-100 pl-0">
                                 <li><a href="#" class="posted_name pl-0">
                                    <span class="image ctk_overflow_hidden rounded-circle mr-2"><img src="images/brayce.png" class="img-fluid" alt=""></span> 
                                    Brayce 
                                    </a> |
                                 </li>
                                 <li><a href="#" class="posted_details">Ui Designer</a></li>
                                 <li class="ml-auto font-600 "><span href="#" class="date_time">24-02-2020  1.20 PM</span></li>
                              </ul>
                              <!--  <h6 class="font-12 font-500">Answer</h6> -->
                              <p class="font-12 qu_answer font-500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. arem jsum has been the industry's standard dummy text ever since </p>
                           </div>
                           <div class="answer_box border_rounded_5 d-inline-block w-100 p-2 mb-2">
                              <ul class="Question_posted_by d-inline-flex align-items-center w-100 pl-0">
                                 <li><a href="#" class="posted_name pl-0">
                                    <span class="image ctk_overflow_hidden rounded-circle mr-2"><img src="images/brayce.png" class="img-fluid" alt=""></span> 
                                    Brayce 
                                    </a> |
                                 </li>
                                 <li><a href="#" class="posted_details">Ui Designer</a></li>
                                 <li class="ml-auto font-600 "><span href="#" class="date_time">24-02-2020  1.20 PM</span></li>
                              </ul>
                              <!--  <h6 class="font-12 font-500">Answer</h6> -->
                              <p class="font-12 qu_answer font-500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. arem jsum has been the industry's standard dummy text ever since </p>
                           </div>
                           <div class="answer_box border_rounded_5 d-inline-block w-100 p-2 mb-2">
                              <ul class="Question_posted_by d-inline-flex align-items-center w-100 pl-0">
                                 <li><a href="#" class="posted_name pl-0">
                                    <span class="image ctk_overflow_hidden rounded-circle mr-2"><img src="images/brayce.png" class="img-fluid" alt=""></span> 
                                    Brayce 
                                    </a> |
                                 </li>
                                 <li><a href="#" class="posted_details">Ui Designer</a></li>
                                 <li class="ml-auto font-600 "><span href="#" class="date_time">24-02-2020  1.20 PM</span></li>
                              </ul>
                              <!--  <h6 class="font-12 font-500">Answer</h6> -->
                              <p class="font-12 qu_answer font-500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. arem jsum has been the industry's standard dummy text ever since </p>
                           </div>
                           <div class="answer_box border_rounded_5 d-inline-block w-100 p-2 mb-2">
                              <ul class="Question_posted_by d-inline-flex align-items-center w-100 pl-0">
                                 <li><a href="#" class="posted_name pl-0">
                                    <span class="image ctk_overflow_hidden rounded-circle mr-2"><img src="images/brayce.png" class="img-fluid" alt=""></span> 
                                    Brayce 
                                    </a> |
                                 </li>
                                 <li><a href="#" class="posted_details">Ui Designer</a></li>
                                 <li class="ml-auto font-600 "><span href="#" class="date_time">24-02-2020  1.20 PM</span></li>
                              </ul>
                              <!--  <h6 class="font-12 font-500">Answer</h6> -->
                              <p class="font-12 qu_answer font-500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. arem jsum has been the industry's standard dummy text ever since </p>
                           </div>
                           <div class="answer_box border_rounded_5 d-inline-block w-100 p-2 mb-2">
                              <ul class="Question_posted_by d-inline-flex align-items-center w-100 pl-0">
                                 <li><a href="#" class="posted_name pl-0">
                                    <span class="image ctk_overflow_hidden rounded-circle mr-2"><img src="images/brayce.png" class="img-fluid" alt=""></span> 
                                    Brayce 
                                    </a> |
                                 </li>
                                 <li><a href="#" class="posted_details">Ui Designer</a></li>
                                 <li class="ml-auto font-600 "><span href="#" class="date_time">24-02-2020  1.20 PM</span></li>
                              </ul>
                              <!--  <h6 class="font-12 font-500">Answer</h6> -->
                              <p class="font-12 qu_answer font-500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. arem jsum has been the industry's standard dummy text ever since </p>
                           </div>
                           <div class="answer_box border_rounded_5 d-inline-block w-100 p-2 mb-2">
                              <ul class="Question_posted_by d-inline-flex align-items-center w-100 pl-0">
                                 <li><a href="#" class="posted_name pl-0">
                                    <span class="image ctk_overflow_hidden rounded-circle mr-2"><img src="images/brayce.png" class="img-fluid" alt=""></span> 
                                    Brayce 
                                    </a> |
                                 </li>
                                 <li><a href="#" class="posted_details">Ui Designer</a></li>
                                 <li class="ml-auto font-600 "><span href="#" class="date_time">24-02-2020  1.20 PM</span></li>
                              </ul>
                              <!--  <h6 class="font-12 font-500">Answer</h6> -->
                              <p class="font-12 qu_answer font-500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. arem jsum has been the industry's standard dummy text ever since </p>
                           </div>
                        </div>
                        <a href="#" class="see-more-ans primary-color">See More</a>
                     </div>
                     <div class="question_box b border_rounded_5 bg-white p_px_15 py-3 position-relative m_mb_20">
                        <span class="date font-12 font-400 text-dark">25-02-2020</span>
                        <ul class="Question_posted_by d-inline-flex align-items-center w-100 pl-0">
                           <li><a href="#" class="posted_name pl-0">
                              <span class="image ctk_overflow_hidden rounded-circle mr-2"><img src="images/user-img.png" class="img-fluid" alt=""></span> 
                              John Smith
                              </a>
                           </li>
                           <li><a href="#" class="posted_details">Ui Designer</a></li>
                        </ul>
                        <h4 class="question_name font-16 font-700 pb-1"><span class="icons icon-question mr-1"></span>What is your favorite training app, and why?</h4>
                        <div class="answer_like_dislike-box d-inline-flex align-items-center w-100 mt-2">
                           <span class="number-of-ans-bage">0 Answer</span>
                           <div class="like-dislike-btns ml-auto d-inline-flex">
                              <a href="#" class="like-btn mr-2"><span class="material-icons">thumb_up</span> 50</a>
                              <a href="#" class="like-btn"><span class="material-icons">thumb_down</span> 0</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                     <div class="main_top_search w-100 position-relative p_pb_20">
                        <div class="row align-items-center">
                           <div class="col-md-6">
                              <h4 class="font-20 font-600 mb-0">My Questions</h4>
                           </div>
                           <div class=" col-md-6">
                              <div class="input-group searc_field">
                                 <input type="text" class="form-control border_rounded_5" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                 <div class="input-group-append">
                                    <button class="btn btn-link sercah_btn" type="button"><span class="icon-loupe"></span></button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="add_question_box position-relative ">
                           <div class="aq_inner_body border_rounded_5 bg-white my-3  py-1 border">
                              <div class="kp_profile_name d-inline-flex align-items-center border-bottom w-100 py-2 p_px_15">
                                 <span class="profile_pic ctk_overflow_hidden rounded-circle mr-3"><img src="images/user-img.png" class="img-fluid" alt=""></span>
                                 <span class="font-14">Jhon Smith</span>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="question_box b border_rounded_5 bg-white p_px_15 py-3 position-relative m_mb_20">
                        <span class="date font-12 font-400 text-dark">25-02-2020</span>
                        <ul class="Question_posted_by d-inline-flex align-items-center w-100 pl-0">
                           <li><a href="#" class="posted_name pl-0">
                              <span class="image ctk_overflow_hidden rounded-circle mr-2"><img src="images/user-img.png" class="img-fluid" alt=""></span> 
                              John Smith
                              </a>
                           </li>
                           <li><a href="#" class="posted_details">Ui Designer</a></li>
                        </ul>
                        <h4 class="question_name font-16 font-700 pb-1"><span class="icons icon-question mr-1"></span>What is your favorite training app, and why?</h4>
                        <div class="answer_like_dislike-box d-inline-flex align-items-center w-100 mt-2">
                           <span class="number-of-ans-bage">0 Answer</span>
                           <div class="like-dislike-btns ml-auto d-inline-flex">
                              <a href="#" class="like-btn mr-2"><span class="material-icons">thumb_up</span> 50</a>
                              <a href="#" class="like-btn"><span class="material-icons">thumb_down</span> 0</a>
                           </div>
                        </div>
                     </div>

                     <div class="question_box b border_rounded_5 bg-white p_px_15 py-3 position-relative m_mb_20">
                        <span class="date font-12 font-400 text-dark">25-02-2020</span>
                        <ul class="Question_posted_by d-inline-flex align-items-center w-100 pl-0">
                           <li><a href="#" class="posted_name pl-0">
                              <span class="image ctk_overflow_hidden rounded-circle mr-2"><img src="images/user-img.png" class="img-fluid" alt=""></span> 
                              John Smith
                              </a>
                           </li>
                           <li><a href="#" class="posted_details">Ui Designer</a></li>
                        </ul>
                        <h4 class="question_name font-16 font-700 pb-1"><span class="icons icon-question mr-1"></span>What is your favorite training app, and why?</h4>
                        <div class="answer_like_dislike-box d-inline-flex align-items-center w-100 mt-2">
                           <span class="number-of-ans-bage">0 Answer</span>
                           <div class="like-dislike-btns ml-auto d-inline-flex">
                              <a href="#" class="like-btn mr-2"><span class="material-icons">thumb_up</span> 50</a>
                              <a href="#" class="like-btn"><span class="material-icons">thumb_down</span> 0</a>
                           </div>
                        </div>
                     </div>

                     <div class="question_box b border_rounded_5 bg-white p_px_15 py-3 position-relative m_mb_20">
                        <span class="date font-12 font-400 text-dark">25-02-2020</span>
                        <ul class="Question_posted_by d-inline-flex align-items-center w-100 pl-0">
                           <li><a href="#" class="posted_name pl-0">
                              <span class="image ctk_overflow_hidden rounded-circle mr-2"><img src="images/user-img.png" class="img-fluid" alt=""></span> 
                              John Smith
                              </a>
                           </li>
                           <li><a href="#" class="posted_details">Ui Designer</a></li>
                        </ul>
                        <h4 class="question_name font-16 font-700 pb-1"><span class="icons icon-question mr-1"></span>What is your favorite training app, and why?</h4>
                        <div class="answer_like_dislike-box d-inline-flex align-items-center w-100 mt-2">
                           <span class="number-of-ans-bage">0 Answer</span>
                           <div class="like-dislike-btns ml-auto d-inline-flex">
                              <a href="#" class="like-btn mr-2"><span class="material-icons">thumb_up</span> 50</a>
                              <a href="#" class="like-btn"><span class="material-icons">thumb_down</span> 0</a>
                           </div>
                        </div>
                     </div>

                     <div class="question_box b border_rounded_5 bg-white p_px_15 py-3 position-relative m_mb_20">
                        <span class="date font-12 font-400 text-dark">25-02-2020</span>
                        <ul class="Question_posted_by d-inline-flex align-items-center w-100 pl-0">
                           <li><a href="#" class="posted_name pl-0">
                              <span class="image ctk_overflow_hidden rounded-circle mr-2"><img src="images/user-img.png" class="img-fluid" alt=""></span> 
                              John Smith
                              </a>
                           </li>
                           <li><a href="#" class="posted_details">Ui Designer</a></li>
                        </ul>
                        <h4 class="question_name font-16 font-700 pb-1"><span class="icons icon-question mr-1"></span>What is your favorite training app, and why?</h4>
                        <div class="answer_like_dislike-box d-inline-flex align-items-center w-100 mt-2">
                           <span class="number-of-ans-bage">0 Answer</span>
                           <div class="like-dislike-btns ml-auto d-inline-flex">
                              <a href="#" class="like-btn mr-2"><span class="material-icons">thumb_up</span> 50</a>
                              <a href="#" class="like-btn"><span class="material-icons">thumb_down</span> 0</a>
                           </div>
                        </div>
                     </div>
                     
                     <div class="question_box b border_rounded_5 bg-white p_px_15 py-3 position-relative m_mb_20">
                        <span class="date font-12 font-400 text-dark">25-02-2020</span>
                        <ul class="Question_posted_by d-inline-flex align-items-center w-100 pl-0">
                           <li><a href="#" class="posted_name pl-0">
                              <span class="image ctk_overflow_hidden rounded-circle mr-2"><img src="images/user-img.png" class="img-fluid" alt=""></span> 
                              John Smith
                              </a>
                           </li>
                           <li><a href="#" class="posted_details">Ui Designer</a></li>
                        </ul>
                        <h4 class="question_name font-16 font-700 pb-1"><span class="icons icon-question mr-1"></span>What is your favorite training app, and why?</h4>
                        <div class="answer_like_dislike-box d-inline-flex align-items-center w-100 mt-2">
                           <span class="number-of-ans-bage">0 Answer</span>
                           <div class="like-dislike-btns ml-auto d-inline-flex">
                              <a href="#" class="like-btn mr-2"><span class="material-icons">thumb_up</span> 50</a>
                              <a href="#" class="like-btn"><span class="material-icons">thumb_down</span> 0</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

