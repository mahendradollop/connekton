<?php
//print_r($users);die;
?>


<!-- dashboard section start here -->
<section class="user_dashboard theme_gary d-inline-block w-100 h-100 pt-0">
	<?php include('includes/inner_sidebar.php'); ?>

	<div class="main-body">
		<div class="top-_bash_boxes">
			<div class="main_top_tiitle d-inline-flex align-items-center bg-white w-100 p_px_15 border_rounded_5 m_mb_20">
                <h4 class="font-18 font-500 mb-0">User Dashboard</h4>
            </div>
			<div class="row">
				<div class="col-md-3">
					<div class="dash-card position-relative border_rounded_5 d-inline-flex align-items-start w-100 position-relative">
						<div class="text-box">
							<h3 class="font-700 font-22 text-dark m-0">25+</h3>
							<p class="font-500 font-16 text-dark m-0">Projects</p>
						</div>
						<div class="icon-box d-inline-flex align-items-center justify-content-center ml-auto">
							<span class="icons icon-briefcase"></span>
						</div>
						<div class="dash_detail_box">
							<p class="font-12 runing mb-1">Runing <span class="dash_card_noti-icon float-right">15</span></p>
							<p class="font-12 Compeleted mb-1">Compeleted <span class="dash_card_noti-icon float-right">06</span></p>
							<p class="font-12 other mb-1">Other <span class="dash_card_noti-icon float-right">02</span></p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="dash-card position-relative border_rounded_5 d-inline-flex align-items-top w-100 position-relative">
						<div class="text-box">
							<h3 class="font-700 font-22 text-dark m-0">22</h3>
							<p class="font-500 font-16 text-dark m-0">My Jobs</p>
						</div>
						<div class="icon-box d-inline-flex align-items-center justify-content-center ml-auto bg-secondry">
							<img src="<?php echo base_url('assets/web/images/')?>/job.png" class="img-fluid">
						</div>
						<div class="dash_detail_box">
							<p class="font-12 runing mb-1">Applied Jobs<span class="dash_card_noti-icon float-right">20</span></p>
							<p class="font-12 Compeleted mb-1">Saved Jobs<span class="dash_card_noti-icon float-right">02</span></p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="dash-card position-relative border_rounded_5 d-inline-flex align-items-top w-100 position-relative">
						<div class="text-box">
							<h3 class="font-700 font-22 text-dark m-0">$1500</h3>
							<p class="font-500 font-16 text-dark m-0">Earning Details</p>
						</div>
						<div class="icon-box d-inline-flex align-items-center justify-content-center ml-auto bg-success">
							<span class="icons icon-earn-money"></span>
						</div>
						<div class="dash_detail_box">
							<p class="font-12 runing mb-1">Total <span class="dash_card_noti-icon float-right">$1500</span></p>
							<p class="font-12 Compeleted mb-1">Month <span class="dash_card_noti-icon float-right">06</span></p>
						</div>
					</div>
				</div>


				<div class="col-md-3">
					<div class="dash-card position-relative border_rounded_5 d-inline-flex align-items-top w-100 position-relative">
						<div class="text-box">
							<h3 class="font-700 font-22 text-dark m-0">25+</h3>
							<p class="font-500 font-16 text-dark m-0">Questions</p>
						</div>
						<div class="icon-box d-inline-flex align-items-center justify-content-center ml-auto bg-sky">
							<span class="icons icon-question"></span>
						</div>
						<div class="dash_detail_box">
							<p class="font-12 runing mb-1">Runing <span class="dash_card_noti-icon float-right">15</span></p>
							<p class="font-12 Compeleted mb-1">Compeleted <span class="dash_card_noti-icon float-right">06</span></p>
							<p class="font-12 other mb-1">Other <span class="dash_card_noti-icon float-right">02</span></p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="dash-card position-relative border_rounded_5 d-inline-flex align-items-top w-100 position-relative">
						<div class="text-box">
							<h3 class="font-700 font-22 text-dark m-0">22</h3>
							<p class="font-500 font-16 text-dark m-0">Articals</p>
						</div>
						<div class="icon-box d-inline-flex align-items-center justify-content-center ml-auto bg-yellow">
							<span class="icons icon-notes"></span>
						</div>
						<div class="dash_detail_box">
							<p class="font-12 runing mb-1">Applied Jobs<span class="dash_card_noti-icon float-right">20</span></p>
							<p class="font-12 Compeleted mb-1">Saved Jobs<span class="dash_card_noti-icon float-right">02</span></p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="dash-card position-relative border_rounded_5 d-inline-flex align-items-top w-100 position-relative">
						<div class="text-box">
							<h3 class="font-700 font-22 text-dark m-0">$1500</h3>
							<p class="font-500 font-16 text-dark m-0">Earning Details</p>
						</div>
						<div class="icon-box d-inline-flex align-items-center justify-content-center ml-auto">
							<span class=""></span>
						</div>
						<div class="dash_detail_box">
							<p class="font-12 runing mb-1">Total <span class="dash_card_noti-icon float-right">$1500</span></p>
							<p class="font-12 Compeleted mb-1">Month <span class="dash_card_noti-icon float-right">06</span></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

