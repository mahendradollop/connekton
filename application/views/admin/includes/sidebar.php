<div class="main-sidebar sidebar-style-2">
	<aside id="sidebar-wrapper">
		<div class="sidebar-brand">
			<a href="<?php echo base_url('dashboard'); ?>"> <img alt="image" src="<?php echo base_url(); ?>assets/logo.png" class="header-logo" />
			</a>
		</div>
		<ul class="sidebar-menu">
			<li class="dropdown active"><a href="<?php echo base_url('dashboard'); ?>" class="nav-link"><i data-feather="monitor"></i><span>Dashboard</span>
				</a>
			</li>
			<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="users"></i><span>User Managment</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="<?php echo base_url('user_list') ?>">Users</a></li>
					<li><a class="nav-link" href="<?php echo base_url('client_list') ?>">Client</a></li>
					<li><a class="nav-link" href="<?php echo base_url('employer_list') ?>">Employers</a></li>
					<li class="dropdown"><a href="#" class="nav-link has-dropdown"><span>KP</span></a>
						<ul class="dropdown-menu">
							<li><a class="nav-link" href="<?php echo base_url('approved_kp_list') ?>">Approved KP</a></li>
							<li><a class="nav-link" href="<?php echo base_url('unapproved_kp_list') ?>">Unapproved KP</a></li>							
						</ul>
					</li >
				</ul>
			</li>

			<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="send"></i><span>Plan Managment</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="<?php echo base_url("plan"); ?>">Add Plan</a></li>
					<li><a class="nav-link" href="<?php echo base_url("view_plan"); ?>">View Plan</a></li>

				</ul>
			</li>
			<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="message-square"></i><span>Article Managment</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="<?php echo base_url("view_article"); ?>">Article List</a></li>

				</ul>
			</li>
			<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="message-square"></i><span>Question Managment</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="<?php echo base_url("view_question"); ?>">Question List</a></li>

				</ul>
			</li>

		</ul>
	</aside>
</div>