<?php include('includes/header.php') ?>;
<?php include('includes/sidebar.php') ?>;
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>All Plan</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="ShowPlan" class="table table-striped table-hover" id="tableExport" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Fee</th>
                                            <th>Duration</th>
                                            <th>Description</th>
                                            <th>Plan For</th>
                                            <th>Question Amount</th>
                                            <th>Question Limit</th>
                                            <th>Role Type</th>
                                            <th>Active</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- <tr>
                                            <td>Tiger Nixon</td>
                                            <td>System Architect</td>
                                            <td>Edinburgh</td>
                                            <td>61</td>
                                            <td>2011/04/25</td>
                                            <td>$320,800</td>
                                        </tr>
                                        <tr>
                                            <td>Garrett Winters</td>
                                            <td>Accountant</td>
                                            <td>Tokyo</td>
                                            <td>63</td>
                                            <td>2011/07/25</td>
                                            <td>$170,750</td>
                                        </tr>
                                        <tr>
                                            <td>Ashton Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>San Francisco</td>
                                            <td>66</td>
                                            <td>2009/01/12</td>
                                            <td>$86,000</td>
                                        </tr> -->

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include('includes/footer.php') ?>;
<script>
    var token = '<?php if (isset($_SESSION['ConneKton_session']['admin_token']) && $_SESSION['ConneKton_session']['admin_token'] != "") {
                        echo $_SESSION['ConneKton_session']['admin_token'];
                    } ?>';
    $(document).ready(function() {

        $.ajax({
            url: "<?php echo base_url('getPlan') ?>",
            type: "GET",
            headers: {
                'Authorization': token
            },
            dataType: "json",
            success: function(data) {
               // console.log(data.plan);
                var plan = data.plan;
                $.each(data.plan, function(i, value) {
                    if (plan[i]['is_active'] == "active") {
                        id = plan[i]['plan_id'];
                        statusChange = 0;
                        var checked = "checked";
                    } else {
                        id = plan[i]['plan_id'];
                        var checked = "";
                        statusChange = 1;
                    }
                    toggle = "<div class='toggle-switch'><label class='toggle-switch switch'  id='swal-6'><input   type='checkbox' " + checked + " onclick='ChangeStatus(" + id + "," + statusChange + ")'   class= 'check " + id + "'   name = 'check' id = 'check'><span  class='slider-switch round'></span> </label></div>";
                    $("#ShowPlan tbody:last-child").append(
                        '<tr>' +
                        '<td>' + plan[i]['plan_title'] + '</td>' +
                        '<td>' + plan[i]['fee'] + '</td>' +
                        '<td>' + plan[i]['duration'] + '</td>' +
                        '<td>' + plan[i]['description'] + '</td>' +
                        '<td>' + plan[i]['plan_for'] + '</td>' +
                        '<td>' + plan[i]['question_amount'] + '</td>' +
                        '<td>' + plan[i]['question_limit'] + '</td>' +
                        '<td>' + plan[i]['role_type'] + '</td>' +
                        "<td>" + toggle + " </td> " +
                        '<td> <a href="plan?id=' + plan[i]['plan_id'] + '"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit"><path d="M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7"></path><path d="M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z"></path></svg></a>  </td>' +
                        '<td> <a href="javascript:void(0)" class="deleteClass" onclick="deleteFunction(' + plan[i]['plan_id'] + ')"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#96a2b4" width="24px" height="24px"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z"></path></svg></a>  </td>' +
                        '</tr>'
                    );
                    $('#ShowPlan').DataTable({
                        dom: 'Bfrtip',
                        buttons: [
                            'csv', 'excel', 'pdf',
                        ]
                    });
                });
            },
            error: function(data) {

            }
        });

    });


    function deleteFunction(id) {
        $.ajax({
            url: '<?php echo base_url() ?>addPlan',
            type: 'POST',
            headers: {
                'Authorization': token
            },
            data: {
                is_delete: "deleted",
                plan_id: id
            },
            dataType: 'json',
            error: function(response) {
                iziToast.error({
                    title: 'Paln',
                    message: "Paln Not Deleted ",
                    position: 'topRight'
                });
            },
            success: function(response) {
                if (response) {
                    iziToast.success({
                        title: 'Plan',
                        message: " Plan Deleted Succefully",
                        position: 'topRight'
                    });
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 3000);
                }
            }

        });
    }

    function ChangeStatus(id, status) {
        $.ajax({
            url: '<?php echo base_url() ?>addPlan',
            type: 'POST',
            headers: {
                'Authorization': token
            },
            data: {
                is_active: status,
                plan_id: id
            },
            dataType: 'json',
            error: function(response) {
                iziToast.error({
                    title: 'Plan',
                    message: "Plan Status Not Change ",
                    position: 'topRight'
                });
            },
            success: function(response) {
                if (response) {
                    iziToast.success({
                        title: 'Plan',
                        message: " Plan Status Change Succefully",
                        position: 'topRight'
                    });
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 3000);
                }
            }

        });
    }
</script>