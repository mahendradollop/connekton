<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Reset Password - ConnecKton</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/app.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bundles/bootstrap-social/bootstrap-social.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='<?php echo base_url(); ?>assets/logo.png' />
  <style type="text/css">

        .error{

            color: red !important;

        }

    </style>
</head>
<body>
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <img style="margin-bottom: 25px;" src="<?php echo base_url(); ?>assets/logo.png" width="240" height="25">
            <div class="card card-primary">

              <div class="card-header">
                <h4>Forgot Password</h4>
                <!--  -->
              </div>
              <?php if($this->session->flashdata('success')){ ?>
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php }if($this->session->flashdata('error')){?>
                <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                   <?php echo $this->session->flashdata('error'); ?>
                </div>
              <?php  } ?>
              <div class="card-body">
                <form method="POST" id="myForm" action="<?php echo base_url('change_forgeted_password')?>" class="needs-validation" novalidate="">
                  <div class="form-group">
                    <label for="new_password_id">New Password</label>
                    <input id="new_password_id" type="password" class="form-control" name="new_password" tabindex="1" required autofocus>
                    <!-- <div class="invalid-feedback">
                      Please fill in your new password
                    </div> -->
                  </div>

                  <div class="form-group">
                    <label for="confirm_password">Confirm Password</label>
                    <input id="confirm_password" type="password" class="form-control" name="confirm_password" tabindex="1" required autofocus>
                    <!-- <div class="invalid-feedback">
                      Please fill in your confirm password
                    </div> -->
                  </div>
                  
                  
                  <div class="form-group">
                    <input type="hidden" name="token" value="<?php echo($this->uri->segment(2)); ?>">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                      Send
                    </button>
                  </div>
                  <div class="form-group">                   
                    <div class="text-center">
                        Already have an account? 
                        <a style="color: white" href="<?php echo base_url('admin')?>" class="text-small">
                          Login
                        </a>
                      </div>
                  </div>
                </form>
              </div>
            </div>
        </div>
      </div>
    </section>
  </div>
  <!-- General JS Scripts -->
  <script src="<?php echo base_url(); ?>assets/admin/js/app.min.js"></script>
  <!-- JS Libraies -->
  <!-- Page Specific JS File -->
  <!-- Template JS File -->
  <script src="<?php echo base_url(); ?>assets/admin/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="<?php echo base_url(); ?>assets/admin/js/custom.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>


    
  <script type="text/javascript">
      jQuery(function ($) {

        $('#myForm').validate({
            rules: {
                new_password: {
                    required: true,
                    minlength: 6,
                },
                confirm_password: {
                    required: true,
                    minlength: 6,
                    equalTo: "#new_password_id"
                },
            },

            messages: {
                new_password: {
                    required: "Please enter new password",
                    minlength: "password should be greater then 6 characters",
                },

                confirm_password: {
                    required: "Please enter confirm password",
                    minlength: "Password should be greater then 6 characters",
                    equalTo: "New password and confirm password are not matched",
                },

            },

        });

    });
</script>
</body>
</html>