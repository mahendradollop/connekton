<?php include('includes/header.php') ?>;
<?php include('includes/sidebar.php') ?>;
<div class="main-content">

    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <form class="needs-validation" name="planForm" id="plan" novalidate="">
                            <div class="card-header">
                                <h4>Add Plan</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Plan Title</label>                                   
                                    <input type="hidden" name="plan_id" value="<?php if (isset($planById['plan_id']) && $planById['plan_id'] != "") {
                                                                                    echo $planById['plan_id'];
                                                                                } ?>">
                                    <input type="text" value="<?php if (isset($planById['plan_title']) && $planById['plan_title'] != "") {
                                                                    echo $planById['plan_title'];
                                                                } ?>" class="form-control" id="plan_title" name="plan_title" required="">

                                </div>
                                <div class="form-group">
                                    <label>Fee</label>
                                    <input type="number" value="<?php if (isset($planById['fee']) && $planById['fee'] != "") {
                                                                    echo $planById['fee'];
                                                                } ?>" id="fee" name="fee" class="form-control" required="">

                                </div>
                                <div class="form-group ">
                                    <label>Description</label>
                                    <textarea class="form-control" id="description" name="description" required="" value="<?php if (isset($planById['description']) && $planById['description'] != "") {
                                                                                                                                echo $planById['description'];
                                                                                                                            } ?>"><?php if (isset($planById['description']) && $planById['description'] != "") {
                                                                                                                                        echo $planById['description'];
                                                                                                                                    } ?></textarea>

                                </div>
                                <div class="form-group">
                                    <label>Duration</label>
                                    <select class="form-control" name="duration" id="duration">
                                        <option>Select Duration</option>
                                        <option <?php if (isset($planById['duration']) && $planById['duration'] == 3) {
                                                    echo "selected";
                                                } ?> value="3">3 Months</option>
                                        <option <?php if (isset($planById['duration']) && $planById['duration'] == 12) {
                                                    echo "selected";
                                                } ?> value="12">12 Months</option>
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label>Role</label>
                                    <select class="form-control" name="role" id="role">
                                        <option>Select Role</option>
                                        <option <?php if (isset($planById['role_type']) && $planById['role_type'] == "Client") {
                                                    echo "selected";
                                                } ?> value="Client">Client</option>
                                        <option <?php if (isset($planById['role_type']) && $planById['role_type'] == "Employer") {
                                                    echo "selected";
                                                } ?> value="Employer">Employer</option>
                                        <option <?php if (isset($planById['role_type']) && $planById['role_type'] == "User") {
                                                    echo "selected";
                                                } ?> value="User">User</option>
                                        <option <?php if (isset($planById['role_type']) && $planById['role_type'] == "KP") {
                                                    echo "selected";
                                                } ?> value="KP">KP'S</option>
                                    </select>
                                </div>
                                <div class="planType">
                                    <div class="form-group ">
                                        <label>Plan For</label>
                                        <select class="form-control" name="plan_for" id="plan_for">
                                            <option>Select Type</option>
                                            <option <?php if (isset($planById['plan_for']) && $planById['plan_for'] == "Advertise") {
                                                        echo "selected";
                                                    } ?> value="Advertise">Advertise</option>
                                            <option <?php if (isset($planById['plan_for']) && $planById['plan_for'] == "Question") {
                                                        echo "selected";
                                                    } ?> value="Question">Question</option>
                                            <option <?php if (isset($planById['plan_for']) && $planById['plan_for'] == "Both") {
                                                        echo "selected";
                                                    } ?> value="Both">Both</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label> Question Limit</label>
                                        <input type="number" value="<?php if (isset($planById['question_limit']) && $planById['question_limit'] != "") {
                                                                        echo $planById['question_limit'];
                                                                    } ?>" name="question_limit" id="question_limit" required="" class="form-control">

                                    </div>
                                    <div class="form-group">
                                        <label>Per Question Pay amount</label>
                                        <input type="number" value="<?php if (isset($planById['question_amount']) && $planById['question_amount'] != "") {
                                                                        echo $planById['question_amount'];
                                                                    } ?>" name="question_amount" id="question_amount" required="" class="form-control">

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary" id="submitBtn">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
<?php include('includes/footer.php') ?>;
<script>
    $('#role').on('change', function() {
        if (this.value == "Client") {
            $('.planType').show();
        } else {
            $('.planType').hide();
        }
    });
    <?php if (isset($planById['role_type']) && $planById['role_type'] == "Client") { ?>
        // alert("dfhdsjfdjf");
        $('.planType').css('display', "block");
    <?php  } else {  ?>
        $('.planType').css('display', "none");
    <?php } ?>
</script>

<script>
    var token = '<?php if (isset($_SESSION['ConneKton_session']['admin_token']) && $_SESSION['ConneKton_session']['admin_token'] != "") {
                        echo $_SESSION['ConneKton_session']['admin_token'];
                    } ?>';
    $(document).ready(function() {
        $("#plan").validate({
            rules: {
                plan_title: "required",
                fee: "required",
                description: "required",
                duration: "required",
                role: "required",
                plan_for: "required",
                question_limit: "required",
                question_amount: "required",

            },
            messages: {
                plan_title: "Please enter plan title",
                fee: "Please enter fee",
                description: "Please enter description",
                duration: "Please enter duration",
                question_amount: "Please enter question amount",
                role: "Please select role a valid email address",
                plan_for: "Please select Plan For",
                question_limit: "Please enter Limits of the question",
            },
            submitHandler: function(form) {
               // form.submit();
                // $planData = $('#plan').serialize();
                // alert($planData);
                $.ajax({
                    url: "<?php echo base_url('addPlan') ?>",
                    type: "POST",
                    dataType: "json",
                    headers: {
                        'Authorization': token
                    },
                    data: $('#plan').serialize(),
                    success: function(data) {
                        console.log(data);
                       // alert(data);

                        <?php if (isset($_GET['id']) && $_GET['id']) { ?>
                            iziToast.success({
                                title: 'Plan',
                                message: 'Plan Update Successfully',
                                position: 'topRight'
                            });
                        <?php } else { ?>
                            iziToast.success({
                                title: 'Plan',
                                message: 'Plan Added Successfully',
                                position: 'topRight'
                            });
                        <?php } ?>
                        window.location.replace("<?php echo base_url("view_plan") ?>");
                    },
                    error: function(data) {
                        iziToast.error({
                            title: 'Plan',
                            message: 'Plan Not Added ',
                            position: 'topRight'
                        });
                    }
                });
            }
        });


    });
</script>

<style>
    .planType {
        display: none;
    }

    .error {
        color: red;
    }
</style>