<?php //print_r($_SESSION['ConneKton_session']['admin_token']);die; 
?><?php include('includes/header.php') ?>;
<?php include('includes/sidebar.php') ?>;
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Export Table</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="ShowArticle" class="table table-striped table-hover articleTable" id="tableExport" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th>Cover Image</th>
                                            <th>Article Title</th>
                                            <th>Name</th>
                                            <th>Posted By</th>
                                            <th>Comments</th>
                                            <th>Like/Dislike</th>
                                            <th>Reports</th>
                                            <th>Active</th>
                                            <th>Delete</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- <tr>
                                            <td>Tiger Nixon</td>
                                            <td>System Architect</td>
                                            <td>Edinburgh</td>
                                            <td>61</td>
                                            <td>2011/04/25</td>
                                            <td>$320,800</td>
                                        </tr>
                                        <tr>
                                            <td>Garrett Winters</td>
                                            <td>Accountant</td>
                                            <td>Tokyo</td>
                                            <td>63</td>
                                            <td>2011/07/25</td>
                                            <td>$170,750</td>
                                        </tr>
                                        <tr>
                                            <td>Ashton Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>San Francisco</td>
                                            <td>66</td>
                                            <td>2009/01/12</td>
                                            <td>$86,000</td>
                                        </tr> -->

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-static-2 mb-30">
                            <div class="card-body-table" id="showDetail">
                                <!--<div class="shopowner-content-left pd-20 modalId">
                  <div class="shopowner-dt-left">
                    <h4>Vivian Harrell</h4>
                  </div>
                  <div class="shopowner-dts">
                    <div class="shopowner-dt-list">
                      <span class="left-dt"><b>Name</b></span>
                      <span class="right-dt">Vivian Harrell</span>
                    </div>
                    <div class="shopowner-dt-list">
                      <span class="left-dt"><b>Mobile Number</b></span>
                      <span class="right-dt">9876543210</span>
                    </div>
                    <div class="shopowner-dt-list">
                      <span class="left-dt"><b>Email</b></span>
                      <span class="right-dt">sanFrancisco@gmail.com</span>
                    </div>
                    <div class="shopowner-dt-list">
                      <span class="left-dt"><b>Plan Type</b></span>
                      <span class="right-dt">Weekly</span>
                    </div>
                    <div class="shopowner-dt-list">
                      <span class="left-dt"><b>Status</b></span>
                      <span class="right-dt">Active</span>
                    </div>
                  </div>
                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="likeModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modelCont">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-static-2 mb-30">
                            <div class="card-body-table">
                                <div class="shopowner-content-left pd-20 modalId">
                                    <div class="shopowner-dt-left">
                                        <h4>Likes</h4>
                                    </div>
                                    <div class="shopowner-dts" id="showLike">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="commentModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modelCont">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-static-2 mb-30">
                            <div class="card-body-table">
                                <div class="shopowner-content-left pd-20 modalId">
                                    <div class="shopowner-dt-left">
                                        <h4>Comments</h4>
                                    </div>
                                    <div class="shopowner-dts" id="showComment">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="reportModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modelCont">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-static-2 mb-30">
                            <div class="card-body-table">
                                <div class="shopowner-content-left pd-20 modalId">
                                    <div class="shopowner-dt-left">
                                        <h4>Reports</h4>
                                    </div>
                                    <div class="shopowner-dts" id="showReport">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('includes/footer.php') ?>;

<script>
    var token = '<?php if (isset($_SESSION['ConneKton_session']['admin_token']) && $_SESSION['ConneKton_session']['admin_token'] != "") {
                        echo $_SESSION['ConneKton_session']['admin_token'];
                    } ?>';
    $(document).ready(function() {

        $.ajax({
            url: "<?php echo base_url('getArticle') ?>",
            type: "GET",
            dataType: "json",
            async: false,
            headers: {
                'Authorization': token
            },
            success: function(data) {
                // console.log(data);
                var article = data.data;
                $.each(article, function(i, value) {
                    //  console.log(value);
                    if (article[i]['is_active'] == "active") {
                        id = article[i]['article_id'];
                        statusChange = 0;
                        var checked = "checked";
                    } else {
                        id = article[i]['article_id'];
                        var checked = "";
                        statusChange = 1;
                    }
                    if (article[i]['type'] == null) {
                        article[i]['type'] = "No article Selected";
                    }
                    toggle = "<div class='toggle-switch'><label class='toggle-switch switch'  id='swal-6'><input   type='checkbox' " + checked + " onclick='ChangeStatus(" + id + "," + statusChange + ")'   class= 'check " + id + "'   name = 'check' id = 'check'><span  class='slider-switch round'></span> </label></div>";
                    $("#ShowArticle tbody:last-child").append(
                        '<tr>' +
                        '<td><img width="50px" height="50px" src="' + article[i]['cover_image'] + '"></td>' +
                        '<td>' + article[i]['article_title'] + '</td>' +
                        '<td>' + article[i]['full_name'] + '</td>' +
                        '<td>' + article[i]['posted_by'] + '</td>' +
                        '<td>' + article[i]['all_comment'] + '<svg onclick = "commentModel(' + article[i]['article_id'] + ')" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-square"><path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path></svg></td>' +
                        '<td onclick = "LikeModel(' + article[i]['article_id'] + ')">  ' + article[i]['all_like_dislike'] + '<svg  xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg><svg xmlns="" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg></td>' +
                        '<td onclick = "reportModel(' + article[i]['article_id'] + ')">' + article[i]['all_report'] + '</td>' +
                        "<td>" + toggle + " </td> " +
                        '<td> <a href="#" class="deleteClass" onclick="deleteFunction(' + article[i]['article_id'] + ')"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#96a2b4" width="24px" height="24px"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z"></path></svg></a>  </td>' +
                        '<td> <a href="javascript:void(0)" onclick="actionFunction(' + article[i]['article_id'] + ')" data-toggle="modal" data-target="#exampleModalCenter"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></a> </td>' +
                        '</tr>'
                    );

                });
                $('.articleTable').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'csv', 'excel', 'pdf',
                    ]
                });
            },
            error: function(data) {

            }
        });

    });

    function commentModel(id) {
        // alert(id);
        var showComment = "";
        $.ajax({
            url: '<?php echo base_url() ?>getAtricleCommentReportLike/' + id,
            type: 'GET',
            headers: {
                'Authorization': token
            },
            dataType: 'json',
            success: function(response) {
                $('#commentModel').modal();
                //  console.log(response.comment);
                $.each(response.comment, function(index, value) {
                    showComment = " <div class='shopowner-dt-list'>" +
                        "<span class='left-dt'>" +
                        "<b>" + value['full_name'] + "</b>" +
                        "</span>" +
                        "<span class='right-dt'>" + value['comment'] + "</span>" +
                        "</div>";
                    $('#showComment').html(showComment);
                });
            }
        });
    }

    function LikeModel(id) {
        //alert(id);
        var showComment = "";
        $.ajax({
            url: '<?php echo base_url() ?>getAtricleCommentReportLike/' + id,
            type: 'GET',
            headers: {
                'Authorization': token
            },
            dataType: 'json',
            success: function(response) {
                $('#likeModel').modal();
                //  console.log(response.comment);
                $.each(response.likeDislike, function(index, value) {
                    showComment = " <div class='shopowner-dt-list'>" +
                        "<span class='left-dt'>" +
                        "<b>" + value['full_name'] + "</b>" +
                        "</span>" +
                        "<span class='right-dt'>" + value['action'] + "</span>" +
                        "</div>";
                    $('#showLike').html(showComment);
                });
            }
        });
    }

    function reportModel(id) {
        //alert(id);
        var showComment = "";
        $.ajax({
            url: '<?php echo base_url() ?>getAtricleCommentReportLike/' + id,
            type: 'GET',
            headers: {
                'Authorization': token
            },
            dataType: 'json',
            success: function(response) {
                $('#reportModel').modal();
                //console.log(response.message);
                if (response.message == "Success") {
                    $.each(response.report, function(index, value) {
                        showComment = " <div class='shopowner-dt-list'>" +
                            "<span class='left-dt'>" +
                            "<b>" + value['full_name'] + "</b>" +
                            "</span>" +
                            "<span class='right-dt'>" + value['content'] + "</span>" +
                            "</div>";
                        $('#showReport').html(showComment);
                    });
                }

            }
        });
    }

    function deleteFunction(id) {
        $.ajax({
            url: '<?php echo base_url() ?>addArticle',
            type: 'POST',
            headers: {
                'Authorization': token
            },
            data: {
                is_delete: "deleted",
                article_id: id
            },
            dataType: 'json',
            error: function(response) {
                iziToast.error({
                    title: 'Article',
                    message: "Article Not Deleted ",
                    position: 'topRight'
                });
            },
            success: function(response) {
                if (response) {
                    iziToast.success({
                        title: 'Article',
                        message: " Article Deleted Succefully",
                        position: 'topRight'
                    });
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 3000);
                }
            }

        });
    }

    function ChangeStatus(id, status) {
        // alert(id);
        $.ajax({
            url: '<?php echo base_url() ?>addArticle',
            type: 'POST',
            headers: {
                'Authorization': token
            },
            data: {
                is_active: status,
                article_id: id
            },
            dataType: 'json',
            error: function(response) {
                iziToast.error({
                    title: 'Article',
                    message: "Article Status Not Change ",
                    position: 'topRight'
                });
            },
            success: function(response) {
                if (response) {
                    iziToast.success({
                        title: 'Article',
                        message: " Article Status Change Succefully",
                        position: 'topRight'
                    });
                    // setTimeout(function() {
                    //     window.location.reload(1);
                    // }, 3000);
                }
            }

        });
    }

    function actionFunction(articleId) {
        var active = "";
        var type = "";
        var Token = '<?php echo $this->session->userdata("token"); ?>';
        var checkBox = document.getElementById("check");
        $.ajax({
            url: '<?php echo base_url() ?>getArticle/' + articleId,
            type: 'GET',
            dataType: 'json',
            headers: {
                // 'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': token
            },
            error: function() {
                swal("Some Error", "", "error");
            },
            success: function(response) {
                //console.log(response);
                if (response.data['is_active'] == "active") {
                    var active = "Active";
                } else {
                    var active = "Inactive";
                }
                showDetails = "<div class='shopowner-content-left pd-20 modalId'>" +
                    "<div class='shopowner-dt-left'>" +
                    "<h4>Article</h4>" +
                    "</div>" +
                    "<div class='shopowner-dts'>" +
                    "<div class='shopowner-dt-list'>" +
                    "<span class='left-dt'>" +
                    "<b>Title</b>" +
                    "</span>" +
                    "<span class='right-dt'>" + response.data['article_title'] + "</span>" +
                    "</div>" +
                    "<div class='shopowner-dt-list'>" +
                    "<span class='left-dt'><b>User Name</b></span>" +
                    "<span class='right-dt'>" + response.data['full_name'] + "</span>" +
                    "</div>" +
                    "<div class='shopowner-dt-list'>" +
                    "<span class='left-dt'><b>Content</b></span>" +
                    "<span class='right-dt'>" + response.data['content'] + "</span>" +
                    "</div>" +
                    "<div class='shopowner-dt-list'>" +
                    "<span class='left-dt'><b>Posted By</b></span>" +
                    "<span class='right-dt'>" + response.data['posted_by'] + "</span>" +
                    "</div>" +
                    "<div class='shopowner-dt-list'>" +
                    "<span class='left-dt'><b>Statu</b></span>" +
                    "<span class='right-dt'>" + active + "</span>" +
                    "</div>" +
                    "</div>" +
                    "</div>";
                $('#showDetail').html(showDetails);

            }
        });

    }
</script>
<style>
    .modelCont {
        background-color: #1d2026;
    }
</style>