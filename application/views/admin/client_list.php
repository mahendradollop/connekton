<?php //print_r($_SESSION['ConneKton_session']['admin_token']);die; 
?><?php include('includes/header.php') ?>;
<?php include('includes/sidebar.php') ?>;
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>User Table</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="ShowUser" class="table table-striped table-hover userTable" id="tableExport" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th>Profile</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Role</th>
                                            <th>Location</th>
                                            <th>Active</th>
                                            <th>Delete</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- <tr>
                                            <td>Tiger Nixon</td>
                                            <td>System Architect</td>
                                            <td>Edinburgh</td>
                                            <td>61</td>
                                            <td>2011/04/25</td>
                                            <td>$320,800</td>
                                        </tr>
                                        <tr>
                                            <td>Garrett Winters</td>
                                            <td>Accountant</td>
                                            <td>Tokyo</td>
                                            <td>63</td>
                                            <td>2011/07/25</td>
                                            <td>$170,750</td>
                                        </tr>
                                        <tr>
                                            <td>Ashton Cox</td>
                                            <td>Junior Technical Author</td>
                                            <td>San Francisco</td>
                                            <td>66</td>
                                            <td>2009/01/12</td>
                                            <td>$86,000</td>
                                        </tr> -->

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-static-2 mb-30">
                            <div class="card-body-table" id="showDetail">
                                <!--<div class="shopowner-content-left pd-20 modalId">
                  <div class="shopowner-dt-left">
                    <h4>Vivian Harrell</h4>
                  </div>
                  <div class="shopowner-dts">
                    <div class="shopowner-dt-list">
                      <span class="left-dt"><b>Name</b></span>
                      <span class="right-dt">Vivian Harrell</span>
                    </div>
                    <div class="shopowner-dt-list">
                      <span class="left-dt"><b>Mobile Number</b></span>
                      <span class="right-dt">9876543210</span>
                    </div>
                    <div class="shopowner-dt-list">
                      <span class="left-dt"><b>Email</b></span>
                      <span class="right-dt">sanFrancisco@gmail.com</span>
                    </div>
                    <div class="shopowner-dt-list">
                      <span class="left-dt"><b>Plan Type</b></span>
                      <span class="right-dt">Weekly</span>
                    </div>
                    <div class="shopowner-dt-list">
                      <span class="left-dt"><b>Status</b></span>
                      <span class="right-dt">Active</span>
                    </div>
                  </div>
                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include('includes/footer.php') ?>;

<script>
    var token = '<?php if (isset($_SESSION['ConneKton_session']['admin_token']) && $_SESSION['ConneKton_session']['admin_token'] != "") {
                        echo $_SESSION['ConneKton_session']['admin_token'];
                    } ?>';
    $(document).ready(function() {

        $.ajax({
            url: "<?php echo base_url('getAllClientByAdmin') ?>",
            type: "GET",
            dataType: "json",
            async: false,
            headers: {
                'Authorization': token
            },
            success: function(data) {
                // console.log(data);
                var user = data.data;
                $.each(user, function(i, value) {
                    //  console.log(value);
                    if (user[i]['is_active'] == "Active") {
                        id = user[i]['user_id'];
                        statusChange = 0;
                        var checked = "checked";
                    } else {
                        id = user[i]['user_id'];
                        var checked = "";
                        statusChange = 1;
                    }
                    if (user[i]['type'] == null) {
                        user[i]['type'] = "No user Selected";
                    }
                    toggle = "<div class='toggle-switch'><label class='toggle-switch switch'  id='swal-6'><input   type='checkbox' " + checked + " onclick='ChangeStatus(" + id + "," + statusChange + ")'   class= 'check " + id + "'   name = 'check' id = 'check'><span  class='slider-switch round'></span> </label></div>";
                    $("#ShowUser tbody:last-child").append(
                        '<tr>' +
                        '<td><img width="50px" height="50px" src="' + user[i]['profile_pic'] + '"></td>' +
                        '<td>' + user[i]['full_name'] + '</td>' +
                        '<td>' + user[i]['email'] + '</td>' +
                        '<td>' + user[i]['phone_number'] + '</td>' +
                        '<td>' + user[i]['user_role'] + '</td>' +
                        '<td>' + user[i]['location'] + '</td>' +
                        "<td>" + toggle + " </td> " +
                        '<td> <a href="javascript:void(0)" class="deleteClass" onclick="deleteFunction(' + user[i]['user_id'] + ')"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#96a2b4" width="24px" height="24px"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z"></path></svg></a>  </td>' +
                        '<td> <a href="javascript:void(0)" onclick="actionFunction(' + user[i]['user_id'] + ')" data-toggle="modal" data-target="#exampleModalCenter"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></a> </td>' +
                        '</tr>'
                    );

                });
                $('.userTable').DataTable({
                    //dom: 'Bfrtip',
                    // buttons: [
                    //     'csv', 'excel', 'pdf',
                    // ]
                });
            },
            error: function(data) {

            }
        });

    });



    function deleteFunction(id) {
        $.ajax({
            url: '<?php echo base_url() ?>updateUserByAdmin',
            type: 'PUT',
            headers: {
                'Authorization': token
            },
            data: {
                is_delete: "deleted",
                user_id: id
            },
            dataType: 'json',
            error: function(response) {
                iziToast.error({
                    title: 'User',
                    message: "User Not Deleted ",
                    position: 'topRight'
                });
            },
            success: function(response) {
                if (response) {
                    iziToast.success({
                        title: 'User',
                        message: " User Deleted Succefully",
                        position: 'topRight'
                    });
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 3000);
                }
            }

        });
    }

    function ChangeStatus(id, status) {
        $.ajax({
            url: '<?php echo base_url() ?>updateUserByAdmin',
            type: 'PUT',
            headers: {
                'Authorization': token
            },
            data: {
                is_active: status,
                user_id: id
            },
            dataType: 'json',
            error: function(response) {
                iziToast.error({
                    title: 'User',
                    message: "User Status Not Change ",
                    position: 'topRight'
                });
            },
            success: function(response) {
                if (response) {
                    iziToast.success({
                        title: 'User',
                        message: " User Status Change Succefully",
                        position: 'topRight'
                    });
                    // setTimeout(function() {
                    //     window.location.reload(1);
                    // }, 3000);
                }
            }

        });
    }

    function actionFunction(id) {
        var active = "";
        var type = "";
        var Token = '<?php echo $this->session->userdata("token"); ?>';
        var checkBox = document.getElementById("check");
        $.ajax({
            url: '<?php echo base_url() ?>getClientByAdmin/' + id,
            type: 'GET',
            dataType: 'json',
            headers: {
                // 'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': token
            },
            error: function() {
                swal("Some Error", "", "error");
            },
            success: function(response) {
                console.log(response);
                if (response.data['is_active'] == "Active") {
                    var active = "Active";
                } else {
                    var active = "Inactive";
                }
                showDetails = "<div class='shopowner-content-left pd-20 modalId'>" +
                    "<div class='shopowner-dt-left'>" +
                    "<h4>Client</h4>" +
                    "</div>" +
                    "<div class='shopowner-dts'>" +
                    "<div class='shopowner-dt-list'>" +
                    "<span class='left-dt'><b>Profile</b></span>" +
                    "<span class='right-dt'><img width='80px'height='50px' src='" + response.data['profile_pic'] + "'></span>" +
                    "</div>" +
                    "<div class='shopowner-dt-list'>" +
                    "<span class='left-dt'>" +
                    "<b>Name</b>" +
                    "</span>" +
                    "<span class='right-dt'>" + response.data['full_name'] + "</span>" +
                    "</div>" +
                    "<div class='shopowner-dt-list'>" +
                    "<span class='left-dt'><b>Email</b></span>" +
                    "<span class='right-dt'>" + response.data['email'] + "</span>" +
                    "</div>" +
                    "<div class='shopowner-dt-list'>" +
                    "<span class='left-dt'><b>Mobile</b></span>" +
                    "<span class='right-dt'>" + response.data['phone_number'] + "</span>" +
                    "</div>" +
                    "<div class='shopowner-dt-list'>" +
                    "<span class='left-dt'><b>Country</b></span>" +
                    "<span class='right-dt'>" + response.data['country_name'] + "</span>" +
                    "</div>" +
                    "<div class='shopowner-dt-list'>" +
                    "<span class='left-dt'><b>State</b></span>" +
                    "<span class='right-dt'>" + response.data['state_name'] + "</span>" +
                    "</div>" +
                    "<div class='shopowner-dt-list'>" +
                    "<span class='left-dt'><b>City</b></span>" +
                    "<span class='right-dt'>" + response.data['city_name'] + "</span>" +
                    "</div>" +
                    "<div class='shopowner-dt-list'>" +
                    "<span class='left-dt'><b>Location</b></span>" +
                    "<span class='right-dt'>" + response.data['location'] + "</span>" +
                    "</div>" +
                    "<div class='shopowner-dt-list'>" +
                    "<span class='left-dt'><b>Statu</b></span>" +
                    "<span class='right-dt'>" + active + "</span>" +
                    "</div>" +
                    "</div>" +
                    "</div>";
                $('#showDetail').html(showDetails);

            }
        });

    }
</script>
<style>
    .modelCont {
        background-color: #1d2026;
    }
</style>