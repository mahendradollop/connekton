<?php

/**
 * @Author:Meenakshi
 *Its Provide services
 */
class User_service extends MY_Service
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('AuthModel');
    $this->load->model('Common_model');
    
  }

  public function check_mobile($mobile)
  {
    $where = array('mobile'=>$mobile);
    return $this->Common_model->getDataWhere('otp','*',$where);    
  }
  public function update_otp($mobile)
  {
    $where = array('mobile'=>$mobile);
    $data = array('otp'=>rand(1111,9999));
    return $this->Common_model->updateData('otp', $where, $data);     
  }
  public function insert_otp($data)
  {
    $data = array('mobile'=>$data,'otp'=>rand(1111,9999));
    return  $this->Common_model->insertData('otp',$data); 
  }
  public function verify_otp($where)
  {
    return $this->Common_model->getDataWhere('otp','*',$where);
  }
   public function check_user_mobile($mobile,$isRegisterSuccessfully="")
  {
    $where = array('phone_number'=>$mobile);
    if($isRegisterSuccessfully!=""){
      $where['isRegisterSuccessfully'] =1;
    }
    return $this->Common_model->getDataWhere('user','*',$where);     
  }
   public function register_user($data)
  {
    return  $this->Common_model->insertData('user',$data); 
  }
 public function insert_register_user($data,$table)
  {
    return  $this->Common_model->insertData($table,$data); 
  }
  public function update_register_user($data,$where,$table)
  {
    return $this->Common_model->updateData($table, $where, $data);   
  }
   public function check_user($user_id,$table)
  {
    $where = array('user_id'=>$user_id);
    return $this->Common_model->getDataWhere($table,'*',$where);     
  }
   public function check_email($email,$table,$user_id="")
  {
    $where = array('email'=>$email,'user_id!='=>$user_id);
    return $this->Common_model->getDataWhere($table,'*',$where);     
  }
  public function communitySkills($user_id,$community,$skills){
    
    $query ="INSERT INTO  user_community_skills (`user_id`, `community_id`, `skill_id`) VALUES ";
    $value = array();
    if(!empty($community))
    {
      foreach ($community as $key => $comm_value) {
        foreach ($skills as $skill_key => $skill_value) {
          $skill_value = explode("_", $skill_value);
          if($skill_value[1]==$comm_value){
            $value[]= "('".$user_id."','".$comm_value."','".$skill_value[0]."')";
            $skill =false;
          }
          else{
             $skill =true;
          }
                     
        }
        if($skill){
          $value[]= "('".$user_id."','".$comm_value."','')";
        }
      } 
    }
    $query_value = implode(",", $value);
    $query = $query.$query_value;
    return $this->Common_model->executeSql($query,'insert');
    
  }

    public function getDataWhere($table,$select,$where)
    {
      return $this->Common_model->getDataWhere($table,$select,$where);
    }

    






  //////admin login
  public function check_login($email,$password)
  {
    $where = array('admin_email'=>$email,'admin_password'=>md5($password));
    return $this->Common_model->getDataWhere('admin','*',$where);    
  }
  //check password
  public function check_admin_password($admin_id,$old_password)
  {
   return $this->AuthModel->check_admin_password($admin_id,$old_password);
  }
  ////change password of the admin
  public function change_admin_password($admin_id,$new_password)
  {
   return $this->AuthModel->change_admin_password($admin_id,$new_password);
  }
  //////user login 
  public function userLogin($emailAndMobile,$password)
  {
    $sql = "SELECT * FROM `user` WHERE (email = '$emailAndMobile' OR phone_number = '$emailAndMobile') AND password = '$password' AND is_delete = 'not_deleted' ";
     $result = $this->Common_model->executeSql($sql,"row_array");
    
     return $result;
  }
  
  public function upload_image($image_data,$num,$path1)
  {
   return $this->Common_model->upload_image($image_data,$num,$path1);
  }
  
  public function CheckMail($email)
  {
    $where = array('user_email'=>$email,'is_delete'=>'not_deleted');
    return $this->Common_model->getDataWhere('user','*', $where );
  }
  public function checkMobile($mobile)
  {
    $where = array('user_phone'=>$mobile,'is_delete'=>'not_deleted');
    return $this->Common_model->getDataWhere('user','*',$where);
  }
  public function createUser($userData)
  {
    return  $this->Common_model->insertData('user',$userData);

  }
  public function updateUser($userId,$data)
  {
    $where = array('user_id'=>$userId);
    return $this->Common_model->updateData('user', $where, $data); 
  }
  public function getUser()
  {
    $where = array('is_delete'=>'not_deleted');
    return $this->Common_model->getAllDataWhere('user','*', $where );

  }
  public function getUserById($userID)
  {
    $where = array('is_delete'=>'not_deleted','user_id'=>$userID);
    return $this->Common_model->getAllDataWhere('user','*', $where );
  }
  public function upload_multiple_images($files)
    {
       // print_r($files);die;
        $image_path = "";
           if(isset($files["tmp_name"])&& $files["tmp_name"][0]!='')
            {
                    $extension=array("jpeg","jpg","png","gif");
                    $i = 0;
               
                    foreach($files["tmp_name"] as $key=>$tmp_name) {
                        $file_name=$files["name"][$key];
                        $file_tmp=$files["tmp_name"][$key];
                        $ext=pathinfo($file_name,PATHINFO_EXTENSION);
                     
                        $filename=basename($file_name,$ext);
                        $newFileName=md5($filename.time())."$i.".$ext;
                        $fullpath = "upload/product/".$newFileName;
                        move_uploaded_file($file_tmp=$files["tmp_name"][$key],$fullpath);
                        $image_path .= ",".$fullpath;
                        $i++;
                    }
            }
        return ltrim($image_path, ",");
    }

    public function insertData($table,$data)
    {
      
      return $this->Common_model->insertData($table,$data);
    }

    public function updateData($table,$where,$data)
    {      
      return $this->Common_model->updateData($table,$where,$data);
      $this->Common_model->updateData($table,$where,$data);
     
    }

    public function executeSql($sql,$action)
    {
      
      return $this->Common_model->executeSql($sql,$action);
    }
    
    public function getAllDataWhere($table,$select,$where)
    {
      
      return $this->Common_model->getAllDataWhere($table,$select,$where);
    }
  
  ///////get all plan
   public function getAllPlan($role="")
   {
      $where = array("is_delete" => "not_deleted");
      if($role!=""){
         $where['role_type'] = $role;
         $where['is_active'] = 'Active';
      }
      return $this->Common_model->getAllDataWhere('plan', '*', $where);
   }
  //////////////get plan by id
  public function getPlanById($planID)
  {
    $where = array('plan_id' => $planID, 'is_delete' => 'not_deleted');
    return $this->Common_model->getDataWhere('plan', '*', $where);
  }
  //////////update srticle
  public function updateArticle($article_id,$updateArticleData)
  {
    $where = array('article_id' => $article_id);
    return $this->Common_model->updateData('article', $where, $updateArticleData); 
  }
  ///////ad article
   public function addArticle( $data)
   {
    return  $this->Common_model->insertData('article', $data); 
   } 
   //////get article by id
   public function getArticleById($article_id)
   {
    $sql = "SELECT article.*,user.full_name FROM `article` 
            LEFT JOIN user ON user.user_id = article.user_id WHERE article.article_id ='$article_id' AND article.is_deleted = 'not_deleted' ";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
      
   }
   ///////get all article
   public function getAllArticle()
   {
    $sql = "SELECT article.*,user.full_name, user.profile_pic,
            (SELECT GROUP_CONCAT(community.community_name) FROM community WHERE find_in_set( community.community_id, article.community_ids)) AS communities,

            (select COUNT(*) FROM user_like_dislike WHERE user_like_dislike.target_id = article.article_id) AS all_like_dislike ,
            (select COUNT(*) FROM user_comments WHERE user_comments.target_id = article.article_id) AS all_comment,
            (select COUNT(*) FROM user_report WHERE user_report.target_id = article.article_id) AS all_report
            FROM `article` 
            LEFT  JOIN user ON user.user_id = article.user_id WHERE article.is_deleted = 'not_deleted'";
    $result = $this->Common_model->executeSql($sql, "result_array");
    return $result;    
    
   }

   ///////get all comments of the article

   public function getAllArticleComment($articleId)
   {
      $sql = "SELECT user_comments.*,user.full_name 
      FROM `user_comments` 
      LEFT JOIN user ON user.user_id = user_comments.user_id 
      WHERE user_comments.target_id = '$articleId'";
      $result = $this->Common_model->executeSql($sql, "result_array");
      return $result;
   }
   public function  getAllArticleLikeDislike($articleId)
   {
      $sql = "SELECT user_like_dislike.*,user.full_name 
      FROM `user_like_dislike` 
      LEFT JOIN user ON user.user_id = user_like_dislike.user_id 
      WHERE user_like_dislike.target_id = '$articleId'";
      $result = $this->Common_model->executeSql($sql, "result_array");
      return $result;
   }
   public function getAllArticleReport($articleId)
   {
      $sql = "SELECT user_report.*,user.full_name 
      FROM `user_report` 
      LEFT JOIN user ON user.user_id = user_report.user_id 
      WHERE user_report.target_id = '$articleId'";
      $result = $this->Common_model->executeSql($sql, "result_array");
      return $result;
   }
    public function getUserByIdByAdmin($userId)
    {
      $sql = "SELECT user.*,country.name AS country_name,city.name AS city_name,states.name AS state_name FROM `user` 
              LEFT JOIN country ON country.id = user.country_id 
              LEFT JOIN city ON city.id = user.city_id 
              LEFT JOIN states ON states.id = user.state_id 
              WHERE user.user_role = 'User' 
              AND user.user_id = '$userId' 
              AND user.isRegisterSuccessfully = 1 
              AND user.is_delete = 'not_deleted'";
      $result = $this->Common_model->executeSql($sql, "row_array");
      return $result;
    }
    public function getAllUserByAdmin()
    {
      $sql = "SELECT * FROM `user` WHERE user_role = 'User' AND isRegisterSuccessfully = 1 AND is_delete = 'not_deleted'";
      $result = $this->Common_model->executeSql($sql, "result_array");
      return $result;
    }
    public function getClientByIdByAdmin($userId)
    {
      $sql = "SELECT user.*,country.name AS country_name,city.name AS city_name,states.name AS state_name FROM `user` 
              LEFT JOIN country ON country.id = user.country_id 
              LEFT JOIN city ON city.id = user.city_id 
              LEFT JOIN states ON states.id = user.state_id 
              WHERE user.user_role = 'Client' 
              AND user.user_id = '$userId' 
              AND user.isRegisterSuccessfully = 1 
              AND user.is_delete = 'not_deleted'";
      $result = $this->Common_model->executeSql($sql, "row_array");
      return $result;
    }
    public function getAllClientByAdmin()
    {
      $sql = "SELECT * FROM `user` WHERE user_role = 'Client' AND isRegisterSuccessfully = 1 AND is_delete = 'not_deleted'";
      $result = $this->Common_model->executeSql($sql, "result_array");
      return $result;
    }
    public function getEmployesByIdByAdmin($userId)
    {
      $sql = "SELECT user.*,country.name AS country_name,city.name AS city_name,states.name AS state_name FROM `user` 
              LEFT JOIN country ON country.id = user.country_id 
              LEFT JOIN city ON city.id = user.city_id 
              LEFT JOIN states ON states.id = user.state_id 
              WHERE user.user_role = 'Employer' 
              AND user.user_id = '$userId' 
              AND user.isRegisterSuccessfully = 1 
              AND user.is_delete = 'not_deleted'";
      $result = $this->Common_model->executeSql($sql, "row_array");
      return $result;
    }
    public function getAllEmployesByAdmin()
    {
      $sql = "SELECT * FROM `user` WHERE user_role = 'Employer' AND isRegisterSuccessfully = 1 AND is_delete = 'not_deleted'";
      $result = $this->Common_model->executeSql($sql, "result_array");
      return $result;
    }
    public function getKpByIdByAdmin($userId)
    {
      $sql = "SELECT user.*,country.name AS country_name,city.name AS city_name,states.name AS state_name FROM `user` 
              LEFT JOIN country ON country.id = user.country_id 
              LEFT JOIN city ON city.id = user.city_id 
              LEFT JOIN states ON states.id = user.state_id 
              WHERE user.user_role = 'KP' 
              AND user.user_id = '$userId' 
              AND user.isRegisterSuccessfully = 1 
              AND user.is_delete = 'not_deleted'";
      $result = $this->Common_model->executeSql($sql, "row_array");
      return $result;
    }
    ////////////////////get all kp by admin
    public function getAllKpByAdmin()
    {
      $sqlApproved = "SELECT * FROM `user` 
                      WHERE user_role = 'KP' 
                      AND isRegisterSuccessfully = 1 
                      AND is_delete = 'not_deleted' 
                     ";
      $result['approved_kp'] = $this->Common_model->executeSql($sqlApproved, "result_array");
      $sqlApproved = "SELECT * FROM `user` 
                      WHERE user_role = 'KP' 
                      AND isRegisterSuccessfully = 1 
                      AND is_delete = 'not_deleted' 
                      AND is_active = 'Deactive'";
      $result['unapproved_kp'] = $this->Common_model->executeSql($sqlApproved, "result_array");
      return $result;
    }
    /////////////////get question by id by admin
    public function getQuestionByIdByAdmin($questionId)
    {
    $sql = "SELECT questions.*,user.full_name FROM `questions` LEFT JOIN user ON user.user_id = questions.user_id WHERE questions.question_id = $questionId AND questions.is_delete = 'not_deleted'";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
    }
    public function getAllQuestionByAdmin()
    {
      $sql = "SELECT questions.*,user.full_name FROM `questions` LEFT JOIN user ON user.user_id = questions.user_id WHERE questions.is_delete = 'not_deleted'";
      $result = $this->Common_model->executeSql($sql, "result_array");
      return $result;
    }
    
    ///////get all Community

   public function getAllCommunity($articleId)
   {
      $sql = "SELECT * FROM `community` WHERE is_active = 'Active'";
      $result = $this->Common_model->executeSql($sql, "result_array");
      return $result;
   }
   public function getAllCommunityByComminityId($comminityIds)
   {
      $sql = "SELECT community_name   FROM `community` WHERE community_id IN($comminityIds)";
      $result = $this->Common_model->executeSql($sql, "result_array");
      return $result;
   }
   ////////////////////// update question by admin
   public function updateQuestionByAdmin($question_id, $updateData)
   {
      $where = array('question_id'=>$question_id);
    return $this->Common_model->updateData('questions', $where, $updateData);
   }
   ////////////////////// Get All Community Members //////////
   public function getAllCommunityMembers()
   {
    $sql = "SELECT  community.community_id, 
                    community.community_name,
                    UCS.user_id,
                    user.full_name,
                    user.profile_pic
                FROM community
                LEFT JOIN user_community_skills UCS ON(UCS.community_id = community.community_id)
                LEFT JOIN user ON(user.user_id = UCS.user_id)
                ORDER BY community.community_id ASC";
    $result = $this->Common_model->executeSql($sql, "result_array");
    return $result;    
    
   }
}