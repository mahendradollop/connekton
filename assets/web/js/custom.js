(function ( $ ) {
    $.fn.multiStepForm = function(args) {
        if(args === null || typeof args !== 'object' || $.isArray(args))
            throw  " : Called with Invalid argument";
            var form = this;
            var tabs = form.find('.tab');
            var steps = form.find('.step');
            steps.each(function(i, e){
                $(e).on('click', function(ev){
                });
            });
            form.navigateTo = function (i) {/*index*/
            /*Mark the current section with the class 'current'*/
            tabs.removeClass('current').eq(i).addClass('current');
            // Show only the navigation buttons that make sense for the current section:
            form.find('.previous').toggle(i > 0);
            atTheEnd = i >= tabs.length - 1;
            form.find('.next').toggle(!atTheEnd);
            // console.log('atTheEnd='+atTheEnd);
            form.find('.submit').toggle(atTheEnd);
            fixStepIndicator(curIndex());
            return form;
        }
        function curIndex() {
            /*Return the current index by looking at which section has the class 'current'*/
            return tabs.index(tabs.filter('.current'));
        }
        function fixStepIndicator(n) {
            steps.each(function(i, e){
                i == n ? $(e).addClass('active') : $(e).removeClass('active');
            });
        }
        /* Previous button is easy, just go back */
        form.find('.previous').click(function() {
            form.navigateTo(curIndex() - 1);
             if(curIndex()=="1"){
                $('.next').html('Get OTP');
            }
            else  if(curIndex()=="2"){
                $('.next').html('Verify OTP');
            }else{
                $('.next').html('Next');
            }

        });

        /* Next button goes forward iff current block validates */
        form.find('.next').click(function() {
            if('validations' in args && typeof args.validations === 'object' && !$.isArray(args.validations)){
                if(!('noValidate' in args) || (typeof args.noValidate === 'boolean' && !args.noValidate)){
                    form.validate(args.validations);
                    if(form.valid() == true){
                        
                        if(curIndex()=='0'){
                            $('.next').html('Get OTP');
                            form.navigateTo(curIndex() + 1);
                        }else if(curIndex()=='1'){
                            send_otp($('#phone_number').val());
                            $('.next').html('Verify OTP')
                            form.navigateTo(curIndex() + 1);

                        }else if(curIndex()=='2'){
                             $('.next').html('Next');

                            var codeBox1 = $('#codeBox1').val();
                            var codeBox2 = $('#codeBox2').val();
                            var codeBox3 = $('#codeBox3').val();
                            var codeBox4 = $('#codeBox4').val();
                            var otp =  codeBox1+codeBox2+codeBox3+codeBox4;
                            var result = otp_verify($('#phone_number').val(),otp);
                           
                            if(result!=''){
                                 form.navigateTo(curIndex() + 1);
                            }
                        }
                        else{

                             $('.next').html('Next');
                             form.navigateTo(curIndex() + 1);
                        }
                        return true;
                    }
                    return false;
                }
            }
            form.navigateTo(curIndex() + 1);
        });
        form.find('.submit').on('click', function(e){
            if(typeof args.beforeSubmit !== 'undefined' && typeof args.beforeSubmit !== 'function')
                args.beforeSubmit(form, this);
                /*check if args.submit is set false if not then form.submit is not gonna run, if not set then will run by default*/        
            if(typeof args.submit === 'undefined' || (typeof args.submit === 'boolean' && args.submit)){
                form.submit();
            }
            return form;
        });
        /*By default navigate to the tab 0, if it is being set using defaultStep property*/
        typeof args.defaultStep === 'number' ? form.navigateTo(args.defaultStep) : null;
        form.noValidate = function() {
    
        }
        return form;
    };
}( jQuery ));


function send_otp(mobile,type=""){
    if(type!=""){
        $('#codeBox1').val('');
        $('#codeBox2').val('');
        $('#codeBox3').val('');
        $('#codeBox4').val('');
    }
  $.ajax({
      url: "sendOtp",
      data:{mobile: mobile },
      method:'POST',
      cache: false,
      success: function(response){
        var obj = JSON.parse(response);
        if(obj.status==200){
            if(type!=""){
                $('.sent_msg').html('');
                $('.resend_msg').html('OTP has been resend on your mobile no. +91 '+mobile);
                setTimeout(function() { $(".resend_msg").html(''); }, 15000);
            }else{
                 $('.sent_msg').html('OTP has been sent on your mobile no. +91 '+mobile);
                setTimeout(function() { $(".sent_msg").html(''); }, 15000);
            }
        }
    }
    });
}

function otp_verify(mobile,otp){
    
    var msg ="";
   
    $.ajax({
      url: "verifyOtp",
      data:{mobile: mobile, otp: otp},
      method:'POST',
      async:false,
      cache: false,
      success: function(response){
        var obj = JSON.parse(response);
        if(obj.status==200){
           msg ="1";
        }else{
            msg ="";
            $('.otp_error_msg').show();
            setTimeout(function() { $(".otp_error_msg").hide(); }, 15000);
        }
    }
    });
     return msg;
}