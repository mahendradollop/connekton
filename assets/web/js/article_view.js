/*********Get Article**********/
getAllArticles();
function getAllArticles() {
$(document).ready(function() {

        $.ajax({
            url: BASE_URL+"getArticle",
            type: "GET",
            dataType: "json",
            async: false,
            headers: {
                'Authorization': token
            },
            success: function(data) {
                //console.log(data);
                var showArticles = '';
                var article = data.data;
                $.each(article, function(i, value) {
                    // console.log(value.communities);
                    if (value.profile_pic == null) {
                        showArticles += `<div class="inner_artical_splits_cards w-100 pb-3 bg-white m_mb_20 position-relative">
                                <span class="date">` + value.created_at + `</span>
                                <div class="job_posted_by d-inline-block w-100 position-relative">
                                     <span class="posted_user_profile d-inline-flex align-items-center font-12 rounded-circle ">
                                     <img src="` + BASE_URL + "assets/web/images/user-img.png" + `" class="img-fluid" alt="">  
                                     </span>
                                     <h5>` + value.full_name + `</h5>
                                     <p>` + value.communities + `</p>
                                  </div>
                                <div class="artical_image pt-2">
                                    <img src="` + BASE_URL + value.cover_image +`" class="img-fluid" alt="">
                                </div>
                                <div class="artical_body">
                                    <h4 class="question_name font-20 font-700 d-inline-flex w-auto pb-1">` + value.article_title + `</h4>
                                    <p class="font-14">` + value.content + `</p>
                                </div>
                                <div class="like-comment-bar d-inline-flex align-items-center w-100 py-3 border-top">
                                    <a href="#" class="like mr-5"><span class="material-icons">thumb_up</span> ` + value.all_like_dislike + `</a>
                                    <a href="#" class="dislike"><span class="material-icons">thumb_down</span> ` + value.all_like_dislike + `</a>
                                    <a href="#" onclick="return false;" class="comment ml-auto"><span class="material-icons">chat</span>` + value.all_comment + `</a>
                                </div>
                                <div class="comment-box">
                                    <div class="cmm-inner-body d-inline-block w-100 py-3 border-top">
                                        <div class="type-comment d-inline-flex align-items-center w-100 mb-3">
                                            <spna class="comm-user-img">
                                                <img src="">
                                            </spna>
                                            <div class="add-your-comment">
                                                <input type="text" class="form-control rounded-pills" placeholder="Type Your Comm">
                                                <a href="#" class="emoji"><img src="<?php echo base_url(); ?>assets/web/images/emoji.png" class=""></a>
                                            </div>
                                            <div class="comment-send">
                                                <a href="#" class="btn btn-send">
                                                    <span class="material-icons">send</span>
                                                </a>
                                            </div>
                                            </div>
                                            <div class="other-comments d-inline-flex w-100 mb-3">
                                                <spna class="comm-user-img">
                                                    <img src="">
                                                </spna>
                                                <div class="add-your-comment position-relative">
                                                    <span class="date">02:45 PM</span>
                                                   <span class="cmm-posted-name">Lisa</span>
                                                   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p> 
                                                </div>
                                            </div>
                                            <div class="other-comments d-inline-flex w-100 mb-3">
                                                <spna class="comm-user-img">
                                                    <img src="">
                                                </spna>
                                                <div class="add-your-comment position-relative">
                                                    <span class="date">02:45 PM</span>
                                                   <span class="cmm-posted-name">Lisa</span>
                                                   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`
                    }
                    else{
                         showArticles += `<div class="inner_artical_splits_cards w-100 pb-3 bg-white m_mb_20 position-relative">
                                <span class="date">` + value.created_at + `</span>
                                <div class="job_posted_by d-inline-block w-100 position-relative">
                                     <span class="posted_user_profile d-inline-flex align-items-center font-12 rounded-circle ">
                                     <img src="` + BASE_URL + value.profile_pic + `" class="img-fluid" alt="">  
                                     </span>
                                     <h5>` + value.full_name + `</h5>
                                     <p>` + value.communities + `</p>
                                  </div>
                                <div class="artical_image pt-2">
                                    <img src="` + BASE_URL + value.cover_image +`" class="img-fluid" alt="">
                                </div>
                                <div class="artical_body">
                                    <h4 class="question_name font-20 font-700 d-inline-flex w-auto pb-1">` + value.article_title + `</h4>
                                    <p class="font-14">` + value.content + `</p>
                                </div>
                                <div class="like-comment-bar d-inline-flex align-items-center w-100 py-3 border-top">
                                    <a href="#" class="like mr-5"><span class="material-icons">thumb_up</span> ` + value.all_like_dislike + `</a>
                                    <a href="#" class="dislike"><span class="material-icons">thumb_down</span> ` + value.all_like_dislike + `</a>
                                    <a href="#" onclick="return false;" class="comment ml-auto"><span class="material-icons">chat</span>` + value.all_comment + `</a>
                                </div>
                                <div class="comment-box">
                                    <div class="cmm-inner-body d-inline-block w-100 py-3 border-top">
                                        <div class="type-comment d-inline-flex align-items-center w-100 mb-3">
                                            <spna class="comm-user-img">
                                                <img src="">
                                            </spna>
                                            <div class="add-your-comment">
                                                <input type="text" class="form-control rounded-pills" placeholder="Type Your Comm">
                                                <a href="#" class="emoji"><img src="<?php echo base_url(); ?>assets/web/images/emoji.png" class=""></a>
                                            </div>
                                            <div class="comment-send">
                                                <a href="#" class="btn btn-send">
                                                    <span class="material-icons">send</span>
                                                </a>
                                            </div>
                                            </div>
                                            <div class="other-comments d-inline-flex w-100 mb-3">
                                                <spna class="comm-user-img">
                                                    <img src="">
                                                </spna>
                                                <div class="add-your-comment position-relative">
                                                    <span class="date">02:45 PM</span>
                                                   <span class="cmm-posted-name">Lisa</span>
                                                   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p> 
                                                </div>
                                            </div>
                                            <div class="other-comments d-inline-flex w-100 mb-3">
                                                <spna class="comm-user-img">
                                                    <img src="">
                                                </spna>
                                                <div class="add-your-comment position-relative">
                                                    <span class="date">02:45 PM</span>
                                                   <span class="cmm-posted-name">Lisa</span>
                                                   <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`
                    }
                });
                    $("#showArticle").html(showArticles);
            },
           
        });

});
}

/*********Get all community**********/

getComunity();
      function getComunity(){ 
         var main_content ="";
         $.ajax({
                url: BASE_URL+"get_comunity",
                
                success: function(result){
                var obj=$.parseJSON(result);
                //console.log(result)
                if(obj.status == 200){
                    $.each(obj.data, function(index,val){
                     main_content+='<option  value="'+val.community_id +'">'+val.community_name+'</option>';
                    })
                }
                $('#community_ids').html(main_content);
                $("#community_ids").select2({
                    placeholder: "Select a programming language",
                    allowClear: true,
                    maximumSelectionLength: 5
                });
            }
        })
    }


/*********POST Article**********/

$( "#article_form" ).validate({
        rules: {
            article_title: "required",
            content: "required",
            community_ids: "required",
            cover_image: "required",
            agrement: "required"
        },
        messages: {
            article_title: "Please enter your title",
            content: "Please enter your content",
            community_ids: "Please selete your community",
            cover_image: "Please selete your cover image",
            agrement: "Please selete your agrement",
        },

        submitHandler: function(form){
                $.ajax({
                  url: BASE_URL+"addArticle",
                  type: 'POST',
                  data:  new FormData($('#article_form')[0]),
                  cache:false,
                  contentType: false,
                  processData: false,
                  headers: {
                    'Authorization': token
                    },
                  success: function(response) {
                    $('#addartical').modal('hide');
                    getAllArticles();
                  }
              });   
        }
    });

/*********Get Community Members**********/

$(document).ready(function() {

        $.ajax({
            url: BASE_URL+"getCommunityMembers",
            type: "GET",
            dataType: "json",
            async: false,
            headers: {
                'Authorization': token
            },
            success: function(data) {
                
                var showCommunities = groupBy(data.data, 'community_id');
              
                var showCommunityHtml = '';
                
                $.each(showCommunities, function(i, communityMembers) {
                    
                    var community_name = '';
                    var community_id = '';
                    var profile_pic = '';
                    var full_name = '';
                    var communityMemberHtml = '';
                    $.each(communityMembers, function(j, MemberInfo) {
                        //console.log(MemberInfo);
                        community_name = MemberInfo.community_name;
                        full_name = MemberInfo.full_name;
                        profile_pic = MemberInfo.profile_pic;
                        community_id = MemberInfo.community_id;
                        if(MemberInfo.user_id != null){
                            if (MemberInfo.profile_pic != null) {
                                communityMemberHtml += `
                                <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="` + BASE_URL + profile_pic + `" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">` + full_name + ` </a>
                                                <span class="member-type font-10">` + community_name + ` </span>
                                            </div>
                                        </div>
                            `;
                            }else{
                            communityMemberHtml += `
                                <div class="member_profile d-flex align-items-center w-100 py-2">
                                            <div class="member_pic ctk_overflow_hidden">
                                                <img src="` + BASE_URL + "assets/web/images/1_member.png" + `" class="img-fluid" alt="">
                                            </div>
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">` + full_name + ` </a>
                                                <span class="member-type font-10">Freelancer </span>
                                            </div>
                                        </div>
                            `;
                        }
                        }else{
                            communityMemberHtml += `
                                <div class="member_profile d-flex align-items-center w-100 py-2">
                                            
                                            <div class="member_name">
                                                <a href="#" class="font-14 m-0 font-500">No Member Found !</a>
                                             
                                            </div>
                                        </div>
                            `;
                        }

                    });

                    showCommunityHtml += `<div class="card">
                                            <div class="card-header active" >
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link font-14 p-0" data-toggle="collapse" data-target="#` + community_id + `" aria-expanded="true" aria-controls="collapseOne">
                                                     ` + community_name + `
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="` + community_id + `" class="collapse" aria-labelledby="` + community_id + `" data-parent="#accordion">
                                                <div class="card-body p_px_15">
                                                ` + communityMemberHtml + `
                                                </div>
                                            </div>
                                        </div>`;
                });
                $("#accordion").html(showCommunityHtml);
            },
           
        });

        //////// read more

});

function groupBy(objectArray, property) {
        // console.log("property");
        // console.log(property);
       return objectArray.reduce((acc, obj) => {
          const key = obj[property];
          if (!acc[key]) {
             acc[key] = [];
          }
          // Add object to list for given key's value
          acc[key].push(obj);
          return acc;
       }, {});
    }